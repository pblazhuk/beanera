<?php
/**
 * Plugin Name: AIT Stripe Payments
 * Version: 1.2
 * Description: Adds Stripe gateway to Directory type themes
 *
 * Author: AitThemes.Club
 * Author URI: https://ait-themes.club
 * License: GPLv2 or later
 * Text Domain: ait-stripe
 * Domain Path: /languages
 */

/* trunk@r25 */

defined('ABSPATH') or die();
define('AIT_STRIPE_ENABLED', true);
include_once(__DIR__.'/load.php');

add_action('after_setup_theme', function() {
	try {
		AitStripe::getInstance();
	} catch (Exception $e) {
		AitStripe::log($e);
	}
});

register_activation_hook(__FILE__, function() {
	stripeCheckCompatibility(true);
	AitCache::clean();
});
add_action('after_switch_theme', function(){
	stripeCheckCompatibility();
});

add_action('plugins_loaded', function() {
	load_plugin_textdomain('ait-stripe', false, dirname(plugin_basename( __FILE__ )) . '/languages');
}, 11);

function stripeCheckCompatibility($die = false){
	if ( !defined('AIT_THEME_TYPE') ){	// directory themes
		require_once(ABSPATH . 'wp-admin/includes/plugin.php' );
		deactivate_plugins(plugin_basename( __FILE__ ));
		if($die){
			wp_die('Current theme is not compatible with Stripe Payments plugin :(', '',  array('back_link'=>true));
		} else {
			add_action( 'admin_notices', function(){
				echo "<div class='error'><p>" . __('Current theme is not compatible with Stripe Payments plugin!', 'ait-stripe') . "</p></div>";
			} );
		}
	}
}