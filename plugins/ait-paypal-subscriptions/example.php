<?php

/**
 * EXAMPLE OF USAGE
 */

if (isset($_GET['test-paypal-subscriptions'])) {

	$data = array(
		'membership' => 'business'
	);

	$paypal = AitPaypalSubscriptions::getInstance();
	$paypal->createAgreement($data, array(
		'name' => 'Daily',
		'description' => 'Daily subscription (1 USD every day)',
		'interval' => 1,
		'amount' => 1,
		'currency' => 'USD'
	));

}

// add_action('ait-paypal-subscriptions-agreement-confirmed', function ($agreement) {
// 	// $agreement->data
// 	AitPaypalSubscriptions::log($agreement, 'AGREEMENT CONFIRMED');
// });

// add_action('ait-paypal-subscriptions-profile-created', function ($profile) {
// 	// $profile->data
// 	AitPaypalSubscriptions::log($profile, 'PROFILE CREATED');
// });

// add_action('ait-paypal-subscriptions-profile-canceled', function ($profile) {
// 	// $profile->data
// 	AitPaypalSubscriptions::log($profile, 'PROFILE CANCELED');
// });

// add_action('ait-paypal-subscriptions-payment-completed', function ($payment) {
// 	// $payment->data
// 	AitPaypalSubscriptions::log($payment, 'PAYMENT COMPLETED');
// });