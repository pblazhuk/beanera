<?php

use PayPal\CoreComponentTypes\BasicAmountType;
use PayPal\EBLBaseComponents\AddressType;
use PayPal\EBLBaseComponents\BillingAgreementDetailsType;
use PayPal\EBLBaseComponents\PaymentDetailsItemType;
use PayPal\EBLBaseComponents\PaymentDetailsType;
use PayPal\EBLBaseComponents\SetExpressCheckoutRequestDetailsType;
use PayPal\PayPalAPI\SetExpressCheckoutReq;
use PayPal\PayPalAPI\SetExpressCheckoutRequestType;
use PayPal\Service\PayPalAPIInterfaceServiceService;

use PayPal\EBLBaseComponents\ActivationDetailsType;
use PayPal\EBLBaseComponents\BillingPeriodDetailsType;
use PayPal\EBLBaseComponents\CreateRecurringPaymentsProfileRequestDetailsType;
use PayPal\EBLBaseComponents\CreditCardDetailsType;
use PayPal\EBLBaseComponents\RecurringPaymentsProfileDetailsType;
use PayPal\EBLBaseComponents\ScheduleDetailsType;
use PayPal\PayPalAPI\CreateRecurringPaymentsProfileReq;
use PayPal\PayPalAPI\CreateRecurringPaymentsProfileRequestType;

use PayPal\IPN\PPIPNMessage;

class AitPaypalSubscriptions
{

	private static $instance = null;
	private static $logFileInfo = 'paypal-info.log';
	private static $logFileError = 'paypal-error.log';
	private static $temporaryDataPrefix = '_ait_paypal_token_';
	private static $profileDataPrefix = '_ait_paypal_profile_';

	private $api;

	public $options;
	public static $getParameterName = 'ait-paypal-subscriptions-action';



	protected function __construct() {}

	public static function getInstance()
	{
		if (null == self::$instance) {
			self::$instance = new self;
			self::$instance->setOptions();
			self::$instance->setLogging();
			self::$instance->handleNotification();
			self::$instance->handleReturn();
		}
		return self::$instance;
	}

	private function setOptions()
	{
		if (!function_exists('aitOptions')) {
			throw new AitPaypalSubscriptionsException("PayPal plugin is compatible only with AIT framework 2.0");
		}
		$options = aitOptions()->get('theme');
		if (isset($options->paypal)) {
			$this->options = $options->paypal;
			// URLs
			$this->options->urls = new StdClass;
			$urlReturn = (!empty($this->options->returnPage)) ? get_permalink($this->options->returnPage) : home_url('/');
			$urlCancel = (!empty($this->options->cancelPage)) ? get_permalink($this->options->cancelPage) : home_url('/');
			$this->options->urls->return = add_query_arg(self::$getParameterName, 'return', $urlReturn);
			$this->options->urls->cancel = add_query_arg(self::$getParameterName, 'cancel', $urlCancel);
			$this->options->urls->notify = add_query_arg(self::$getParameterName, 'notification', home_url());
		}
	}

	private function setApi()
	{
		if (empty($this->options)) {
			throw new AitPaypalSubscriptionsException("Missing theme options for Paypal");
		}
		if (empty($this->options->realApiUsername) || empty($this->options->realApiPassword) || empty($this->options->realApiSignature)) {
			throw new AitPaypalSubscriptionsException("Missing API credentials for Paypal");
		}
		$config = array(
			"mode" => "live",
			"acct1.UserName" => $this->options->realApiUsername,
			"acct1.Password" => $this->options->realApiPassword,
			"acct1.Signature" => $this->options->realApiSignature
		);
		if ($this->options->logging) {
			$config = $config + array(
				'log.LogEnabled' => true,
				'log.FileName' => WP_CONTENT_DIR."/".self::$logFileInfo,
				'log.LogLevel' => 'DEBUG' // PLEASE USE `FINE` LEVEL FOR LOGGING IN LIVE ENVIRONMENTS
				// 'validation.level' => 'log'
			);
		}
		$api = new PayPalAPIInterfaceServiceService($config);
		$this->api = $api;
		return $api;
	}

	private function getApi()
	{
		if (empty($this->api)) {
			$this->setApi();
		}
		return $this->api;
	}

	public function createAgreement($data, $agreement)
	{
		try {
			// Convert agreement array to object
			$agreement = (object) $agreement;
			// Correct strings
			$agreement->name = substr($agreement->name, 0, 127);
			$agreement->description = substr($agreement->description, 0, 127);
			if (empty($agreement->description)) {
				$agreement->description = $agreement->name;
			}
			// Translate strings
			if (class_exists('AitLangs')) {
				$agreement->name = AitLangs::getCurrentLocaleText($agreement->name);
				$agreement->description = AitLangs::getCurrentLocaleText($agreement->description);
			}
			$agreement->data = $data;

			$api = $this->getApi();

			// Notify URL for recurring payments isn't available
			// $paymentDetails = new PaymentDetailsType();
			// $paymentDetails->NotifyURL = $this->options->urls->notify;
			// $setECReqDetails->PaymentDetails = $paymentDetails;

			// Get token
			$setECReqDetails = new SetExpressCheckoutRequestDetailsType();
			$setECReqDetails->CancelURL = $this->options->urls->cancel;
			$setECReqDetails->ReturnURL = $this->options->urls->return;
			$billingAgreementDetails = new BillingAgreementDetailsType('RecurringPayments');
			$billingAgreementDetails->BillingAgreementDescription = $agreement->description;
			$setECReqDetails->BillingAgreementDetails = array($billingAgreementDetails);
			$setECReqType = new SetExpressCheckoutRequestType();
			$setECReqType->SetExpressCheckoutRequestDetails = $setECReqDetails;
			$setECReq = new SetExpressCheckoutReq();
			$setECReq->SetExpressCheckoutRequest = $setECReqType;

			$setECResponse = $api->SetExpressCheckout($setECReq);
			if(!isset($setECResponse) || $setECResponse->Ack != 'Success') {
				throw new AitPaypalSubscriptionsException($setECResponse->LongMessage);
			}

			// save temporary data to WP DB (three hours)
			set_transient($temporaryDataPrefix.$setECResponse->Token, $agreement, 60 * 60 * 3);

			// Approve token
			header('Location: https://www.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token='.$setECResponse->Token);
			die();
		} catch (Exception $e) {
			self::error($e);
		}
	}

	public function handleReturn()
	{
		if (isset($_GET[self::$getParameterName]) && $_GET[self::$getParameterName] == 'return' && isset($_GET['token'])) {
			$token = $_GET['token'];
			$agreement = get_transient($temporaryDataPrefix.$token);
			delete_transient($temporaryDataPrefix.$token);
			if ($agreement) {
				try {
					$api = $this->getApi();

					// Create profile
					$createRPProfileRequestDetail = new CreateRecurringPaymentsProfileRequestDetailsType();
					$createRPProfileRequestDetail->Token = $token;

					if (empty($agreement->initialAmount)) {
						$startDate = date(DATE_ISO8601, time()+10); // + 10 seconds to work with PayPal API
					} else {
						$startDate = date(DATE_ISO8601, strtotime('+'.$interval.' days'));
					}
					$RPProfileDetails = new RecurringPaymentsProfileDetailsType();
					$RPProfileDetails->BillingStartDate = $startDate;
					$createRPProfileRequestDetail->RecurringPaymentsProfileDetails = $RPProfileDetails;

					$currencyCode = $agreement->currency;
					$paymentBillingPeriod =  new BillingPeriodDetailsType();
					$paymentBillingPeriod->BillingFrequency = $agreement->interval;
					$paymentBillingPeriod->BillingPeriod = 'Day';
					$paymentBillingPeriod->Amount = new BasicAmountType($currencyCode, $agreement->amount);

					$scheduleDetails = new ScheduleDetailsType();
					$scheduleDetails->Description = $agreement->description;
					$scheduleDetails->PaymentPeriod = $paymentBillingPeriod;
					$createRPProfileRequestDetail->ScheduleDetails = $scheduleDetails;

					$createRPProfileRequest = new CreateRecurringPaymentsProfileRequestType();
					$createRPProfileRequest->CreateRecurringPaymentsProfileRequestDetails = $createRPProfileRequestDetail;

					$createRPProfileReq =  new CreateRecurringPaymentsProfileReq();
					$createRPProfileReq->CreateRecurringPaymentsProfileRequest = $createRPProfileRequest;

					// Create
					$createRPProfileResponse = $api->CreateRecurringPaymentsProfile($createRPProfileReq);

					if (empty($createRPProfileResponse->CreateRecurringPaymentsProfileResponseDetails->ProfileID)) {
						throw new AitPaypalSubscriptionsException("Problem with creating profile");
					}

					$agreement->id = $createRPProfileResponse->CreateRecurringPaymentsProfileResponseDetails->ProfileID;

					// Call action
					do_action('ait-paypal-subscriptions-agreement-confirmed', $agreement);

					// Save profile to DB
					update_option(self::$profileDataPrefix.$agreement->id, $agreement);

				} catch (Exception $e) {
					self::error($e);
				}
			}
		}
	}

	public function handleNotification()
	{
		if (isset($_GET[self::$getParameterName]) && $_GET[self::$getParameterName] == 'notification' && file_get_contents('php://input')) {
			try {
				$config = array("mode" => "live");
				if ($this->options->logging) {
					$config = $config + array(
						'log.LogEnabled' => true,
						'log.FileName' => WP_CONTENT_DIR."/".self::$logFileInfo,
						'log.LogLevel' => 'DEBUG' // PLEASE USE `FINE` LEVEL FOR LOGGING IN LIVE ENVIRONMENTS
						// 'validation.level' => 'log'
					);
				}
				$ipnMessage = new PPIPNMessage(null, $config);
				if(!$ipnMessage->validate()) {
					$message = file_get_contents('php://input');
					$messageDump = print_r($message, true);
					throw new AitPaypalSubscriptionsException("Error with validation of IPN message\n".$messageDump);	
				}
				$transactionType = $ipnMessage->getTransactionType();
				if (empty($transactionType)) {
					throw new AitPaypalSubscriptionsException("Empty transaction type");	
				}
				$message = $ipnMessage->getRawData();
				do_action('ait-paypal-subscriptions-notification', $message);
				// only messages for recurring payments
				if (isset($message['recurring_payment_id'])) {
					$profileId = $message['recurring_payment_id'];	
					$profile = get_option(self::$profileDataPrefix.$profileId);
					if (!$profile) {
						$messageDump = print_r($message, true);
						throw new AitPaypalSubscriptionsException("Missing profile data in DB. Profile ID: ".$profileId."\n".$messageDump);
					}
					$message = (object) $message;
					$message->data = $profile->data;
					switch ($transactionType) {
						case 'recurring_payment':
							do_action('ait-paypal-subscriptions-payment-completed', $message);
							break;
						case 'recurring_payment_profile_created':
							do_action('ait-paypal-subscriptions-profile-created', $message);
							break;
						case 'recurring_payment_profile_cancel':
							do_action('ait-paypal-subscriptions-profile-canceled', $message);
							break;
					}
				}
			} catch (Exception $e) {
				self::error($e);
			}
		}
	}

	private function setLogging()
	{
		if ($this->options->logging) {
			add_action('ait-paypal-subscriptions-notification', function ($message) {
				AitPaypalSubscriptions::log($message, 'NOTIFICATION');
			});
		}
	}

	public static function log($message, $title = '')
	{
		$message = print_r($message, true);
		$title = (!empty($title)) ? " - " . $title : "";
		$message = date("Y-m-d H:i:s") . $title . "\n\n" . $message . "\n";
		$file = WP_CONTENT_DIR."/".self::$logFileInfo;
		error_log($message, 3, $file);
	}

	public static function error($message)
	{
		if ($message instanceof Exception) {
			$message = $message->getMessage();
		} else {
			$message = print_r($message, true);
		}
		$message = date("Y-m-d H:i:s") . " - " . $message . "\n";
		$file = WP_CONTENT_DIR."/".self::$logFileError;
		error_log($message, 3, $file);
	}

}

class AitPaypalSubscriptionsException extends Exception {}