<?php
/**
 * Plugin Name: AIT PayPal Subscriptions
 * Version: 1.5
 * Description: Adds PayPal subscriptions (automatic recurring payments) to Directory type themes
 *
 * Author: AitThemes.Club
 * Author URI: https://ait-themes.club
 * License: GPLv2 or later
 * Text Domain: ait-paypal-subscriptions
 * Domain Path: /languages
 */

/* trunk@r78 */

defined('ABSPATH') or die();
define('AIT_PAYPAL_SUBSCRIPTIONS_ENABLED', true);
include_once(dirname(__FILE__).'/load.php');

add_action('after_setup_theme', function() {
	try {
		AitPaypalSubscriptions::getInstance();
	} catch (Exception $e) {
		AitPaypalSubscriptions::error($e);
	}
});

register_activation_hook(__FILE__, function(){
	paypalSubscriptionsCheckCompatibility(true);
	AitCache::clean();
});
add_action('after_switch_theme', function(){
	paypalSubscriptionsCheckCompatibility();
});

add_action('plugins_loaded', function() {
	load_plugin_textdomain('ait-paypal-subscriptions', false, dirname(plugin_basename( __FILE__ )) . '/languages');
}, 11);

function paypalSubscriptionsCheckCompatibility($die = false){
	if ( !defined('AIT_THEME_TYPE') ){	// directory themes
		require_once(ABSPATH . 'wp-admin/includes/plugin.php' );
		deactivate_plugins(plugin_basename( __FILE__ ));
		if($die){
			wp_die('Current theme is not compatible with PayPal Subscriptions plugin :(', '',  array('back_link'=>true));
		} else {
			add_action( 'admin_notices', function(){
				echo "<div class='error'><p>" . __('Current theme is not compatible with PayPal Subscriptions plugin!', 'ait-paypal-subscriptions') . "</p></div>";
			} );
		}
	}
}