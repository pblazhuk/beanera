<?php

// sdk
include_once(dirname(__FILE__).'/vendor/autoload.php');

// admin config
include_once(dirname(__FILE__).'/admin/config.php');

// main class
include_once(dirname(__FILE__).'/classes/main.php');