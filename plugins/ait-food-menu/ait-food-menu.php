<?php

/*
Plugin Name: AIT Food Menu
Plugin URI: http://ait-themes.club
Description: Some description of the plugin functionality
Version: 1.8
Author: AitThemes.Club
Author URI: http://ait-themes.club
Text Domain: ait-food-menu
Domain Path: /languages
License: GPLv2 or later
*/

/* trunk@r433 */
define('AIT_FOOD_MENU_VERSION', '1.8');
define('AIT_FOOD_MENU_ENABLED', true);

AitFoodMenu::init();

class AitFoodMenu {
	protected static $currentTheme;
	protected static $paths;
	protected static $compatibleThemes = array('foodguide', 'skeleton');
	protected static $pageSlug = 'food-menu-options';

	public static function init(){
		self::$currentTheme = sanitize_key(get_stylesheet());
		self::$paths = array(
			'pluginfile' => __FILE__,
			'config' => dirname( __FILE__ ).'/config',
			'templates' => dirname( __FILE__ ).'/templates',
		);

		// WP Plugin functions
		register_activation_hook( __FILE__, array(__CLASS__, 'onActivation') );
		register_deactivation_hook(  __FILE__, array(__CLASS__, 'onDeactivation') );
		add_action('after_switch_theme', array(__CLASS__, 'themeSwitched'));

		add_action('plugins_loaded', array(__CLASS__, 'onLoaded'));

		add_action('ait-after-framework-load', array(__CLASS__, 'onAfterFwLoadCallback'));

		add_action('init', array(__CLASS__, 'onInit'));

		// Metaboxes
		add_action('init', array(__CLASS__, 'addItemMetabox'), 12, 0);

		// Custom columns
		add_filter('manage_ait-food-menu_posts_columns', array(__CLASS__, 'foodMenuChangeColumns'), 10, 2);
		add_action('manage_posts_custom_column', array(__CLASS__, 'cptCustomColumns'), 10, 2);

		// Custom admin display
		add_filter( 'pre_get_posts', array(__CLASS__, 'cptCustomAdminDisplay'));
		add_filter( 'views_edit-ait-food-menu' , array(__CLASS__, 'cptFixAdminCounts'), 10, 1);

		// Template functions
		add_filter('wplatte-get-template-part', array(__CLASS__, 'getTemplate'), 10, 3);
		//add_filter('single_template', array(__CLASS__, 'getSinglePageTemplate'), 10);

		add_action( 'save_post', array(__CLASS__, 'saveMenuMeta'), 10, 3);

		add_action( 'admin_init', array(__CLASS__, 'onAdminInit')); // we need access to $typenow var, it is available after admin_init
		add_filter( 'parse_query', array(__CLASS__, 'applyMenuFilter' ));

		// Ajax functions
		add_action( 'wp_ajax_loadSpecialMenu', array(__CLASS__, 'ajaxLoadSpecialMenu'));
		add_action( 'wp_ajax_nopriv_loadSpecialMenu', array(__CLASS__, 'ajaxLoadSpecialMenu'));

		add_action('admin_head', array(__CLASS__, 'removeMediaButton'));
		// Modify Tiny_MCE init
		add_filter('mce_buttons', array(__CLASS__, 'customformatTinyMCE1' ));
		add_filter('mce_buttons_2', array(__CLASS__, 'customformatTinyMCE2' ));
	}

	/* WP PLUGIN FUNCTIONS */
	public static function onActivation($network_wide){
		if ( $network_wide )
			wp_die('Food Menu Plugin is not allowed for network activation :(');

		if ( !defined('AIT_THEME_CODENAME') || !in_array(AIT_THEME_CODENAME, self::$compatibleThemes) ) {
			require_once(ABSPATH . 'wp-admin/includes/plugin.php' );
			deactivate_plugins(plugin_basename( __FILE__ ));
			wp_die('Current theme is not compatible with Food Menu plugin :(', '',  array('back_link'=>true));
		}

		// some actions on activation
		AitFoodMenu::updateThemeOptions();

		AitFoodMenu::createDbTables();

		AitFoodMenu::updateDirectoryUsers();

		flush_rewrite_rules();
		if(class_exists('AitCache')){
			AitCache::clean();
		}

        AitFoodMenu::updatePllOptions();
	}

	public static function onDeactivation(){
		flush_rewrite_rules();
		if(class_exists('AitCache')){
			AitCache::clean();
		}
	}

	public static function themeSwitched(){
		if ( !defined('AIT_THEME_CODENAME') || !in_array(AIT_THEME_CODENAME, self::$compatibleThemes) ) {
			require_once(ABSPATH . 'wp-admin/includes/plugin.php' );
			deactivate_plugins(plugin_basename( __FILE__ ));
			add_action( 'admin_notices', function(){
				echo "<div class='error'><p>" . _x('Current theme is not compatible with Food Menu plugin!', 'ait-food-menu') . "</p></div>";
			} );
		}
	}

	public static function onLoaded(){
		load_plugin_textdomain('ait-food-menu', false,  dirname(plugin_basename(__FILE__ )) . '/languages');



		$options = get_option('ait-food-menu-plugin');
		if (!$options) {
			// defines default values for options in case this is the first installation
			$options = array(
				'version' => '1.0',
			);
			update_option('ait-food-menu-plugin', $options);
		}

		// plugin upgrade
		if ($options && version_compare($options['version'], AIT_FOOD_MENU_VERSION, '<')) {
			require_once dirname(__FILE__) . '/AitFoodMenuUpgrade.php';
			$upgrade = new AitFoodMenuUpgrade($options);
			if (!$upgrade->upgrade()) // if the version is too old
				return;
		}
	}

	public static function onAfterFwLoadCallback(){
		add_filter('ait-theme-config', array(__CLASS__, 'prepareThemeConfig'));

		// add custom capabilities to user packages
		if(!defined("AIT_PERMISSIONS_MANAGER_ENABLED")){
			add_filter('ait_package_caps', function($wp_avalaible_caps){
				return array_merge($wp_avalaible_caps,	array(
					'ait_toolkit_food_menu_edit_post',
					'ait_toolkit_food_menu_edit_posts',
					'ait_toolkit_food_menu_read_post',
					'ait_toolkit_food_menu_publish_posts',
					'ait_toolkit_food_menu_delete_posts',
					'ait_toolkit_food_menu_delete_published_posts',
					'ait_toolkit_food_menu_edit_published_posts',
					'ait_food_menu_category_assign_terms',
				));
			});
		}
	}

	public static function onInit(){
		AitFoodMenu::registerFoodMenuCpt();
		AitFoodMenu::registerFoodMenuTax();

		// assign food-menu post type capabilities for administrators and -guide roles
		if (!get_option('ait_food_menu_capabilities_assigned', null)) {
			AitFoodMenu::registerAdminCapabilities();

			if(!defined("AIT_PERMISSIONS_MANAGER_ENABLED")){
				AitFoodMenu::registerUserCapabilities();
			}

			update_option( 'ait_food_menu_capabilities_assigned', true );
		}

		AitFoodMenu::addFoodMenuMetabox();

        // add custom tables in demo content
        add_action('ait-create-content-custom-tables', array(__CLASS__, 'createDbTables'), 10, 1);
        add_filter('ait-backup-content-custom-tables', function($tables){
            array_push($tables, 'ait_item_foodmenus');
            array_push($tables, 'ait_foodmenu_dates');
            return $tables;
        });


	}

	public static function onAdminInit(){
		add_action('restrict_manage_posts',  array(__CLASS__, 'menuFilters'));
		add_action('delete_post',  array(__CLASS__, 'deletePost'));
	}

    public static function updatePllOptions()
	{
		$pllOptions = get_option('polylang', '');
		if (!empty($pllOptions)) {

			if (!in_array('ait-food-menu', $pllOptions['post_types'])) {
				$pllOptions['post_types'][] = 'ait-food-menu';
			}

			if (!in_array('ait-food-menu-tags', $pllOptions['taxonomies'])) {
				$pllOptions['taxonomies'][] = 'ait-food-menu-tags';
			}

			update_option( 'polylang', $pllOptions );
		}
	}

	public static function menuFilters($postType){
		if (!$postType == 'ait-food-menu') {
			return;
		}
		$menuTypes = self::getMenuTypes();
		$result = '';

		$result .= '<select name="ait-foodmenu-filter" id="ait-foodmenu-filter">';
		$result .= '<option selected="selected" value="0">All Menu Types</option>';
		foreach( $menuTypes as $id => $title ) {
			$result .= '<option value="'.$id.'">'.$title.'</option>';
		}
		$result .= '</select>';

		echo($result);
	}



	public static function applyMenuFilter( $query ) {
		if( is_admin() AND $query->query['post_type'] == 'ait-food-menu' ) {
			$qv = &$query->query_vars;
			$qv['meta_query'] = array();

			if( !empty( $_GET['ait-foodmenu-filter'] ) ) {
				$qv['meta_query'][] = array(
					'key' => 'ait-food-menu-type',
					'value' => $_GET['ait-foodmenu-filter'],
					'compare' => '=',
				);
			}
		}
	}

	/* CONFIGURATION FUNCTIONS */
	public static function loadThemeConfig($type = 'raw'){
		$config = include self::$paths['config'].'/theme-options.php';
		return $config[$type];
	}

	public static function prepareThemeConfig($config = array()){
		$plugin = AitFoodMenu::loadThemeConfig();

		if(count($config) == 0){
			$theme = self::$currentTheme;
			$config = get_option("_ait_{$theme}_theme_opts", array());
			$plugin = AitFoodMenu::loadThemeConfig('defaults');
		}

		return array_merge($config, $plugin);
	}

	public static function updateThemeOptions(){
		// check if the settings already exists
		$theme = self::$currentTheme;
		$themeOptions = get_option("_ait_{$theme}_theme_opts");
		if(!isset($themeOptions['itemFoodMenu'])){
			$updatedConfig = AitFoodMenu::prepareThemeConfig();
			$theme = self::$currentTheme;
			update_option("_ait_{$theme}_theme_opts", $updatedConfig);
		}
	}

	public static function createDbTables() {
		global $wpdb;
		require_once(ABSPATH . 'wp-admin/includes/upgrade.php' );

		$table_name = $wpdb->prefix . 'ait_foodmenu_dates';
		$charset_collate = $wpdb->get_charset_collate();

		$sql = "CREATE TABLE $table_name (
			id mediumint(9) NOT NULL AUTO_INCREMENT,
			post_id mediumint(9) NOT NULL,
			date_from date,
			date_to date,
			UNIQUE KEY id (id)
		) $charset_collate;";

		dbDelta( $sql );


		$table_name = $wpdb->prefix . 'ait_item_foodmenus';

		$sql = "CREATE TABLE $table_name (
			id mediumint(20) NOT NULL AUTO_INCREMENT,
			item_id varchar(20) NOT NULL,
			menu_id varchar(20) NOT NULL,
			menu_type_id varchar(8) NOT NULL,
			food_type_id varchar(8) NOT NULL,
			UNIQUE KEY id (id)
		) $charset_collate;";

		dbDelta( $sql );

		// add_options( );
	}

	public static function getPluginThemeOptions(){
		$themeOptions = (object)aitOptions()->getOptionsByType('theme');
		return $themeOptions->foodMenu;
	}
	/* CONFIGURATION FUNCTIONS */

	/* CPT AND TAXONOMY FUNCTIONS */
	public static function registerFoodMenuCpt(){
		$labels = array(
			'name'					=> _x( 'Menus', 'post type general name', 'ait-food-menu' ),
			'singular_name'			=> _x( 'Menu', 'post type singular name', 'ait-food-menu' ),
			'menu_name'				=> _x( 'Food Menu', 'admin menu', 'ait-food-menu' ),
			'name_admin_bar'		=> _x( 'Menu', 'add new on admin bar', 'ait-food-menu' ),
			'add_new'				=> _x( 'Add New', 'menu', 'ait-food-menu' ),
			'add_new_item'			=> __( 'Add New Menu', 'ait-food-menu' ),
			'new_item'				=> __( 'New Menu', 'ait-food-menu' ),
			'edit_item'				=> __( 'Edit Menu', 'ait-food-menu' ),
			'view_item'				=> __( 'View Menu', 'ait-food-menu' ),
			'all_items'				=> __( 'All Menus', 'ait-food-menu' ),
			'search_items'			=> __( 'Search Menus', 'ait-food-menu' ),
			'parent_item_colon'		=> __( 'Parent Menus:', 'ait-food-menu' ),
			'not_found'				=> __( 'No menus found.', 'ait-food-menu' ),
			'not_found_in_trash'	=> __( 'No menus found in Trash.', 'ait-food-menu' )
		);

		$capabilities = array(
			'edit_post'					=> "ait_toolkit_food_menu_edit_post",
			'read_post'					=> "ait_toolkit_food_menu_read_post",
			'delete_post'				=> "ait_toolkit_food_menu_delete_post",
			'edit_posts'				=> "ait_toolkit_food_menu_edit_posts",
			'edit_others_posts'			=> "ait_toolkit_food_menu_edit_others_posts",
			'publish_posts'				=> "ait_toolkit_food_menu_publish_posts",
			'read_private_posts'		=> "ait_toolkit_food_menu_read_private_posts",
			'delete_posts'				=> "ait_toolkit_food_menu_delete_posts",
			'delete_private_posts'		=> "ait_toolkit_food_menu_delete_private_posts",
			'delete_published_posts'	=> "ait_toolkit_food_menu_delete_published_posts",
			'delete_others_posts'		=> "ait_toolkit_food_menu_delete_others_posts",
			'edit_private_posts'		=> "ait_toolkit_food_menu_edit_private_posts",
			'edit_published_posts'		=> "ait_toolkit_food_menu_edit_published_posts",
		);

		$supports = array(
			'title',
			'thumbnail',
			// 'tags',
			'editor',
		);

		$args = array(
			'labels'				=> $labels,
			'public'				=> false,
			'publicly_queryable'	=> false,
			'show_ui'				=> true,
			'show_in_menu'			=> true,
			'query_var'				=> true,
			'rewrite'				=> array( 'slug' => 'food-menu' ),
			'capabilities'			=> $capabilities,
			'has_archive'			=> true,
			'hierarchical'			=> false,
			'menu_position'			=> null,
			'menu_icon'				=> plugins_url( 'design/img/food-menu-icon.png', __FILE__ ),
			'supports'				=> $supports,
		);

		register_post_type( 'ait-food-menu', $args );
	}


	/* CPT AND TAXONOMY FUNCTIONS */
    public static function registerFoodMenuTax(){
		$labels = array(
			'name'				=> _x( 'Tags', 'taxonomy general name', 'default' ),
			'singular_name'		=> _x( 'Tag', 'taxonomy singular name', 'default' ),
			'search_items'		=> __( 'Search Tags', 'default' ),
			'all_items'			=> __( 'All Tags', 'default' ),
			'edit_item'			=> __( 'Edit Tag', 'default' ),
			'update_item'		=> __( 'Update Tag', 'default' ),
			'add_new_item'		=> __( 'Add New Tag', 'default' ),
			'new_item_name'		=> __( 'New Tag Name', 'default' ),
			'menu_name'			=> __( 'Tags', 'default' ),
		);

		$capabilities = array(
			'manage_terms'		=> "ait_food_menu_category_manage_terms",
			'edit_terms'		=> "ait_food_menu_category_edit_terms",
			'delete_terms'		=> "ait_food_menu_category_delete_terms",
			'assign_terms'		=> "ait_food_menu_category_assign_terms",
		);

		$args = array(
			'hierarchical'		=> false,
			'labels'			=> $labels,
			'show_ui'			=> true,
			'show_admin_column'	=> true,
			'query_var'			=> true,
			// 'rewrite'			=> array( 'slug' => 'food-menus' ),
			'capabilities'		=> $capabilities,
		);

		register_taxonomy( 'ait-food-menu-tags', 'ait-food-menu', $args );
	}

	/* CAPABILITIES FUNCTIONS */
	public static function registerAdminCapabilities(){
		$capabilities = array(
			/* Food Menu CPT Capabilities */
			"ait_toolkit_food_menu_edit_post",
			"ait_toolkit_food_menu_read_post",
			"ait_toolkit_food_menu_delete_post",
			"ait_toolkit_food_menu_edit_posts",
			"ait_toolkit_food_menu_edit_others_posts",
			"ait_toolkit_food_menu_publish_posts",
			"ait_toolkit_food_menu_read_private_posts",
			"ait_toolkit_food_menu_delete_posts",
			"ait_toolkit_food_menu_delete_private_posts",
			"ait_toolkit_food_menu_delete_published_posts",
			"ait_toolkit_food_menu_delete_others_posts",
			"ait_toolkit_food_menu_edit_private_posts",
			"ait_toolkit_food_menu_edit_published_posts",

			"ait_food_menu_category_manage_terms",
			"ait_food_menu_category_edit_terms",
			"ait_food_menu_category_delete_terms",
			"ait_food_menu_category_assign_terms",
		);

		$role = get_role('administrator');
		foreach($capabilities as $val){
			$role->add_cap($val);
		}
	}

	public static function registerUserCapabilities(){
		$capabilities = array(
			"ait_toolkit_food_menu_edit_post",
			"ait_toolkit_food_menu_edit_posts",
			"ait_toolkit_food_menu_read_post",
			"ait_toolkit_food_menu_publish_posts",
			"ait_toolkit_food_menu_delete_posts",
			"ait_toolkit_food_menu_delete_published_posts",
			"ait_toolkit_food_menu_edit_published_posts",
			"ait_food_menu_category_manage_terms",
			"ait_food_menu_category_edit_terms",
			"ait_food_menu_category_delete_terms",
			"ait_food_menu_category_assign_terms",
		);

		global $wp_roles;
		foreach($wp_roles->role_objects as $key => $role){
			if(strpos($key, 'cityguide_') !== FALSE){
				foreach($capabilities as $capability){
					$role->add_cap( $capability );
				}
			}
		}
	}

	public static function addFoodMenuMetabox(){
		// in this metabox the user will choose the price, description ?
		$params = array(
			'title' => __('Food Menu Options', 'ait-food-menu'),
			'config' => self::$paths['config'].'/ait-food-menu-data.metabox.php',
			'types' => array('ait-food-menu'),
			'saveCallback' => array(__CLASS__, 'saveMetaCallback'),
		);
		AitFoodMenu::addMetabox('ait-food-menu', 'food-menu-data', $params);
	}

	public static function addItemMetabox(){
		if(!class_exists('AitToolkit')) return;
		$manager = AitToolkit::getManager('cpts');
		$allCpts = $manager->getAll();

		$params = array(
			'title' => __('Food Menu Options', 'ait-food-menu'),
			'config' => self::$paths['config'].'/ait-item-food-menu-options.metabox.php',
			'saveCallback' => array(__CLASS__, 'saveMetaCallback'),
		);

		foreach($allCpts as $cpt){
			if($cpt->getId() === 'item'){
				$cpt->addMetabox('food-options', $params);
			}
		}
	}

	public static function addMetabox($post_type, $metabox_slug, $params){
		if(class_exists('AitMetabox')){
			new AitMetabox($post_type, $post_type.'_'.$metabox_slug, $params);
		}
	}

	public static function saveMetaCallback($postId, $post, $metabox, $data){
		switch($post->post_type){
			case 'ait-food-menu':
				if(empty($data)){
					$data = get_post_meta($postId, '_ait-food-menu_food-menu-data', true);
				}
				$price = !empty($data['price']) ? $data['price'] : 0;
				update_post_meta($postId, '_ait-food-menu_food-menu_price', $price);
				update_post_meta($postId, '_ait-food-menu_food-menu-data', $data);
			break;
			case 'ait-item':
				if(empty($data)){
					$data = get_post_meta($postId, '_ait-item_food-options', true);
				}
				$price = !empty($data['averagePrice']) ? $data['averagePrice'] : 0;
				update_post_meta($postId, '_ait-item_food-options_price', $price);
				update_post_meta($postId, '_ait-item_food-options', $data);
			break;
			default:
			break;
		}
	}



	public static function saveMenuMeta( $post_id, $post, $update )	{
	    $slug = 'ait-food-menu';

	    // If this isn't a 'food-menu' post, don't update it.
	    if ( $slug != $post->post_type ) {
	        return;
	    }

	    // save recurring dates
	    if ( isset( $_POST['_ait-food-menu_food-menu-data']['dates'] ) ) {
	    	$infiniteFrom = false;
	    	$minDate = '';
	    	global $wpdb;

			$table_name = $wpdb->prefix . 'ait_foodmenu_dates';

			// remove old dates for current post
			$wpdb->delete( $table_name, array(
					'post_id' => $post_id
				),
				array ('%s')
			);

	    	foreach ($_POST['_ait-food-menu_food-menu-data']['dates'] as $key => $date) {
	    		// if dateFrom is empty record is not valid
	    		// remove such record
	    		if (empty($date['dateFrom'])) {
	    			unset($_POST['_ait-food-menu_food-menu-data']['dates'][$key]);
	    			continue;
	    		}

	    		// get all date intervals with unspecified end and find the final interval
				if (empty($date['dateTo'])) {
	    			// initialize lowest date
		    		if ( empty($minDate) ) {
	    				$infiniteFrom = true;
		    			$minDate = $date['dateFrom'];
		    			continue;
		    		}

		    		if (strtotime($date['dateFrom']) < strtotime($minDate)) {
		    			$minDate = $date['dateFrom'];
		    		}
				} else {
					// manage recurring dates which have end
					// TODO: quick edit doesn't have dates data

					// insert new menu date interval
					$wpdb->insert(
						$table_name,
						array(
							'post_id' => $post_id,
							'date_from' => $date['dateFrom'],
							'date_to' => $date['dateTo'],
						)
					);
					// add_post_meta( $post_id, 'ait-food-menu-date-from', $date['dateFrom'], false );
					// add_post_meta( $post_id, 'ait-food-menu-date-to', $date['dateTo'], false );
				}
			}

			// insert final infinit interval
			if ($infiniteFrom) {
				$wpdb->insert(
					$table_name,
					array(
						'post_id' => $post_id,
						'date_from' => $minDate
					)
				);
			}
	    }

	    // save item foodmenus to custom table
	    if ( isset( $_POST['_ait-food-menu_food-menu-data']['item'] ) ) {
	    	global $wpdb;
			$table_name = $wpdb->prefix . 'ait_item_foodmenus';

			if ( empty($_POST['_ait-food-menu_food-menu-data']['item']) ) {
				// if item/restaurant isn't assigned remove entries from db
				$wpdb->delete(
					$table_name,
					array(
						'menu_id' => $post_id
					),
					'%d'
				);
			} else {
				$result = $wpdb->get_row( $wpdb->prepare(
					"
					SELECT *
					FROM $table_name
					WHERE
						menu_id LIKE %s
					",
					$post_id
				) );


				// check if menu was already added to table
				if ($result == NULL) {
					$wpdb->insert(
						$table_name,
						array(
							'item_id'      => $_POST['_ait-food-menu_food-menu-data']['item'],
							'menu_id'      => $post_id,
							'menu_type_id' => $_POST['_ait-food-menu_food-menu-data']['menuType'],
							'food_type_id' => $_POST['_ait-food-menu_food-menu-data']['foodType'],
						),
						'%s'
					);
				} else {
					// update rows if restaurant, menu type or food type changed
					$result = $wpdb->update(
						$table_name,
						array(
							'item_id'      => $_POST['_ait-food-menu_food-menu-data']['item'],
							'menu_id'      => $post_id,
							'menu_type_id' => $_POST['_ait-food-menu_food-menu-data']['menuType'],
							'food_type_id' => $_POST['_ait-food-menu_food-menu-data']['foodType'],
						),
						array('menu_id' => $post_id),
						'%s',
						'%s'
					);

				}
			}
	    }

	    // save menu item/restaurant
	    if ( isset( $_POST['_ait-food-menu_food-menu-data']['item'] ) ) {
			update_post_meta( $post_id, 'ait-food-menu-item', $_POST['_ait-food-menu_food-menu-data']['item'] );
	    }

	    // save menu type
	    if ( isset( $_POST['_ait-food-menu_food-menu-data']['menuType'] ) ) {
			update_post_meta( $post_id, 'ait-food-menu-type', $_POST['_ait-food-menu_food-menu-data']['menuType'] );
	    }

	    // save food type
	    if ( isset( $_POST['_ait-food-menu_food-menu-data']['foodType'] ) ) {
			update_post_meta( $post_id, 'ait-food-type', $_POST['_ait-food-menu_food-menu-data']['foodType'] );
	    }
	}

	public static function deletePost($post_id)
	{
		// from menu recurring dates table remove records for deleted menu
		global $wpdb;

		$table_name = $wpdb->prefix . 'ait_foodmenu_dates';
		$wpdb->delete( $table_name, array(
				'post_id' => $post_id
			),
			array ('%s')
		);

		// remove information from foodmenus for deleted item or menu
		$table_name = $wpdb->prefix . 'ait_item_foodmenus';
		$wpdb->delete( $table_name, array(
				'item_id' => $post_id
			),
			array ('%s')
		);
		$wpdb->delete( $table_name, array(
				'menu_id' => $post_id
			),
			array ('%s')
		);


	}



	/* METABOXES FUNCTIONS */

	/* CUSTOM ADMIN DISPLAY */
	public static function foodMenuChangeColumns($cols){
		$newCols = array(
			'cb'						=> '<input type="checkbox" />',
			'title'						=> __('Title', 'ait-food-menu'),
			'food-menu-amount'			=> __('Amount', 'ait-food-menu'),
			'food-menu-price'			=> __('Price', 'ait-food-menu'),
			'food-menu-item'			=> __('Restaurant', 'ait-food-menu'),
		);
		return $newCols;
	}



	public static function cptCustomColumns($column, $id){
		switch($column){
			case 'food-menu-item':
				$meta = get_post_meta($id, '_ait-food-menu_food-menu-data', true);
				if(!empty($meta['item'])){
					$item_query = new WP_Query(array('post_type' => 'ait-item', 'p' => $meta['item']));
					$item = reset($item_query->posts);
					echo '<a href="'.get_edit_post_link($item->ID, "").'">'.$item->post_title.'</a>';
				} else {
					echo '-';
				}
			break;
			case 'food-menu-price':
				$meta = get_post_meta($id, '_ait-food-menu_food-menu-data', true);
				echo !empty($meta['price']) ? $meta['price'] : "-";
			break;
			case 'food-menu-amount':
				$meta = get_post_meta($id, '_ait-food-menu_food-menu-data', true);
				echo !empty($meta['amount']) ? $meta['amount'] : "-";
			break;
		}
	}

	public static function cptCustomAdminDisplay($query){
		if($query->is_admin){
			$wp_user = wp_get_current_user();
			if(isset($query->query_vars['post_type'])){
				if($query->query_vars['post_type'] == 'ait-food-menu' || $query->query_vars['post_type'] == 'ait-daily-menu'){
					if(!in_array('administrator', $wp_user->roles)){
						// standard user
						$query->set('author', $wp_user->ID);
					}
				}
			}
		}
		return $query;
	}

	public static function cptFixAdminCounts($views){
		global $current_screen;
		switch( $current_screen->id ) {
			case 'edit-ait-food-menu':
				$views = self::cptFixAdminItemsCount( 'ait-food-menu', $views );
				break;
		}
		return $views;
	}

	public static function cptFixAdminItemsCount($post_type, $views){
		$wp_user = wp_get_current_user();
		if(!in_array('administrator', $wp_user->roles)){
			unset($views['mine']);
			// do the counts query
			$query = new WP_Query(array(
				'post_type' => $post_type,
				'author' => $wp_user->ID
			));
			$views['all'] = preg_replace( '/\(.+\)/U', '('.count($query->posts).')', $views['all'] );

			$query = new WP_Query(array(
				'post_type' => $post_type,
				'author' => $wp_user->ID,
				'post_status' => 'publish'
			));
			$views['publish'] = preg_replace( '/\(.+\)/U', '('.count($query->posts).')', $views['publish'] );

			if(isset($views['draft'])){
				$query = new WP_Query(array(
					'post_type' => $post_type,
					'author' => $wp_user->ID,
					'post_status' => 'draft'
				));
				$views['draft'] = preg_replace( '/\(.+\)/U', '('.count($query->posts).')', $views['draft'] );
			}

			if(isset($views['pending'])){
				$query = new WP_Query(array(
					'post_type' => $post_type,
					'author' => $wp_user->ID,
					'post_status' => 'pending'
				));
				$views['pending'] = preg_replace( '/\(.+\)/U', '('.count($query->posts).')', $views['pending'] );
			}

			if(isset($views['trash'])){
				$query = new WP_Query(array(
					'post_type' => $post_type,
					'author' => $wp_user->ID,
					'post_status' => 'trash'
				));
				$views['trash'] = preg_replace( '/\(.+\)/U', '('.count($query->posts).')', $views['trash'] );
			}

		}
		return $views;
	}
	/* CUSTOM ADMIN DISPLAY */

	/* TEMPLATE FUNCTIONS */
	public static function getTemplate($templates, $slug, $name){
		$ok = true;
		foreach(glob(self::$paths['templates'] . '/*.php') as $file){
			$filename = basename($file, '.php');
			if(!self::contains($slug, $filename)){
				$ok = false;
			}else{
				$ok = true;
				break;
			}
		}

		if(!$ok){
			return $templates;
		}

		// create name of file
		// e.g. 'parts/entry-date-format-<NAME>-loop.php'
		// e.g. 'parts/entry-date-format-<NAME>.php'
		if($name){
			//if(!is_singular()) $templates[] = "{$slug}-{$name}-loop.php";
			$templates[] = "{$slug}-{$name}.php";
		}

		// e.g. 'parts/entry-date-format-loop.php'
		// e.g. 'parts/entry-date-format.php'
		//if(!is_singular()) $templates[] = "{$slug}-loop.php";
			$templates[] = "{$slug}.php";

		$locatedInTheme = locate_template($templates, false, false);


		$pluginDir = self::$paths['templates'];

		if(!$locatedInTheme){ // in theme file does not exist, load it from plugin
			$newTemplate = '';
			foreach($templates as $tmpl){
				$tmpl = basename($tmpl);

				if(file_exists("$pluginDir/$tmpl")){
					$newTemplate = "$pluginDir/$tmpl";
				}else{
					continue;
				}
			}
			if(!$newTemplate){
				trigger_error("Template '$tmpl' does not exist in plugin dir nor theme dir");
			}
			return $newTemplate;
		} else {
			return $locatedInTheme; // exist in theme
		}
	}

	public static function getSinglePageTemplate($single){
		global $post;
		$template = $single;

		/* Checks for single template by post type */
		switch($post->post_type){
			case "ait-food-menu":
				if(file_exists(self::$paths['templates']. '/single-food-menu.php')){
					$template = self::$paths['templates'] . '/single-food-menu.php';
				}
			break;

			break;
			default:
				// $template = $single;
			break;
		}

		return $template;
	}
	/* TEMPLATE FUNCTIONS */




	public static function adminPrintScripts(){

	}
	/* DESIGN FUNCTIONS */




	/* HELPER FUNCTIONS */
	public static function contains($haystack, $needle)
	{
		return strpos($haystack, $needle) !== FALSE;
	}
	/* HELPER FUNCTIONS */



	/**
	 * Returns menu ids by date:
	 * @param  string yyyy-mm-dd
	 * @return array
	 */
	public static function getMenusByDate($date = '')
	{
		global $wpdb;
		$table_name = $wpdb->prefix . 'ait_foodmenu_dates';

		$postids = $wpdb->get_col( $wpdb->prepare(
			"
			SELECT DISTINCT post_id
			FROM $table_name
			WHERE
				( date_from <= %s AND date_to >= %s )
				OR
				( date_from <= %s AND date_to IS NULL )
			",
			$date,
			$date,
			$date
		) );

		return $postids;
	}

	/**
	 * Returns menu query ids by date:
	 * @param  string
	 * @param  string
	 * @param  string yyyy-mm-dd
	 * @return WpLatteQuery
	 */
	public static function getItemSpecialMenu($itemId, $menuTypeId, $date)
	{
		$postIds = self::getMenusByDate($date);
		if (empty($postIds)) {
			$postIds = array(0);
		}

		$metaQuery = array(
			array(
				'key'     => 'ait-food-menu-type',
				'value'   => $menuTypeId,
				'compare' => '='
			),
			array(
				'key'     => 'ait-food-menu-item',
				'value'   => $itemId,
				'compare' => '='
			),
			'relation' => 'AND',
		);

		$args = array(
			'nopaging'   => true,
			'post_type'  => 'ait-food-menu',
			'meta_query' => $metaQuery,
			'post__in'   => $postIds,
		);
		$query = new WpLatteWpQuery($args);
		return $query;
	    // return new WpLatteWpQuery($args);
	}

	public static function getItemMenuByType($itemId, $menuTypeId, $foodTypeId)
	{
		$metaQuery = array(
			array(
				'key'     => 'ait-food-menu-type',
				'value'   => $menuTypeId,
				'compare' => '='
			),
			array(
				'key'     => 'ait-food-type',
				'value'   => $foodTypeId,
				'compare' => '='
			),
			array(
				'key'     => 'ait-food-menu-item',
				'value'   => $itemId,
				'compare' => '='
			),
			'relation' => 'AND',
		);

		$args = array(
			'nopaging'  => true,
			'post_type' => 'ait-food-menu',
			'meta_query' => $metaQuery,
			// 'post__in'  => self::getMenusByDate($date)
		);
	    return new WpLatteWpQuery($args);
	}

	public static function getMenuTypes()
	{
		$menuTypes = $options = aitOptions()->getOptionsByType('theme');
		$menuTypes = $menuTypes['foodMenu']['menuTypes'];
		$options = array();
		if (empty($menuTypes)) {
			return $options;
		}
		foreach ($menuTypes as $menuType) {
			$options[$menuType['id']] = $menuType['title'][AitLangs::getCurrentLocale()];
		}
		return $options;
	}

	public static function getFoodTypes()
	{
		$foodTypes = $options = aitOptions()->getOptionsByType('theme');
		$foodTypes = $foodTypes['foodMenu']['foodTypes'];
		$options = array();
		if (empty($foodTypes)) {
			return $options;
		}
		foreach ($foodTypes as $foodType) {
			$options[$foodType['id']] = $foodType['title'][AitLangs::getCurrentLocale()];
		}
		return $options;
	}


	public static function getFullMenuTypes($item_id, $except)
	{
		global $wpdb;
		$table_name = $wpdb->prefix . 'ait_item_foodmenus';

		$menuTypesList = $wpdb->get_col( $wpdb->prepare(
			"
			SELECT DISTINCT menu_type_id
			FROM $table_name
			WHERE
				item_id LIKE %s
				AND
				menu_type_id NOT LIKE %s
			",
			$item_id, $except
		) );
		return $menuTypesList;
	}


	public static function ajaxLoadSpecialMenu()
	{
		// $context = ''.aitRenderLatteTemplate('/parts/special-food-menu-content.php');
		if(isset($_POST)){
			header( 'Content-Type: application/json; charset=utf-8' );
			$ajaxData = $_POST['data'];
			$query = self::getItemSpecialMenu($ajaxData['postId'], $ajaxData['specialMenuType'], $ajaxData['date']);
			$html = trim(self::renderLatteTemplate('special-food-menu-content', array('query' => $query)));
			$result = array(
				'status' => false,
				'html'   => $html,
				'request'=> $_POST,
			);
			if(!empty($html)){

				$result['status'] = true;
			}
			echo json_encode( $result );
			exit();
		}
	}



	public static function renderLatteTemplate($template, $params = array())
	{
		$path = self::getTemplate(array(), $template, '');

	    AitWpLatte::init();
	    ob_start();
	    WpLatte::render($path, $params);
	    $result = ob_get_contents();
	    ob_end_clean();
	    return $result;
	}


	public static function removeMediaButton()
	{
		if (get_current_screen() !== NULL && get_current_screen()->post_type == 'ait-food-menu') {
	    	remove_action( 'media_buttons', 'media_buttons' );
		}
	}

	/*
	 * Modifying TinyMCE editor to remove unused items.
	 */
	public static function customformatTinyMCE1($buttons) {
		if ( get_current_screen() !== NULL && get_current_screen()->post_type == 'ait-food-menu') {
			$invalid_buttons = array(
				'blockquote', 'alignleft', 'aligncenter', 'alignright', 'wp_more'
			);

			foreach ( $buttons as $button_key => $button_value ) {
	    		if ( in_array( $button_value, $invalid_buttons ) ) {
	      			unset( $buttons[ $button_key ] );
	    		}
			}
		}

		return $buttons;
	}

	/*
	 * Modifying TinyMCE editor to remove unused items.
	 */
	public static function customformatTinyMCE2($buttons) {
		if ( get_current_screen() !== NULL && get_current_screen()->post_type == 'ait-food-menu') {
			$invalid_buttons = array(
				'formatselect', 'alignjustify', 'outdent', 'indent'
			);

			foreach ( $buttons as $button_key => $button_value ) {
	    		if ( in_array( $button_value, $invalid_buttons ) ) {
	      			unset( $buttons[ $button_key ] );
	    		}
			}
		}

		return $buttons;
	}

	// this function updates existing accounts and adds capabilities for new food menu cpts
	public static function updateDirectoryUsers()
	{
		$directoryRoles = getThemeUserRoles();
		foreach ($directoryRoles as $key => $role) {
			foreach (array(
				'ait_toolkit_food_menu_edit_post',
				'ait_toolkit_food_menu_edit_posts',
				'ait_toolkit_food_menu_read_post',
				'ait_toolkit_food_menu_publish_posts',
				'ait_toolkit_food_menu_delete_posts',
				'ait_toolkit_food_menu_delete_published_posts',
				'ait_toolkit_food_menu_edit_published_posts',
				'ait_food_menu_category_assign_terms',
				) as $cap) {
				$role->add_cap($cap);
			}
		}
	}




	public static function getPaths()
	{
		return self::$paths;
	}





}
