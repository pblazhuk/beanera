<?php

/*
 * manages plugin upgrades
 *
 */
class AitFoodMenuUpgrade {
	public $options;

	/*
	 * constructor
	 *
	 */
	public function __construct(&$options) {
		$this->options = &$options;
	}

	/*
	 * upgrades if possible otherwise die to avoid activation
	 *
	 */
	public function upgrade_at_activation() {
		if (!$this->can_upgrade()) {
			ob_start();
			// $this->admin_notices();
			die(ob_get_contents());
		}
	}

	/*
	 * upgrades if possible otherwise returns false to stop plugin loading
	 *
	 * @return bool true if upgrade is possible, false otherwise
	 */
	public function upgrade() {
		if (!$this->can_upgrade()) {
			add_action('all_admin_notices', array(&$this, 'admin_notices'));
			return false;
		}
		return true;
	}


	/*
	 * check if we the previous version is not too old
	 * /!\ never start any upgrade before admin_init as it is likely to conflict with some other plugins
	 *
	 * @return bool true if upgrade is possible, false otherwise
	 */
	public function can_upgrade() {
		add_action('admin_init', array(&$this, '_upgrade'));
		return true;
	}

	/*
	 * displays a notice when ugrading from a too old version
	 *
	 */
	public function admin_notices() {

	}

	/*
	 * upgrades the plugin depending on the previous version
	 *
	 */
	public function _upgrade() {
		foreach (array('1.4') as $version) {
			if (version_compare($this->options['version'], $version, '<')) {
				call_user_func(array(&$this, 'upgrade_' . str_replace('.', '_', $version)));
			}
		}

		$this->options['previous_version'] = $this->options['version']; // remember the previous version of plugin
		$this->options['version'] = AIT_FOOD_MENU_VERSION;
		update_option('ait-food-menu-plugin', $this->options);
	}



	/*
	 * upgrades if the previous version is < 1.4
	 *
	 */
	protected function upgrade_1_4() {
		$directoryRoles = getThemeUserRoles();
		foreach ($directoryRoles as $key => $role) {
			foreach (array(
				'ait_toolkit_food_menu_edit_post',
				'ait_toolkit_food_menu_edit_posts',
				'ait_toolkit_food_menu_read_post',
				'ait_toolkit_food_menu_publish_posts',
				'ait_toolkit_food_menu_delete_posts',
				'ait_toolkit_food_menu_delete_published_posts',
				'ait_toolkit_food_menu_edit_published_posts',
				'ait_food_menu_category_assign_terms',
				) as $cap) {
				$role->add_cap($cap);
			}
			foreach (array(
				'ait_food_menu_category_manage_terms',
				'ait_food_menu_category_edit_terms',
				'ait_food_menu_category_delete_terms',
				) as $cap) {
				$role->remove_cap($cap);
			}

		}
		flush_rewrite_rules(); // rewrite rules have been modified in 1.0
	}






}
