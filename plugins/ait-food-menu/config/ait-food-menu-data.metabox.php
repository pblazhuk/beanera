<?php

return array(

	'item' => array(
		'label' => __('Restaurant', 'ait-food-menu'),
		'type'=> 'posts',
		'cpt' => 'item',
		'default' => '',
		'showCurrentUserPosts' => true,
	),

	'amount' => array(
		'label' => __('Amount', 'ait-food-menu'),
		'type'=> 'string',
		'default' => '',
	),

	'price' => array(
		'label' => __('Price', 'ait-food-menu'),
		'type'=> 'string',
		'default' => '',
	),

	'menuType' => array(
		'label' => __('Menu Type', 'ait-food-menu'),
		'type'=> 'select-dynamic',
		'dataFunction' => 'AitFoodMenu::getMenuTypes',
	),

	'foodType' => array(
		'label' => __('Food Type', 'ait-food-menu'),
		'type'=> 'select-dynamic',
		'dataFunction' => 'AitFoodMenu::getFoodTypes',
	),

	'dates' => array(
		'label' => __('Offer dates', 'ait-food-menu'),
		'type'=> 'clone',
		'max' => 50,
		'help' => __("Menu is available only these dates:", 'ait-food-menu'),
		'items' => array(
			'dateFrom' => array(
				'label' => __('From', 'ait-food-menu'),
				'type'=> 'date',
				'picker' => 'date',
				'default' => 'none',
				'format' => 'D, d M yy',
			),
			'dateTo' => array(
				'label' => __('To', 'ait-food-menu'),
				'help' => __('leave empty date "To" for never ending offer', 'ait-food-menu'),
				'type'=> 'date',
				'picker' => 'date',
				'default' => 'none',
				'format' => 'D, d M yy',
			),
		),
		'default' => array(
			0 => array(
				'dateFrom' => '',
				'dateTo' => '',
			),
		),
	),

);
