<?php

return array(
	'raw' => array(
		'foodMenu' => array(
			'title' => __('Food Menu', 'ait-food-menu'),
			'options' => array(
				'menuTypes' => array(
					'label' => __('Menu Types', 'ait-food-menu'),
					'type'  => 'clone',
					'items' => array(
						'id' => array(
							'type'    => 'hidden',
							'uuid' => true,
							'default' => '',
						),
						'title' => array(
							'label'   =>  __('Title', 'ait-food-menu'),
							'type'    => 'text',
							'default' => '',
						),
					),
					'default' => array(
						array(
							'id' => '11111111',
							'title' => 'A La Carte',
						),
						array(
							'id' => '22222222',
							'title' => 'Daily Menu',
						),
						array(
							'id' => '33333333',
							'title' => 'Breakfast Menu',
						),
					),
				),
				'featuredMenuType' => array(
					'label' =>  __('Default Special Menu Type', 'ait-food-menu'),
					'type' => 'select-dynamic',
					'dataFunction' => 'AitFoodMenu::getMenuTypes',
					'selected' => '22222222',
					'help' =>  __('Refresh the page to see newly added menu types. This might be for example a Daily Menu', 'ait-food-menu'),
				),
				'foodTypes' => array(
					'label' => __('Food Types', 'ait-food-menu'),
					'type'  => 'clone',
					'items' => array(
						'id' => array(
							'type'    => 'hidden',
							'uuid' => true,
							'default' => '',
						),
						'title' => array(
							'label'   =>  __('Title', 'ait-food-menu'),
							'type'    => 'text',
							'default' => '',
						),
					),
					'default' => array(
						array(
							'id' => '11111111',
							'title' => 'Main Meals',
						),
						array(
							'id' => '22222222',
							'title' => 'Soups',
						),
						array(
							'id' => '33333333',
							'title' => 'Beverages',
						),
					),
				),
				'priceRangeIcon' => array(
					'label'    => _x('Price Range Icon', 'Price Range is infographic that says how expensive is restaurant eg. $$$', 'ait-food-menu'),
					'type'     => 'font-awesome-select',
					'default'  => 'fa-usd',
					'help' =>  __('Select symbol of local currency', 'ait-food-menu'),

				),
			),
		),
	),
	'defaults' => array(
		'foodMenu' => array(
			// 'featuredMenuType' => '22222222',
			// 'menuTypes' => array(),

		)
	)
);



