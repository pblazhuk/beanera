<?php

return array(
	'averagePrice' => array(
		'label' => __('Average price category', 'ait-food-menu'),
		'type' => 'range',
		'min' => 1,
		'max' => 5,
		'unit' => '$',
	)
);
