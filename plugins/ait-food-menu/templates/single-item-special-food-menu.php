{*
template parameters:
	$specialMenuType
	$postId
	$priceRating
*}

<div class="foodmenu-special-container">
	<h2>{!$specialMenuType['title']}</h2>

	<div class="foodmenu-special-wrap">
		{includePart portal/parts/special-food-menu-header}

		<div class="foodmenu-special-content">
			<div id="ajax-special-food-menu">
				{includePart portal/parts/special-food-menu-content query => AitFoodMenu::getItemSpecialMenu($postId, $specialMenuType['id'], date('Y-m-d'))}
			</div>
		</div>
	</div>

</div>
