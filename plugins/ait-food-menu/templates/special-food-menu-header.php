
{? wp_enqueue_script('jquery-ui-datepicker')}

<div class="foodmenu-special-header">

	<div class="foodmenu-special-controls">
		<a class="arrow-left" href="#"><i class="fa fa-caret-left"></i></a>
		<input type="text" id="menu-date-input" class="menu-date" readonly>
		<a id="foodmenu-date-picker" class="date-picker" href="#"><i class="fa fa-calendar-o"></i></a>
		<a class="arrow-right" href="#"><i class="fa fa-caret-right"></i></a>
	</div>

	<div class="foodmenu-special-date">
		<span class="alt-menu-date">{date_i18n('j F Y', time())}</span>
	</div>

	<script id="special-foodmenu-script">

			jQuery(document).ready(function(){
				var dpOptions = {
					// showOn: 'button',
					// buttonText: '<i class="fa fa-calendar"></i>',
					showButtonPanel: true,
					dateFormat: 'yy-mm-dd',
					onSelect: function(date){
						refreshSpecialMenu(date);
						var myDate = new Date(date);
						//Set a new var with different format to use
						var newFormat = jQuery.datepicker.formatDate("d MM yy", myDate);
						//Choose the div you want to replace
						jQuery(".foodmenu-special-date").html('<span class="alt-menu-date">'+newFormat+'</span>');
					},
					beforeShow: function(){
						jQuery('#ui-datepicker-div').addClass('foodmenu-datepicker');
					},
					onClose: function(){
						setTimeout(function(){
							jQuery('#ui-datepicker-div').removeClass('foodmenu-datepicker');
						}, 500);

					}
				};
				var dp = jQuery('.menu-date').datepicker(dpOptions);
				dp.datepicker('setDate', new Date());

				jQuery('#foodmenu-date-picker').click(function(e){
					e.preventDefault();
					dp.datepicker('show');
				});

				jQuery('.foodmenu-special-controls .arrow-left').click(function(e){
					e.preventDefault();
					var date = new Date(dp.datepicker('getDate'));
					// date is null on first click

    				date.setDate(date.getDate() - 1);
					dp.datepicker('setDate', date);

					var newFormat = jQuery.datepicker.formatDate("d MM yy", date);
					// jQuery(".alt-menu-date").html(newFormat);
					jQuery(".foodmenu-special-date").html('<span class="alt-menu-date">'+newFormat+'</span>');

					var phpFormat = jQuery.datepicker.formatDate("yy-mm-dd", date);
					refreshSpecialMenu(phpFormat);
				});


				jQuery('.foodmenu-special-controls .arrow-right').click(function(e){
					e.preventDefault();
					var date = new Date(dp.datepicker('getDate'));
					// date is null on first click

    				date.setDate(date.getDate() + 1);
					dp.datepicker('setDate', date);

					var newFormat = jQuery.datepicker.formatDate("d MM yy", date);
					// jQuery(".alt-menu-date").html(newFormat);
					jQuery(".foodmenu-special-date").html('<span class="alt-menu-date">'+newFormat+'</span>');


					var phpFormat = jQuery.datepicker.formatDate("yy-mm-dd", date);
					refreshSpecialMenu(phpFormat);
				});

			});






			function refreshSpecialMenu(date) {
				var data = {
					date: date,
					postId: {$post->id},
					specialMenuType: {$specialMenuType['id']},
				};
				var request_data = {
					data: data,
				};
				ait.ajax.post('loadSpecialMenu', request_data).done(function(data){
					if(data.status == true){
						console.log(data);
						jQuery('#ajax-special-food-menu').html(data.html);

					} else {
						console.log("not success");
					}
				}).fail(function(){
					console.log("fail");
					console.log(data);
				});
			}

	</script>

</div>
