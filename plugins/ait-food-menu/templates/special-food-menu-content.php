{customLoop from $query as $item}
	{var $meta = $item->meta(food-menu-data)}
	<div class="foodmenu-special-item">
		<div class="left-panel">
			{if $meta->amount}
			<div class="ammount">{!$meta->amount}</div>
			{/if}
			<div class="title"><h3>{!$item->title}</h3></div>
			<div class="description">{!strip_shortcodes( $item->content )}</div>
		</div>

		<div class="right-panel">
			{if $item->imageUrl != ''}
			<div class="thumbnail">
				<span class="thumbnail-icon"><i class="fa fa-picture-o"></i></span>
				<div class="thumbnail-wrap"><img src="{imageUrl $item->imageUrl, width => 150, height => 100, crop => 1}" alt="{!$item->title}"></div>
			</div>
			{/if}
			{includePart "parts/food-menu-single-price", price => $meta->price}
		</div>
	</div>
{/customLoop}

{if !$query->havePosts}
	<div class="foodmenu-no-special">
		<i class="fa fa-calendar-times-o"></i><div class="msg"><?php _e("There are no daily menus for current day", 'ait-food-menu') ?></div>
	</div>
{/if}
