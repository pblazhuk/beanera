{*
template parameters:
	$specialMenuType
	$postId

*}

{var $menuTypes = AitFoodMenu::getMenuTypes()}
{var $nonEmptyMenuTypes = AitFoodMenu::getFullMenuTypes($postId, $specialMenuType['id'])}
{var $foodTypes = AitFoodMenu::getFoodTypes()}

{if !empty($nonEmptyMenuTypes)}

<div class="foodmenu-container">
	<div class="foodmenu-header">
		<h2><?php _e('Our Menu', 'ait-food-menu') ?></h2>

		<div class="foodmenu-tabs">
			<div class="optiscroll">
				<div class="optiscroll-content dragscroll">
					<ul>
					{var $isFirst = true}
					{foreach $menuTypes as $id => $menuType}
						{if $id == $specialMenuType['id'] or !in_array($id, $nonEmptyMenuTypes)}
							{? continue}
						{/if}
						<li class="foodmenu-tab-item"><a href="#foodmenu-{$id}" class="{if $isFirst}active{var $isFirst = false}{/if}">{!$menuType}</a></li>
					{/foreach}
					</ul>
				</div>
			</div>
		</div>
	</div>

	<div class="foodmenu-tabs-content collapsed-off" data-maxheight="510">
	{var $isFirst = true}
	{foreach $menuTypes as $id => $menuType}
		{if $id == $specialMenuType['id'] or !in_array($id, $nonEmptyMenuTypes)}
			{? continue}
		{/if}

		<div id="foodmenu-{$id}" class="foodmenu-menu-type {if $isFirst}active{var $isFirst = false}{/if}">
			{var $allMenusTags = array()}



			{capture $foodMenuCapture}
        	{foreach $foodTypes as $foodTypeId => $foodType}
				{var $query = AitFoodMenu::getItemMenuByType($postId, $id, $foodTypeId)}
				{if $query->havePosts}

					<div class="foodmenu-food-type">
						<h3>{!$foodType}</h3>

						<div class="foodmenu-content">
						{var $i = 0}
						{customLoop from $query as $item}

						{var $tags = wp_get_post_terms($item->id, 'ait-food-menu-tags', array("fields" => "ids"))}
						{var $allMenusTags = array_merge($allMenusTags, $tags)}


							<div class="foodmenu-item" data-tags="{json_encode($tags)}">
								{var $meta = $item->meta('food-menu-data')}
								{if $item->imageUrl != ''}
								<div class="thumbnail">
									<a href="{$item->imageUrl}" target="_blank" data-rel="foodmenu-{$id}-gallery">
										<img src="{imageUrl $item->imageUrl, width => 125, height => 125, crop => 1}" alt="{!$item->title}">
									</a>
								</div>
								{/if}
								<div class="data">
									<h3>{!$item->title}</h3>
									<div class="desc">{!strip_shortcodes($item->content)}</div>
									{if $meta->amount}<div class="ammount">{!$meta->amount}</div>{/if}
									<div class="price">{includePart "parts/food-menu-single-price", price => $meta->price}</div>
								</div>
							</div>
						{/customLoop}
						</div>
					</div>
				{/if}

			{/foreach}
			{/capture}
    		{includePart "parts/food-menu-tags-filter" tags => $allMenusTags, taxonomy => 'ait-food-menu-tags', filterId => $id}

   			{!$foodMenuCapture}

		</div>

	{/foreach}



	</div>

	<div class="foodmenu-toggle collapsed-toggle collapsed-off">
		<span><?php _e('View Full Menu', 'ait-food-menu') ?></span>
	</div>

	<script type="text/javascript">

		jQuery(document).ready(function() {

			jQuery(".foodmenu-tab-item a").on('click', function(e) {
				e.preventDefault();
				changeTab(jQuery(this));
			});

		});

		jQuery(window).load(function() {

			toggleCollapsed(jQuery(".foodmenu-tabs-content"));

		});

		function changeTab(elm) {
			var target = elm.attr('href');

			elm.closest('ul').find('a').removeClass('active');
			elm.addClass('active');

			jQuery(target).parent().find('> div').removeClass('active');
			jQuery(target).addClass('active');
		}

		function toggleCollapsed(elm) {
			var maxHeight = elm.data('maxheight');
			var toggle = elm.parent().find('.collapsed-toggle');

			var expand = function() {
				elm.toggleClass('collapsed-on collapsed-off');
				toggle.toggleClass('collapsed-on collapsed-off');
			}

			if (elm.outerHeight(true) > maxHeight)
				expand();

			toggle.on('click', function() {
				expand();
			});
		}
	</script>
</div>
{/if}
