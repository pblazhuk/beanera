{* ACCEPTED PARAMETERS *}

{var $class  = isset($class) ? $class : ''}
{var $type   = isset($type) ? $type : 'light'}
{var $rating = isset($rating) ? $rating : 3}

{* VARIABLES *}
{var $symbol = isset($options->theme->foodMenu->priceRangeIcon) ? $options->theme->foodMenu->priceRangeIcon : 'fa-usd'}

{var $count  = 5}

{if $rating}
	<div class="foodmenu-price-rating {$type} {$class}">
		{for $i = 1; $i <= $count; $i++}
			<span class="foodmenu-price-symbol {if $i <= $rating}on{else}off{/if}"><i class="fa {$symbol}"></i></span>
		{/for}
	</div>
{/if}
