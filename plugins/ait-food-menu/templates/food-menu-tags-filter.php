{*
template parameters:
	$filterId
    $taxonomy
*}

{var $tags = get_terms($taxonomy, array('include' => $tags))}

    {if $tags}
<div class="filters-container" data-filter-id="{!$filterId}">
        <ul class="filters">
            <li class="filter-clear">
                <div class="filter-clear-container">
                    <a href="#"><span class="clear-text"><i class="fa fa-filter"></i></span></a>
                </div>
            </li>
            {foreach $tags as $tag}
                <li class="filter-tag" data-filter="{$tag->term_id}">
                    <div class="filter-enable-container">
                        <a class="filter-enable" href="#">{!$tag->name}</a>
                    </div>
                    <div class="filter-disable-container">
                        <span class="filter-text">{!$tag->name}</span>
                        <a class="filter-disable" href="#"><i class="fa fa-close"></i></a>
                    </div>
                </li>
            {/foreach}
        </ul>
</div>
    {/if}


<script id="food-menu-tags-filter-{!$filterId}-script">
jQuery(document).ready(function(){
	var $filters = jQuery('#foodmenu-{!$filterId} .filters-container ul.filters');
	$filters.find('li:not(.filter-clear)').each(function(){
		jQuery(this).find('a.filter-enable').on('click', function(e){
			e.preventDefault();
			jQuery(this).parent().parent().addClass('filter-enabled');
			filterContents({!$filterId});
		});
		jQuery(this).find('.filter-disable-container').on('click', function(e){
			e.preventDefault();
			jQuery(this).parent().removeClass('filter-enabled');
			filterContents({!$filterId});
		});
	});
	$filters.find('li.filter-clear a').on('click', function(e){
		e.preventDefault();
		// animateMenuContent();
		$filters.find('li.filter-enabled').removeClass('filter-enabled');
		$filters.find('li:not(.filter-clear)').show();

		filterContents({!$filterId})
		

		jQuery('#foodmenu-{!$filterId} .filter-clear .fa').removeClass('fa-close').addClass('fa-filter');
		jQuery('#foodmenu-{!$filterId}').removeClass('filter-on');
	});

});
function filterContents(elementId){
	animateMenuContent();

	var $filters = jQuery('#foodmenu-'+elementId+' .filters-container ul.filters');
	var allFilters = [];

	$filters.find('li.filter-enabled').each(function(){
		var currentFilter = jQuery(this).attr('data-filter');
		allFilters.push(parseInt(currentFilter));
	});

	if(allFilters.length > 0) {
		console.log(jQuery('#foodmenu-'+elementId+' .foodmenu-tabs-content'));
		jQuery('#foodmenu-'+elementId).addClass('filter-on');
		jQuery('#foodmenu-'+elementId+' .filters-container .filter-clear .fa').removeClass('fa-filter').addClass('fa-close');
	} else {
		jQuery('#foodmenu-'+elementId).removeClass('filter-on');
		jQuery('#foodmenu-'+elementId+' .filters-container .filter-clear .fa').removeClass('fa-close').addClass('fa-filter');
	}

    // iterate on each foodmenu and check if they contain at least one of active filters
    jQuery('#foodmenu-'+elementId+' .foodmenu-item').each(function(){
        var menuTags = jQuery(this).data('tags');
        var visibility = false;

        if (allFilters.length === 0) {
			// all menu items are visible
            visibility = true;
        }

        var i;
        for (i in allFilters) {
            if (jQuery.inArray(allFilters[i], menuTags) > -1) {
                visibility = true;
                break;
            }
        }

        if (visibility) {
            jQuery(this).removeClass('invisible');
        } else {
            jQuery(this).addClass('invisible');
        }

    });
	
	jQuery('#foodmenu-'+elementId+' .foodmenu-food-type').each(function(){
		if (jQuery(this).find('.foodmenu-item:not(.invisible)').length === 0) {
			jQuery(this).hide();
		} else {
			jQuery(this).show();
		}
	});
}




function animateMenuContent() {
	jQuery('.foodmenu-content').addClass('animate');
	setTimeout(function(){
		jQuery('.foodmenu-content').removeClass('animate');
	}, 100);
}


</script>
