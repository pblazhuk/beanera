��          \      �       �   �   �      g     y  $   �     �     �  
   �  t  �  	  <  !   F     h  d   �     �  )   �     (                                       Adds simple announcements bar at the top of page on the frontend. You have full control over HTML and CSS. This plugin works only on themes from AIT for now. Announcements Bar End Date Expired. Bar is no longer displayed. General Save Options Start Date Project-Id-Version: ait-announcements-bar
POT-Creation-Date: 2017-02-24 12:56:54+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2017-03-01 06:26-0500
Language-Team: Ukrainian
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
Language: uk
 Додає просту смугу повідомлень у верхній частині сторінок сайту. У вас є повний контроль над HTML і CSS. Поки цей плагін працює тільки на темах від AIT. Смуга повідомлень Дата закінчення Термін дії закінчився. Смуга більше не відображається. Загальний Зберегти налаштування Дата початку 