��          t      �         �        �     �  $   �     �     �     �       
   &     1  )  N  �   x     <     O  '   [  	   �     �     �     �     �     �               	                 
                            Adds simple announcements bar at the top of page on the frontend. You have full control over HTML and CSS. This plugin works only on themes from AIT for now. Announcements Bar End Date Expired. Bar is no longer displayed. General HTML Layout for your announcement Save Options Start Date Styles for your announcement Project-Id-Version: ait-announcements-bar
POT-Creation-Date: 2017-02-24 12:56:54+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2017-08-28 06:34-0400
Language-Team: French
Plural-Forms: nplurals=2; plural=(n > 1);
Language: fr_FR
 Ajoute une barre d'annonces simple en haut de la page de l'interface. Vous avez un contrôle complet sur le code HTML et CSS. Ce plugin fonctionne seulement sur les thèmes de AIT pour le moment. Barre des Annonces Date de Fin Expiré. La Barre n'est plus affichée. Général HTML Mise en page pour votre annonce Options d'enregistrement Date de Début Styles pour votre annonce 