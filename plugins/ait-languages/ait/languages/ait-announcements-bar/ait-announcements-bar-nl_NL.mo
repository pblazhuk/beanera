��          t      �         �        �     �  $   �     �     �     �       
   &     1  )  N  �   x     +  	   >  ,   H     u     ~     �     �  
   �     �               	                 
                            Adds simple announcements bar at the top of page on the frontend. You have full control over HTML and CSS. This plugin works only on themes from AIT for now. Announcements Bar End Date Expired. Bar is no longer displayed. General HTML Layout for your announcement Save Options Start Date Styles for your announcement Project-Id-Version: ait-announcements-bar
POT-Creation-Date: 2017-02-24 12:56:54+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2017-08-28 06:34-0400
Language-Team: Dutch
Plural-Forms: nplurals=2; plural=(n != 1);
Language: nl_NL
 Voegt een simpele aankondigingenbalk aan de top van de pagina op het voorkant. Je hebt volledige controle over HTML en CSS. Deze plugin werkt voorlopig alleen op thema's van AIT. Aankondigingenbalk Einddatum Verlopen. De balk wordt niet langer getoond. Algemeen HTML Ontwerp van je publicatie Opties opslaan Startdatum Stijlen voor jouw publicatie 