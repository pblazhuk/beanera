��          t      �         �        �     �  $   �     �     �     �       
   &     1  *  N  �   y          4  -   =  	   k     u     z     �  
   �     �               	                 
                            Adds simple announcements bar at the top of page on the frontend. You have full control over HTML and CSS. This plugin works only on themes from AIT for now. Announcements Bar End Date Expired. Bar is no longer displayed. General HTML Layout for your announcement Save Options Start Date Styles for your announcement Project-Id-Version: ait-announcements-bar
POT-Creation-Date: 2017-02-24 12:56:54+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2017-08-28 06:34-0400
Language-Team: German
Plural-Forms: nplurals=2; plural=(n != 1);
Language: de_DE
 Fügt eine einfache Benachrichtigungsleiste am oberen Seitenrand auf dem Frontend ein. Sie können HTML und CSS frei gestalten. Dieses Plugin funktioniert nur mit Benachrichtigungsleiste Enddatum Abgelaufen. Leiste wird nicht mehr angezeigt. Allgemein HTML Layout für Ihre Mitteilung Optionen speichern Startdatum Stile für Ihre Mitteilung 