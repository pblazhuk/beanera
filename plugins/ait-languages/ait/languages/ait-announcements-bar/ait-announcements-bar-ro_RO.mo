��          t      �         �        �     �  $   �     �     �     �       
   &     1  X  N  �   �     S     a  !   p     �     �     �     �     �     �               	                 
                            Adds simple announcements bar at the top of page on the frontend. You have full control over HTML and CSS. This plugin works only on themes from AIT for now. Announcements Bar End Date Expired. Bar is no longer displayed. General HTML Layout for your announcement Save Options Start Date Styles for your announcement Project-Id-Version: ait-announcements-bar
POT-Creation-Date: 2017-02-24 12:56:54+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2017-09-18 08:27-0400
Language-Team: Romanian
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : (n==0 || (n%100>0 && n%100<20)) ? 1 : 2);
Language: ro_RO
 Adaugă simplu anunturile  la bara din partea de sus a paginii pe frontend. Ai control deplin asupra HTML şi CSS. In prezent acest plug-funcţionează numai pe teme AIT . Bara Anunturi Data Sfârșit Expirat. Bara nu se mai afiseaza. General HTML Aspect pentru anunţul tău Salvare Opţiuni Data Inceput Stiluri pentru anunțul tău 