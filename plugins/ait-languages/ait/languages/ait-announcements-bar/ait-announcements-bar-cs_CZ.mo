��          t      �         �        �     �  $   �     �     �     �       
   &     1  D  N  �   �     A     S  .   d     �     �      �     �     �     �               	                 
                            Adds simple announcements bar at the top of page on the frontend. You have full control over HTML and CSS. This plugin works only on themes from AIT for now. Announcements Bar End Date Expired. Bar is no longer displayed. General HTML Layout for your announcement Save Options Start Date Styles for your announcement Project-Id-Version: ait-announcements-bar
POT-Creation-Date: 2017-02-24 12:56:54+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2017-09-18 08:27-0400
Language-Team: Czech
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
Language: cs_CZ
 Přidá jednoduchý oznámovací panel v horní části web stránky. Máte plnou kontrolu nad HTML a CSS. Tento plugin funguje pouze prozatím jen se šablonami Ait-Themes. Oznamovací panel Datum ukončení Vypršela platnost. Panel se již nezobrazuje. Hlavné HTML Rozložení pro vaše oznámení Uložit nastavení Počáteční datum Styly pro vaše oznámení 