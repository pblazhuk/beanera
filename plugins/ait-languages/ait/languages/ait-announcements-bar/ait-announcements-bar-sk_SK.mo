��          t      �         �        �     �  $   �     �     �     �       
   &     1  E  N  �   �     *     =  #   O     s     {     �     �     �     �               	                 
                            Adds simple announcements bar at the top of page on the frontend. You have full control over HTML and CSS. This plugin works only on themes from AIT for now. Announcements Bar End Date Expired. Bar is no longer displayed. General HTML Layout for your announcement Save Options Start Date Styles for your announcement Project-Id-Version: ait-announcements-bar
POT-Creation-Date: 2017-02-24 12:56:54+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2017-08-28 06:36-0400
Language-Team: Slovak
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
Language: sk_SK
 Pridáva jednoduchú oznamovaciu lištu na vrch web stránky. Máte plnú kontrolu nad HTML a CSS. Tento plugin zatiaľ funguje len s témami od AIT. Oznamovacia lišta Dátum ukončenia Expirované. Lišta sa nezobrazuje. Hlavné HTML Layout pre vaše oznámenie Uložiť nastavenia Dátum začatia Štýly pre vaše oznámenie 