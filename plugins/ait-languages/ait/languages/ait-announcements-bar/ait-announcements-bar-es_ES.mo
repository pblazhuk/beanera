��          t      �         �        �     �  $   �     �     �     �       
   &     1  +  N  �   z     ^     v     �     �     �     �     �     �     �               	                 
                            Adds simple announcements bar at the top of page on the frontend. You have full control over HTML and CSS. This plugin works only on themes from AIT for now. Announcements Bar End Date Expired. Bar is no longer displayed. General HTML Layout for your announcement Save Options Start Date Styles for your announcement Project-Id-Version: ait-announcements-bar
POT-Creation-Date: 2017-02-24 12:56:54+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2017-08-28 06:36-0400
Language-Team: Spanish
Plural-Forms: nplurals=2; plural=(n != 1);
Language: es_ES
 Añade una sencilla barra para notificaciones en la parte superior de la página de la interfaz. Tienes control total sobre los lenguages HTML y CSS. Por ahora este complemento funciona únicamente en plantillas y temas de AIT. Barra de notificaciones Fecha de finalización La barra ha expirado. General HTML Diseño para tu anuncio Opciones de guardado Fecha de inicio Estilos para tu anuncio 