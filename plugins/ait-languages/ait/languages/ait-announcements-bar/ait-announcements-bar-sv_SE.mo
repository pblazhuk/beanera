��          t      �         �        �     �  $   �     �     �     �       
   &     1  +  N  �   z       	   .  &   8     _     h     m     �  
   �     �               	                 
                            Adds simple announcements bar at the top of page on the frontend. You have full control over HTML and CSS. This plugin works only on themes from AIT for now. Announcements Bar End Date Expired. Bar is no longer displayed. General HTML Layout for your announcement Save Options Start Date Styles for your announcement Project-Id-Version: ait-announcements-bar
POT-Creation-Date: 2017-02-24 12:56:54+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2017-08-28 06:36-0400
Language-Team: Swedish
Plural-Forms: nplurals=2; plural=(n != 1);
Language: sv_SE
 Lägger till enkla meddelandefält överst på framsidan. Du har full kontroll över HTML och CSS. Denna plugin fungerar för tillfället bara på teman från AIT. Meddelandefält Slutdatum Utgånget. Fältet visas inte längre. Allmänt HTML Layout för dina meddelanden Spara alternativ Startdatum Utformning av dina meddelanden 