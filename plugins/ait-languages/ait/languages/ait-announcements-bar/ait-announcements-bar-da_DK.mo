��          |      �          �   !     �     �     �  $   �                    -  
   :     E  *  b  �   �            	   "  !   ,     N     W     \     x  
   �     �               
              	                              Adds simple announcements bar at the top of page on the frontend. You have full control over HTML and CSS. This plugin works only on themes from AIT for now. Announcements Bar CSS End Date Expired. Bar is no longer displayed. General HTML Layout for your announcement Save Options Start Date Styles for your announcement Project-Id-Version: ait-announcements-bar
POT-Creation-Date: 2017-02-24 12:56:54+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2017-04-12 08:47-0400
Language-Team: Danish
Plural-Forms: nplurals=2; plural=(n != 1);
Language: da_DK
 Tilføj simpel meddelelse på toppen af siden. Du har fuld kontrol over HTML og CSS. Dette plugin virker kun med temaer fra AIT. Meddelelses Bar CSS Slut Dato Udløbet. Feltet vises ikke mere. Generelt HTML Udseende for din meddelelse Gem Indstillinger Start Dato Styling for din meddelelse 