��          t      �         �        �     �  $   �     �     �     �       
   &     1  .  N  �   }          2  /   >     n     t     y     �     �     �               	                 
                            Adds simple announcements bar at the top of page on the frontend. You have full control over HTML and CSS. This plugin works only on themes from AIT for now. Announcements Bar End Date Expired. Bar is no longer displayed. General HTML Layout for your announcement Save Options Start Date Styles for your announcement Project-Id-Version: ait-announcements-bar
POT-Creation-Date: 2017-02-24 12:56:54+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2017-08-28 06:35-0400
Language-Team: Portuguese
Plural-Forms: nplurals=2; plural=(n != 1);
Language: pt_PT
 Adiciona uma simples barra de anúncios no topo da página pública. Tem controlo total sobre o HTML e CSS. Este plug-in só funciona com temas da AIT por agora. Barra de Anúncios Data de Fim Expirado. A barra já não está a ser exibida. Geral HTML Layout para o seu anúncio Guardar Opções Data de Início Estilos para o seu anúncio 