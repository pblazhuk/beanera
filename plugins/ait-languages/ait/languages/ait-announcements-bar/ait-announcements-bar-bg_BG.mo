��          |      �          �   !     �     �     �  $   �                    -  
   :     E  -  b    �  "   �     �     �  D   �     1     :  +   ?  &   k     �  +   �               
              	                              Adds simple announcements bar at the top of page on the frontend. You have full control over HTML and CSS. This plugin works only on themes from AIT for now. Announcements Bar CSS End Date Expired. Bar is no longer displayed. General HTML Layout for your announcement Save Options Start Date Styles for your announcement Project-Id-Version: ait-announcements-bar
POT-Creation-Date: 2017-02-24 12:56:54+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2017-03-01 06:23-0500
Language-Team: Bulgarian
Plural-Forms: nplurals=2; plural=(n != 1);
Language: bg_BG
 Добавя лента за прости съобщения от горе на страницата в клиентската част. Вие имате пълен контрол над HTML и CSS. Този плъгин работи само с шаблони на Ait за сега. Лента за съобщения CSS Крайна дата С изтекъл срок. Лентата не се показва. Общи HTML Стилове за вашата обява Запазване на опциите Начална дата Стилове за вашата обява 