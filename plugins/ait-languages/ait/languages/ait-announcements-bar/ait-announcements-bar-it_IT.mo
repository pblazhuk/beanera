��          t      �         �        �     �  $   �     �     �     �       
   &     1  +  N  �   z     2     F  +   S          �     �     �     �     �               	                 
                            Adds simple announcements bar at the top of page on the frontend. You have full control over HTML and CSS. This plugin works only on themes from AIT for now. Announcements Bar End Date Expired. Bar is no longer displayed. General HTML Layout for your announcement Save Options Start Date Styles for your announcement Project-Id-Version: ait-announcements-bar
POT-Creation-Date: 2017-02-24 12:56:54+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2017-08-28 06:35-0400
Language-Team: Italian
Plural-Forms: nplurals=2; plural=(n != 1);
Language: it_IT
 Aggiungi una semplice barra di annunci nella parte superiore della pagina sul frontend. Hai il pieno controllo sull'HTML e CSS. Per adesso il plugin funziona solo con temi AIT-themes. Barra degli annunci Data di fine Scaduto. La barra non è più visualizzata. Generale HTML Layout per il tuo annuncio Salva le Opzioni Data di inizio Stili per il tuo annuncio 