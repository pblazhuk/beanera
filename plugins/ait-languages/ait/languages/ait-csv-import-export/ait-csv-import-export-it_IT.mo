��          �            x     y     �  +   �     �  
   �     �     �               2     ;     Y     m  W   r     �  +  �          0  (   J     s     �     �     �     �  "   �     �  #   �          %  o   *     �     
                   	                                                       CSV Import/Export Column name in CSV file Description for each type with examples: %s Download sample CSV Export CSV Export to CSV Import from CSV Import from file Invalid date format! Language Missing separator information Select type of data Type You can import items from CSV or export items to CSV file in your directory-based theme Your file is missing column Project-Id-Version: ait-csv-import-export
POT-Creation-Date: 2016-10-04 13:36:12+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2017-08-28 06:35-0400
Language-Team: Italian
Plural-Forms: nplurals=2; plural=(n != 1);
Language: it_IT
 Importazione/Esportazione CSV Nome colonna nel file CSV Descrizione per ogni tipo con esempi: %s Scarica esempio CSV Esporta CSV Esporta a CSV Importa da CSV Importa file da Formato della data non consentito! Lingua Informazioni di separatore mancanti Seleziona tipo di dati Tipo Puoi importare elementi da file CSV o esportare elementi su file CSV all'interno del tuo tema di tipo directory Al tuo file manca una colonna 