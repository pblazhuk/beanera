��          �            x     y     �  +   �     �  
   �     �     �               2     ;     Y     m  W   r     �  *  �          $  *   >     i          �     �     �     �     �     �     �       a        w     
                   	                                                       CSV Import/Export Column name in CSV file Description for each type with examples: %s Download sample CSV Export CSV Export to CSV Import from CSV Import from file Invalid date format! Language Missing separator information Select type of data Type You can import items from CSV or export items to CSV file in your directory-based theme Your file is missing column Project-Id-Version: ait-csv-import-export
POT-Creation-Date: 2016-10-04 13:36:12+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2017-04-12 08:47-0400
Language-Team: Danish
Plural-Forms: nplurals=2; plural=(n != 1);
Language: da_DK
 CSV-Import/eksport Kolonnenavnet i CSV-filen Beskrivelse af hver type med eksempler: %s Download CSV eksempel Eksporter CSV Eksporter til CSV Import fra CSV Importer fra fil Ugyldigt datoformat! Sprog Manglende separator oplysninger Vælg typen data type Type Du kan importere emnerne fra CSV eller eksportere emner til CSV-filen i dit katalog-baseredd tema Din fil mangler kolonne/r 