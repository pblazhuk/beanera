��          �            x     y     �  +   �     �  
   �     �     �               2     ;     Y     m  W   r     �  -  �  3     *   H  =   s  #   �     �     �          +  2   F     y  ;   �  -   �     �  �   �  /   �     
                   	                                                       CSV Import/Export Column name in CSV file Description for each type with examples: %s Download sample CSV Export CSV Export to CSV Import from CSV Import from file Invalid date format! Language Missing separator information Select type of data Type You can import items from CSV or export items to CSV file in your directory-based theme Your file is missing column Project-Id-Version: ait-csv-import-export
POT-Creation-Date: 2016-10-04 13:36:12+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2016-12-05 07:13-0500
Language-Team: Bulgarian
Plural-Forms: nplurals=2; plural=(n != 1);
Language: bg_BG
 CSV импортиране/експортиране Име на колона в CSV файла Описание за всеки тип с примери: %s Свалете примерен CSV Експорт в CSV Експортни като CVS Импортиране от CSV Импорт от файл Невалиден формат на датата! Език Липсва информация за разделител Изберете тип на елемента Тип Вие можете да импортирате от CSV или да експортирате елементи в CSV файл във Вашия CityGuide шаблон Вашият файл липсва колона 