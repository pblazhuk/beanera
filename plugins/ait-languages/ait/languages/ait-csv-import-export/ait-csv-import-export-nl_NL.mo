��          �            x     y     �  +   �     �  
   �     �     �               2     ;     Y     m  W   r     �  )  �          "  .   ;     j     �     �     �     �     �     �      �          '  h   ,     �     
                   	                                                       CSV Import/Export Column name in CSV file Description for each type with examples: %s Download sample CSV Export CSV Export to CSV Import from CSV Import from file Invalid date format! Language Missing separator information Select type of data Type You can import items from CSV or export items to CSV file in your directory-based theme Your file is missing column Project-Id-Version: ait-csv-import-export
POT-Creation-Date: 2016-10-04 13:36:12+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2017-08-28 06:34-0400
Language-Team: Dutch
Plural-Forms: nplurals=2; plural=(n != 1);
Language: nl_NL
 CSV Import/Export Kolomnaam in CSV bestand Beschrijving voor elk type met voorbeelden: %s Download voorbeeld CSV Exporteer CSV Exporteren naar CSV Importeren uit CSV Importeren uit bestand Ongeldige gegevensindeling! Taal Ontbrekende separator informatie Selecteer type gegevens Type U kunt items van CSV importeren of items naar een CSV-bestand exporteren in uw thema gebaseerd op uw map Uw bestand mist een kolom 