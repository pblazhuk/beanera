��          �            x     y     �  +   �     �  
   �     �     �               2     ;     Y     m  W   r     �  }  �     d     u  !   �     �  	   �     �     �     �     �                ;     N  a   R      �     
                   	                                                       CSV Import/Export Column name in CSV file Description for each type with examples: %s Download sample CSV Export CSV Export to CSV Import from CSV Import from file Invalid date format! Language Missing separator information Select type of data Type You can import items from CSV or export items to CSV file in your directory-based theme Your file is missing column Project-Id-Version: ait-csv-import-export
POT-Creation-Date: 2016-10-04 13:36:12+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2017-09-18 08:27-0400
Language-Team: Serbian (Latin)
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
Language: sr_RS
 Uvoz/izvoz u CSV Naziv kolone u CSV datoteci Opis za svaki tip sa uzorcima: %s Preuzmite uzorak CSV Izvoz CSV Izvoz u CSV Uvoz iz CSV Uvoz iz datoteke Pogrešan format datuma! Jezik Nedostaje informacija separatora Odaberi tip datuma Tip Možete izvesti stavke sa CSV ili uvesti stavke na CSV datoteku na vaš direktorijum na bazi teme Vašoj datoteci nedostaje kolona 