��          �            x     y     �  +   �     �  
   �     �     �               2     ;     Y     m  W   r     �  *  �          #  -   =     k     �     �     �     �     �     �     �          3  h   7      �     
                   	                                                       CSV Import/Export Column name in CSV file Description for each type with examples: %s Download sample CSV Export CSV Export to CSV Import from CSV Import from file Invalid date format! Language Missing separator information Select type of data Type You can import items from CSV or export items to CSV file in your directory-based theme Your file is missing column Project-Id-Version: ait-csv-import-export
POT-Creation-Date: 2016-10-04 13:36:12+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2017-08-28 06:34-0400
Language-Team: German
Plural-Forms: nplurals=2; plural=(n != 1);
Language: de_DE
 CSV Import/Export Spaltentitel in CSV-Datei Beschreibung für jede Art mit Beispielen: %s Beispiel-CSV herunterladen CSV exportieren Als CSV exportieren Aus CSV importieren Aus Datei importieren Ungültiges Datumsformat! Sprache Die Separatorinformation fehlt Wählen Sie die Art der Daten Typ In dein verzeichnisbasiertes Theme kannst du Elemente aus CSV importieren oder als CSV-Datei exportieren In Ihrer Datei fehlt eine Spalte 