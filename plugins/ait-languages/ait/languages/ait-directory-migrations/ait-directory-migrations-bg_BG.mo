��          �      L      �     �  ;   �  F        V  ;   g  S   �     �  b   �  	   b     l     z     �     �  	   �  	   �     �     �     �  0  �     �  �     �   �     !  ^   ;  �   �     >  �   Q       '   !  '   I     q     �     �     �     �     �     �                            
                            	                                            !!! Important !!! Backup your database before starting the migration process. Best way is to make an <strong>SQL dump</strong> of complete database. Congratulations! Everything was migrated. For more information, check the %s If you aren´t familiar with database backup, please contact your hosting provider. Migrate Migrate items, categories, locations from Directory and Business Finder themes to City Guide theme Migration Role Settings Save Settings Settings item(s) remaining review(s) special offer(s) term(s) user(s) Project-Id-Version: ait-directory-migrations
POT-Creation-Date: 2016-10-04 13:36:23+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2016-12-05 07:13-0500
Language-Team: Bulgarian
Plural-Forms: nplurals=2; plural=(n != 1);
Language: bg_BG
 !!! Важно!!! Архивиране на вашата база данни преди започване на процеса на мигриране. Най-добрият начин е да се направи <strong>SQL стоварвам</strong> на пълна база данни. Поздравления! Всичко е мигрирал. За повече информация проверете %s Ако сте aren´t запознат с резервната база данни, моля свържете се с вашия хостинг доставчик. Мигриране Мигрират елементи, категории, места от директория и търсачка на бизнес теми на тема градски Пътеводител Миграция Запазете настройките Запазете настройките Настройки условия останалите отзив(и) специални оферти условия потребител(и) 