��          �      L      �     �  ;   �  F        V  ;   g  S   �     �  b   �  	   b     l     z     �     �  	   �  	   �     �     �     �  [  �     *  V   <  W   �     �  I   �  w   A  	   �  g   �     +     3     ?     S  
   Z     e     m     y  	   �     �                            
                            	                                            !!! Important !!! Backup your database before starting the migration process. Best way is to make an <strong>SQL dump</strong> of complete database. Congratulations! Everything was migrated. For more information, check the %s If you aren´t familiar with database backup, please contact your hosting provider. Migrate Migrate items, categories, locations from Directory and Business Finder themes to City Guide theme Migration Role Settings Save Settings Settings item(s) remaining review(s) special offer(s) term(s) user(s) Project-Id-Version: ait-directory-migrations
POT-Creation-Date: 2016-10-04 13:36:23+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2017-09-18 08:27-0400
Language-Team: Romanian
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : (n==0 || (n%100>0 && n%100<20)) ? 1 : 2);
Language: ro_RO
 !!! Important !!! Faceți o copie de rezervă la baza de date înainte de a începe procesul de migrare. Cel mai bună metodă este de a face <strong>SQL dump</strong> complet la baza de date. Felicitari! Toate datele au fost mutate. Pentru mai multe informații, verificați %s Dacă nu sunteți familiarizat cu executarea de copii de baze date, vă rugăm să contactați firma dvs. de găzduire. Migrează Migrează articole, categorii, locații din temele Directory și Business Finder către tema City Guide Migrare Setări Rol Salvează Setările Setari articol(e) rămase recenzie(i) ofertă(e) specială(e) termen(i) utilizator(i) 