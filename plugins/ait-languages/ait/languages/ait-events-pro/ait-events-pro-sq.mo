��    '      T  5   �      `     a     r     �     �  
   �  
   �     �  	   �     �     �     �  
   �     �                    !  	   3     =     M     f     l     u     �     �     �  $   �     �     �     �  
   �       "   !  "   D  )   g  &   �     �     �  "  �          (     <     C     Z     n     z  	   �  
   �     �     �     �      �     �  
   �     
          ,  !   9  /   [     �     �     �     �     �     �  1   �     	     3	     ;	     S	  !   b	     �	     �	     �	     �	     �	  	   �	                "                                                  &         !                       %          $                       
      #   '                          	           Add New Category Add New Event Address All Categories All Events Categories Creation Date Date From Date To Description Edit Category Edit Event Ending date of event EventAdd New Image Label New Category Name New Event No Events found No Events found in Trash Order Order By Parent Category Price Search Categories Search Events Select order of items listed on page Starting date of event Title Update Category View Event Zoom level of Google Map custom metabox titleEvent Options featured image metaboxEvent Image featured image metaboxRemove Event Image featured image metaboxSet Event Image general optionsGeneral taxonomy singular nameCategory Project-Id-Version: ait-events-pro
POT-Creation-Date: 2016-12-08 12:07:04+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2016-12-08 07:25-0500
Language-Team: Albanian
Plural-Forms: nplurals=2; plural=(n != 1);
Language: sq
 Shto kategori të re Shto Ngjarje të re Adresa Të gjitha kategoritë Të gjitha ngjarjet Kategoritë Data e krijimit Data prej Data deri  Përshkrimi Përpuno kategorinë Përpuno ngjarjen Data e përfundimit të Ngjarjes Shto të re Fotografia Etiketa Emri i kategorisë së re Ngjarje e re Nuk është gjetur asnjë ngjarje Nuk është gjetur asnjë ngjarje në Mbeturina Renditja Rendit sipas Kategoria përmbajtëse Çmimi Kërko kategoritë Kërko ngjarjen  Selekto rendin e elementeve të listuara në faqe Data e fillimit të Ngjarjes Titulli Përditëso kategorinë Shiko ngjarjen Niveli i zmadhimit të Google Map Opcionet e ngjarjeve Fotografia e ngjarjes Heq fotografinë e ngjarjes Vendos fotografinë e Ngjarjes Të përgjithshme Kategoria 