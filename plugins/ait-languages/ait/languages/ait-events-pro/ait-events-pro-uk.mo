��    ,      |  ;   �      �     �     �     �     �  
   �  
   
       	   #     -     5     A  
   O     Z     o     }     �     �     �     �     �  	   �     �     �  	   �     �          
          +     1     >     P  $   ^     �     �     �  
   �  "   �  "   �  )     &   +     R     j  m  �  (   �  "   !     D     Q     k          �     �     �     �  '   �     	  *   !	     L	     d	  M   y	     �	  
   �	  
   �	  %   �	     
  $   (
  4   M
     �
     �
     �
  )   �
  *   �
           &     G     e  X   }  $   �     �  !     !   0     R  !   j  2   �  6   �     �     	     '                         *   &          %   
          #                    ,                       )       (                        !      $   +                                           "   	           Add New Category Add New Event Address All Categories All Events Categories Creation Date Date From Date To Description Edit Category Edit Event Ending date of event EventAdd New Image Image displayed in header Item Label Map New Category Name New Event No Events found No Events found in Trash No header Order Order By Parent Category Parent Category: Price Related Item Search Categories Search Events Select order of items listed on page Starting date of event Title Update Category View Event custom metabox titleEvent Options featured image metaboxEvent Image featured image metaboxRemove Event Image featured image metaboxSet Event Image general optionsGeneral taxonomy singular nameCategory Project-Id-Version: ait-events-pro
POT-Creation-Date: 2016-12-08 12:07:04+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2016-12-08 07:26-0500
Language-Team: Ukrainian
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
Language: uk
 Додати нову категорію Додати новий захід Адреса Всі категорії Всі заходи Категорії Дата створення Дата з Дата до Описання Редагувати категорію Редагувати захід Дата закінчення заходу Додати новий Зображення Зображення, що відображається в заголовку Елемент Назва Карта Ім'я нової категорії Новий захід Заходів не знайдено Заходів у Кошику не знайдено Без заголовка Замовлення Замовити  по Батьківська категорія Батьківська категорія: Ціна Зв'язаний елемент Пошук категорій Шукати захід Виберіть порядок елементів у списку на сторінці Дата початку заходу Заголовок Оновити категорію Переглянути захід Опції заходу Зображення заходу Видалити зображення заходу Встановити зображення заходу Загальний Категорія 