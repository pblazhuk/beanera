��    D      <  a   \      �  	   �     �               (     0     4  A   B  -   �  	   �     �     �     �  %   �     �          &  
   6  
   A     L     T     p     �     �     �     �     �     �  	   �     �     �     �     �     �     �     �     �                    .     4     :     C     J     V     \     y     �  
   �  1   �     �  	   �     �  
   �     �     �     	  	   	  I   	  
   Z	  
   e	     p	     �	     �	      �	      �	  #  
  	   '     1     L     h     q     w     |  T   �  1   �               ,     2  *   7     b  0   j     �     �     �  	   �     �     �  
                       .     =     E     Q     X     ]     d     h     l     s     �     �     �     �     �     �     �     �  
   �     �     �     �       
     -        J     R  
   Z  
   e     p     x     �     �  `   �     �     �     �       	          #   =                   .   9       C   
                                         6   $       !   8           )   D   (   *               ?      "             &      	           <      /   1   3   :   -   @   =                       7   #                       +   2      ;           '                  5   %   0      B          A   ,                    >   4        %s (blog) %s - WooCommerce Shop Page &hellip; saving &hellip; 404 page Account All Archive pages Are you sure you want to delete custom page options of this page? Are you sure you want to remove this element? Ascending Attachment pages Both Bottom Category, Taxonomy, Tag, Author, Date Center Click to add custom description Custom Meta Box Descending Dev mode:  Disable Empty image (WPThumb) cache Empty theme cache Features Friday Gallery Homepage (blog) Import / Export Left Locations Monday Move Next Off On Order: Page Builder Please select backup file Remove Reset Resetting&hellip; Right Round Saturday Search Search page Share Single - WooCommerce Product Single post Source Start Date Successfully reset. This page will reload&hellip; Sunday Telephone Theme Admin Thumbnails Thursday Tuesday Web Wednesday You are about to create custom options for page:
{pageTitle}.
Is this ok? exportAll importAll name of elementFacilities name of elementFooter Items name of elementProducts settings were saved successfully there was an error during saving Project-Id-Version: skeleton admin
POT-Creation-Date: 2017-08-24 13:08:35+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2017-08-24 09:44-0400
Language-Team: Danish
Plural-Forms: nplurals=2; plural=(n != 1);
Language: da_DK
 %s (blog) %s - WooCommerce Shop side &hellip; arkiverer &hellip; 404 side Konto Alle Arkiv sider Er du sikker på, at du vil slette de brugerdefinerede indstillinger for denne side? Er du sikker på, at du vil fjerne dette element? Stigende Vedhæftnings sider Begge Knap Kategori, Taksonomi, Tags, Forfatter, Dato Centrer Klik for at tilføje brugerdefineret beskrivelse Brugerdefineret Meta Box Faldende Dev tilstand:  Deaktiver Tøm billed (WPThumb) cache Tøm temaets cache Funktioner Fredag Galleri Forside (blog) Import/eksport Venstre Placeringer Mandag Flyt Næste Fra Til Ordre: Page Builder Vælg sikkerhedskopi-fil Fjern Nulstil Resetting&hellip; Højre Runde Lørdag Søg Søge side Del Enkelt - WooCommerce produkt Enkelt post Kilde Start Dato Reset gennemført. Siden genindlæses&hellip; Søndag Telefon Tema Admin Miniaturer Torsdag Tirsdag Web Onsdag Du er på vej til at lave brugerdefinerede indstillinger for denne side:
{pageTitle}.
Er det ok? Alle Alle Faciliteter Footer elementer Produkter indstillingerne er nu arkiveret der opstod en fejl under arkivering 