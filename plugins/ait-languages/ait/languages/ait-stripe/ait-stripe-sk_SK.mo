��          �      �       0  '   1     Y     n     �     �     �     �     �  
   �     �  ;   �  ?   *  :  j  (   �     �     �  
   �     	     $     6     N     \     o  I   �  I   �              	                              
              Adds Stripe gateway to City Guide Theme After failed payment After successful payment Environment Live credentials Live environment Publishable key Redirections Secret key Test credentials Visitor is redirected to selected page after failed payment Visitor is redirected to selected page after successful payment Project-Id-Version: ait-stripe
POT-Creation-Date: 2016-10-04 13:36:01+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2017-08-28 06:36-0400
Language-Team: Slovak
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
Language: sk_SK
 Pridá Stripe bránu do City Guide témy Po neúspešnej platbe Po úspešnom zaplatení Prostredie Živé prístupové údaje Živé prostredie Zverejniteľný kľúč Presmerovanie Diskrétny kľúč Testovacie prístupové údaje Návštevník je po nevykonanej platbe presmerovaný na vybranú stránku Návštevník je po úspešnej platbe presmerovaný na vybranú stránku  