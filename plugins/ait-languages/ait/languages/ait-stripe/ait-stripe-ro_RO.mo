��          �      �       0  '   1     Y     n     �     �     �     �     �  
   �     �  ;   �  ?   *  M  j  7   �     �               #  
   ?     J     ]     l     {  M   �  Y   �              	                              
              Adds Stripe gateway to City Guide Theme After failed payment After successful payment Environment Live credentials Live environment Publishable key Redirections Secret key Test credentials Visitor is redirected to selected page after failed payment Visitor is redirected to selected page after successful payment Project-Id-Version: ait-stripe
POT-Creation-Date: 2016-10-04 13:36:01+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2017-09-18 08:27-0400
Language-Team: Romanian
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : (n==0 || (n%100>0 && n%100<20)) ? 1 : 2);
Language: ro_RO
 Adaugă modalitatea de plată Stripe la tema City Guide După plată eșuată După plată reușită Mediu Datele de conectare actuale Mediu live Cheie publicabilă Redirectionari Cheie Secretă Testează datele de conectare Vizitatorul este redirecționat către pagina selectată după plata eșuată Vizitatorul este redirecționat către pagina selectată după plata efectuată cu succes 