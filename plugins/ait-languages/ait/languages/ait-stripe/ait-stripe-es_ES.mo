��          �      �       0  '   1     Y     n     �     �     �     �     �  
   �     �  ;   �  ?   *     j  5   �     �     �     �     �          .     ?     M     [  G   r  G   �              	                              
              Adds Stripe gateway to City Guide Theme After failed payment After successful payment Environment Live credentials Live environment Publishable key Redirections Secret key Test credentials Visitor is redirected to selected page after failed payment Visitor is redirected to selected page after successful payment Project-Id-Version: ait-stripe
POT-Creation-Date: 2016-10-04 13:36:01+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2017-08-28 06:36-0400
Language-Team: Spanish
Plural-Forms: nplurals=2; plural=(n != 1);
Language: es_ES
 Añade una pasarela de pago Stripe al tema City Guide Tras el pago fallido Después del pago con éxito Entorno Credenciales de producción Entorno de producción Clave publicable Redirecciones Clave secreta Credenciales de prueba Se redirige al visitante a la página seleccionada tras el pago fallido El visitante es redirigido a la página seleccionada tras hacer el pago 