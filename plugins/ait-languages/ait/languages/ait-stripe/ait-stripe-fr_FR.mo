��          �      �       0  '   1     Y     n     �     �     �     �     �  
   �     �  ;   �  ?   *    j  <   �     �     �                      5     D     Q     _  Q   s  U   �              	                              
              Adds Stripe gateway to City Guide Theme After failed payment After successful payment Environment Live credentials Live environment Publishable key Redirections Secret key Test credentials Visitor is redirected to selected page after failed payment Visitor is redirected to selected page after successful payment Project-Id-Version: ait-stripe
POT-Creation-Date: 2016-10-04 13:36:01+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2017-08-28 06:34-0400
Language-Team: French
Plural-Forms: nplurals=2; plural=(n > 1);
Language: fr_FR
 Ajoute la passerelle de paiement Stripe au thème City Guide Après échec du paiement Après confirmation du paiement Environnement Identifiant actif Environnement actuel Clé publiable Redirections Clé secrète Identifiant de test Le visiteur est redirigé vers la page sélectionnée après l'échec du paiement Le visiteur est redirigé vers la page sélectionnée après confirmation du paiement 