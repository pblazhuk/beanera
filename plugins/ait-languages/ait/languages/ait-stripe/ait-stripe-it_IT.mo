��          �      �       0  '   1     Y     n     �     �     �     �     �  
   �     �  ;   �  ?   *     j  *   �     �      �     �     �               /     @     O  Y   d  W   �              	                              
              Adds Stripe gateway to City Guide Theme After failed payment After successful payment Environment Live credentials Live environment Publishable key Redirections Secret key Test credentials Visitor is redirected to selected page after failed payment Visitor is redirected to selected page after successful payment Project-Id-Version: ait-stripe
POT-Creation-Date: 2016-10-04 13:36:01+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2017-08-28 06:35-0400
Language-Team: Italian
Plural-Forms: nplurals=2; plural=(n != 1);
Language: it_IT
 Aggiunge gateway Stripe al Tema City Guide Dopo pagamento non riuscito Dopo completamento del pagamento Ambiente Credenziali Live Ambiente Live Chiave pubblicabile Reindirizzamenti Chiave privata Credenziali di prova Il visitatore viene reindirizzato sulla pagina selezionata dopo un pagamento non riuscito Il visitatore viene reindirizzato verso la pagina selezionata dopo l'avvenuto pagamento 