��          �      �       0  '   1     Y     n     �     �     �     �     �  
   �     �  ;   �  ?   *  %  j  ,   �     �     �     �     �     �     �               (  6   5  <   l              	                              
              Adds Stripe gateway to City Guide Theme After failed payment After successful payment Environment Live credentials Live environment Publishable key Redirections Secret key Test credentials Visitor is redirected to selected page after failed payment Visitor is redirected to selected page after successful payment Project-Id-Version: ait-stripe
POT-Creation-Date: 2016-10-04 13:36:01+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2017-08-28 06:34-0400
Language-Team: Chinese Traditional
Plural-Forms: nplurals=1; plural=0;
Language: zh_TW
 在 City Guide 主題中加入 Stripe 途徑 付款失敗後 付款成功後 環境 即時憑證 即時環境 可公開金鑰 重新定向 秘密金鑰 檢測憑證 付款失敗後將訪客重新導向到選定的頁面 在成功付款後訪客將重新定向到已選擇的頁面 