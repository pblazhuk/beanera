��          �      �       0  '   1     Y     n     �     �     �     �     �  
   �     �  ;   �  ?   *  j  j  H   �  *     (   I     r  2   �  #   �     �     	     (  ,   D  �   q  �   �              	                              
              Adds Stripe gateway to City Guide Theme After failed payment After successful payment Environment Live credentials Live environment Publishable key Redirections Secret key Test credentials Visitor is redirected to selected page after failed payment Visitor is redirected to selected page after successful payment Project-Id-Version: ait-stripe
POT-Creation-Date: 2016-10-04 13:36:01+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2017-08-28 06:35-0400
Language-Team: Russian
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
Language: ru_RU
 Добавление полосы Payment Gateway к City Guide теме После неудачной оплаты После успешной оплаты Окружающая среда Действующие учетные данные Продуктивная среда Публикуемый ключ Перенаправления Секретный ключ Тестовые учетные данные Посетитель перенаправляется на выбранную страницу после неудачного платежа Посетитель, после успешной оплаты, будет перенаправлен на выбранную страницу 