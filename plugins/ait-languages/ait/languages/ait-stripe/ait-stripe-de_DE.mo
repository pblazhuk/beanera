��          �      �       0  '   1     Y     n     �     �     �     �     �  
   �     �  ;   �  ?   *    j  /   �     �     �     �     �               ;     M     a  Q   s  M   �              	                              
              Adds Stripe gateway to City Guide Theme After failed payment After successful payment Environment Live credentials Live environment Publishable key Redirections Secret key Test credentials Visitor is redirected to selected page after failed payment Visitor is redirected to selected page after successful payment Project-Id-Version: ait-stripe
POT-Creation-Date: 2016-10-04 13:36:01+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2017-08-28 06:35-0400
Language-Team: German
Plural-Forms: nplurals=2; plural=(n != 1);
Language: de_DE
 Stripe-Gateway zur City Guide Theme hinzufügen Nach fehlgeschlagener Zahlung Nach erfolgreicher Zahlung Umgebung Live-Anmeldedaten Live-Umgebung Veröffentlichbarer Schlüssel Seitenumleitungen Geheimer Schlüssel Test-Anmeldedaten Besucher wird nach fehlgeschlagener Zahlung zu ausgewählter Seite weitergeleitet Besucher wird nach erfolgreicher Bezahlung auf gewählte Seite weitergeleitet 