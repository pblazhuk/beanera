��          �      �       0  '   1     Y     n     �     �     �     �     �  
   �     �  ;   �  ?   *    j  ,   �     �     �     �     �               "     1     A  H   R  J   �              	                              
              Adds Stripe gateway to City Guide Theme After failed payment After successful payment Environment Live credentials Live environment Publishable key Redirections Secret key Test credentials Visitor is redirected to selected page after failed payment Visitor is redirected to selected page after successful payment Project-Id-Version: ait-stripe
POT-Creation-Date: 2016-10-04 13:36:01+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2017-04-12 08:47-0400
Language-Team: Danish
Plural-Forms: nplurals=2; plural=(n != 1);
Language: da_DK
 Tilføjer Stripe Gateway til City Guide tema Efter mislykket betaling Efter gennemført betaling Miljø Live oplysninger Live Miljø Publicerbar nøgle Viderestilling Hemmelig nøgle Test oplysninger Besøgende bliver viderestillet til valgte side efter annuleret betaling Besøgende bliver viderestillet til valgte side efter gennemført betaling 