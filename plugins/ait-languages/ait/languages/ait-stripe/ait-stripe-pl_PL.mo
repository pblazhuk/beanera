��          �      �       0  '   1     Y     n     �     �     �     �     �  
   �     �  ;   �  ?   *  Y  j  *   �     �          $     0     M     e     q     �     �  T   �  M   �              	                              
              Adds Stripe gateway to City Guide Theme After failed payment After successful payment Environment Live credentials Live environment Publishable key Redirections Secret key Test credentials Visitor is redirected to selected page after failed payment Visitor is redirected to selected page after successful payment Project-Id-Version: ait-stripe
POT-Creation-Date: 2016-10-04 13:36:01+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2017-08-28 06:35-0400
Language-Team: Polish
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
Language: pl_PL
 Dodaje bramkę Stripe do motywu City Guide Po niepomyślnej transakcji Po pomyślnej transakcji Środowisko Rzeczywiste uwierzytelnienie Środowisko rzeczywiste Jawny klucz Przekierowania Tajny klucz Testowe uwierzytelnienie Odwiedzający zostanie przekierowany na wybraną stronę po niepomyślnej transakcji Odwiedzający jest przekierowany na wybraną stronę po dokonaniu płatności 