��          �      �       0  '   1     Y     n     �     �     �     �     �  
   �     �  ;   �  ?   *  .  j  -   �     �     �               !     2     D     V     d  T   y  T   �              	                              
              Adds Stripe gateway to City Guide Theme After failed payment After successful payment Environment Live credentials Live environment Publishable key Redirections Secret key Test credentials Visitor is redirected to selected page after failed payment Visitor is redirected to selected page after successful payment Project-Id-Version: ait-stripe
POT-Creation-Date: 2016-10-04 13:36:01+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2017-08-28 06:35-0400
Language-Team: Portuguese, Brazilian
Plural-Forms: nplurals=2; plural=(n != 1);
Language: pt_BR
 Adiciona portal "Stripe" no tema "City Guide" Após pagamento não realidado Após o pagamento com sucesso Ambiente Credenciais ao vivo Ambiente ao vivo Chave publicável Redirecionamentos Chave secreta Credenciais de teste Visitante é redirecionado para página selecionada após o pagamento não realizado O visitante é redirecionado à página selecionada depois do pagamento bem sucedido 