��          �      �       0  '   1     Y     n     �     �     �     �     �  
   �     �  ;   �  ?   *    j  ?   �  5   �  -   �     *  +   ?  #   k  %   �     �     �  )   �  �     �   �              	                              
              Adds Stripe gateway to City Guide Theme After failed payment After successful payment Environment Live credentials Live environment Publishable key Redirections Secret key Test credentials Visitor is redirected to selected page after failed payment Visitor is redirected to selected page after successful payment Project-Id-Version: ait-stripe
POT-Creation-Date: 2016-10-04 13:36:01+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2017-08-22 08:48-0400
Language-Team: Greek
Plural-Forms: nplurals=2; plural=(n != 1);
Language: el
 Προσθέτεi λωρίδα πύλη στο City Guide Theme Μετά την αποτυχημένη πληρωμή Μετά την επιτυχή πληρωμή Περιβάλλον Ζωντανά διαπιστευτήρια Ζωντανό περιβάλλον Δημοσιεύσιμο κλειδί Ανακατευθύνσεις Μυστικό κλειδί Δοκιμή διαπιστευτήρια Επισκέπτης ανακατευθύνεται επιλεγμένη σελίδα μετά την αποτυχημένη πληρωμή Επισκέπτης ανακατευθύνεται σε επιλεγμένη σελίδα μετά την επιτυχή πληρωμή 