��          �      �       0  '   1     Y     n     �     �     �     �     �  
   �     �  ;   �  ?   *    j  ,   �  )   �  %   �               "     3     N     b     q  D   �  U   �              	                              
              Adds Stripe gateway to City Guide Theme After failed payment After successful payment Environment Live credentials Live environment Publishable key Redirections Secret key Test credentials Visitor is redirected to selected page after failed payment Visitor is redirected to selected page after successful payment Project-Id-Version: ait-stripe
POT-Creation-Date: 2016-10-04 13:36:01+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2017-08-28 06:34-0400
Language-Team: Finnish
Plural-Forms: nplurals=2; plural=(n != 1);
Language: fi
 Lisää Stripe-portin City Guiden kuvaukseen Epäonnistuneen maksutoimituksen jälkeen Onnistuneen maksutoimituksen jälkeen Ympäristö Live-suositukset Live-ympäristö Julkaistavissa oleva avain Uudelleen ohjaukset Salainen avain Testisuositukset Vierailija ohjataan valitulle sivulle maksutoiminnon epäonnistuessa Vierailija ohjataan uudelleen valitulle sivulle onnistuneen maksusuorituksen jälkeen 