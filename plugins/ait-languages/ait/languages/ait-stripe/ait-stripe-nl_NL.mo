��          �      �       0  '   1     Y     n     �     �     �     �     �  
   �     �  ;   �  ?   *    j  +   �     �     �     �     �                $     0     @  M   Q  L   �              	                              
              Adds Stripe gateway to City Guide Theme After failed payment After successful payment Environment Live credentials Live environment Publishable key Redirections Secret key Test credentials Visitor is redirected to selected page after failed payment Visitor is redirected to selected page after successful payment Project-Id-Version: ait-stripe
POT-Creation-Date: 2016-10-04 13:36:01+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2017-08-28 06:34-0400
Language-Team: Dutch
Plural-Forms: nplurals=2; plural=(n != 1);
Language: nl_NL
 Voegt Stripe poort toe aan City Guide Thema Na mislukte betaling Na een succesvolle betaling Omgeving Live referenties Live omgeving Publiceerbare sleutel Omleidingen Geheime sleutel Test referenties Bezoeker wordt doorgestuurd naar de geselecteerde pagina na mislukte betaling Bezoeker wordt verwezen naar de geselecteerde pagina na succesvolle betaling 