��          �      �       0  '   1     Y     n     �     �     �     �     �  
   �     �  ;   �  ?   *  $  j  ,   �     �     �     �     �     �       	        #     *  6   7  3   n              	                              
              Adds Stripe gateway to City Guide Theme After failed payment After successful payment Environment Live credentials Live environment Publishable key Redirections Secret key Test credentials Visitor is redirected to selected page after failed payment Visitor is redirected to selected page after successful payment Project-Id-Version: ait-stripe
POT-Creation-Date: 2016-10-04 13:36:01+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2017-08-28 06:34-0400
Language-Team: Chinese Simplified
Plural-Forms: nplurals=1; plural=0;
Language: zh_CN
 在城市指南主题中添加 Stripe 网关 在付款失败之后 付款成功之后 环境 有效凭据 有效环境 可发布的密钥 重定向 密钥 测试凭据 付款失败后，将访问者重定向至选定页面 支付成功后访客将被重定向至相应页面 