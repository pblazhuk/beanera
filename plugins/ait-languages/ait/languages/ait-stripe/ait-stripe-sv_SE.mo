��          �      �       0  '   1     Y     n     �     �     �     �     �  
   �     �  ;   �  ?   *     j  ,   �     �     �     �     �  
             ,     ;     I  ?   f  A   �              	                              
              Adds Stripe gateway to City Guide Theme After failed payment After successful payment Environment Live credentials Live environment Publishable key Redirections Secret key Test credentials Visitor is redirected to selected page after failed payment Visitor is redirected to selected page after successful payment Project-Id-Version: ait-stripe
POT-Creation-Date: 2016-10-04 13:36:01+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2017-08-28 06:36-0400
Language-Team: Swedish
Plural-Forms: nplurals=2; plural=(n != 1);
Language: sv_SE
 Adderar Stripe gateway till City Guide Theme Efter misslyckad betalning Efter lyckad betalning Miljö Autentiseringsuppgifter live Livemiljö Publicerbar nyckel Omdirigeringar Hemlig nyckel Autentiseringsuppgifter test Besökare omdirigeras till vald sida efter misslyckad betalning Besökaren blir skickad till vald sida efter genomförd betalning 