��          �      �       0  '   1     Y     n     �     �     �     �     �  
   �     �  ;   �  ?   *  9  j  ,   �     �     �               )     <     S     d     w  I   �  L   �              	                              
              Adds Stripe gateway to City Guide Theme After failed payment After successful payment Environment Live credentials Live environment Publishable key Redirections Secret key Test credentials Visitor is redirected to selected page after failed payment Visitor is redirected to selected page after successful payment Project-Id-Version: ait-stripe
POT-Creation-Date: 2016-10-04 13:36:01+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2017-09-18 08:27-0400
Language-Team: Czech
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
Language: cs_CZ
 Přidá Stripe bránu do City Guide šablony Po neúspěšné platbě Po úspěšné platbě Prostředí Živé přístupové údaje Živé prostředí Zveřejnitelný klíč Přesměrování Diskrétní klíč Testovací přístupové údaje Návštěvník po zrušení platby je přesměrován na vybranou stránku Návštěvník po úspěšné platbě je přesměrován na vybranou stránku 