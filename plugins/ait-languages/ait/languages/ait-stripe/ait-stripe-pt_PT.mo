��          �      �       0  '   1     Y     n     �     �     �     �     �  
   �     �  ;   �  ?   *  #  j  2   �     �     �     �                     4     F     V  Q   k  Q   �              	                              
              Adds Stripe gateway to City Guide Theme After failed payment After successful payment Environment Live credentials Live environment Publishable key Redirections Secret key Test credentials Visitor is redirected to selected page after failed payment Visitor is redirected to selected page after successful payment Project-Id-Version: ait-stripe
POT-Creation-Date: 2016-10-04 13:36:01+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2017-08-28 06:35-0400
Language-Team: Portuguese
Plural-Forms: nplurals=2; plural=(n != 1);
Language: pt_PT
 Adicionar Stripe gateway ao Tema do Guia da Cidade Após pagamento rejeitado Após pagamento bem-sucedido Ambiente Credenciais Live Ambiente Live Código publicável Redirecionamentos Código secreto Credenciais de teste O visitante é redirecionado para a página selecionada após pagamento rejeitado O visitante é direcionado para a página selecionada após pagamento com sucesso 