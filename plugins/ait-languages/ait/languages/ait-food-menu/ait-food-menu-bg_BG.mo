��    %      D  5   l      @     A  	   N     X     _     v  	   �  	   �     �  	   �  
   �     �  	   �  
   �  #   �     	          +     ;     G     P     ^  V   d  
   �     �  ,   �  (         )     /     2  	   A     K     e  ,   z     �     �     �  %  �  '        <     X  ,   m  @   �  *   �       4        T     f     x     }     �  :   �     �  ;   �  )   &	     P	     f	     |	     �	  �   �	     c
  "   v
  J   �
  D   �
     )     :  *   ?     j     |     �  `   �     �          +                !   #      $                 %                                       
                    "                                          	                                    Add New Menu All Menus Amount Average price category Default Special Menu Type Edit Menu Food Menu Food Menu Options Food Type Food Types From Menu Type Menu Types Menu is available only these dates: New Menu No menus found in Trash. No menus found. Offer dates Our Menu Parent Menus: Price Refresh the page to see newly added menu types. This might be for example a Daily Menu Restaurant Search Menus Some description of the plugin functionality There are no daily menus for current day Title To View Full Menu View Menu add new on admin barMenu admin menuFood Menu leave empty date "To" for never ending offer menuAdd New post type general nameMenus post type singular nameMenu Project-Id-Version: ait-food-menu
POT-Creation-Date: 2016-10-18 12:26:40+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2016-12-05 07:13-0500
Language-Team: Bulgarian
Plural-Forms: nplurals=2; plural=(n != 1);
Language: bg_BG
 Добавяне на ново меню Всички събития Количество Средна ценова категория Специално меню тип по подразбиране Редактиране на събитие Меню за харна Създаване на меню на храните Вид храна Вид храна От Вид меню Вид меню Менюто е налично само тези дати: Ново меню Няма намерени събития в кошчето. Няма намерени събития. Оферта дати Нашето меню Родителско меню: Цена Обновяване на страницата за да видите видовете новодобавен меню. Това може да бъде например дневно меню Ресторант Търсене на събития Някои описание на плъгин функционалност Има няма дневни менюта за текущия ден Заглавие До Меню за изглед на пълен Ново меню Меню Меню за харна оставете празно датата "До за безкрайните предлагат" Добавяне на ново Менюта Меню 