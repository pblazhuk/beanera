��    %      D  5   l      @     A  	   N     X     _     v  	   �  	   �     �  	   �  
   �     �  	   �  
   �  #   �     	          +     ;     G     P     ^  V   d  
   �     �  ,   �  (         )     /     2  	   A     K     e  ,   z     �     �     �  u  �     d  
   t          �  !   �  
   �  
   �     �  	   �     �        
     
     "     	   <     F     c     x  	   �     �     �  Y   �      	     		  $   	  #   ?	     c	     j	     m	     �	     �	  
   �	  ,   �	  
   �	     �	     �	                !   #      $                 %                                       
                    "                                          	                                    Add New Menu All Menus Amount Average price category Default Special Menu Type Edit Menu Food Menu Food Menu Options Food Type Food Types From Menu Type Menu Types Menu is available only these dates: New Menu No menus found in Trash. No menus found. Offer dates Our Menu Parent Menus: Price Refresh the page to see newly added menu types. This might be for example a Daily Menu Restaurant Search Menus Some description of the plugin functionality There are no daily menus for current day Title To View Full Menu View Menu add new on admin barMenu admin menuFood Menu leave empty date "To" for never ending offer menuAdd New post type general nameMenus post type singular nameMenu Project-Id-Version: ait-food-menu
POT-Creation-Date: 2016-10-18 12:26:40+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2017-09-18 08:27-0400
Language-Team: Serbian (Latin)
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
Language: sr_RS
 Dodaj novi Meni Svi Meniji Iznos Prosečna cena kategorije Uobičajeni Specijalni Tip Menija Uredi Meni Meni Hrane Meni Hrane Opcije Tip Hrane Tipovi Hrane Od Tip Menija Tip menija Meni je dostupan samo ovih datuma: Novi Meni Meni nije pronađen u Trash. Meni nije pronađen. Ponuda Datuma Naš Meni Nadređeni Meni: Cena Obnovi stranicu da vidiš dodate nove tipove menija. Ovo može biti na primer Dnevni Meni Restoran Pretraži Menije Neki opisi za funkcionalnost dodatka Nema dnevnih menija za aktuelan dan Naslov Za Pogledaj Potpun Meni Pogledaj Meni Meni Meni Hrane ostavi prazan datum "To" za beskrajnu ponudu Dodaj novo Meniji Meni 