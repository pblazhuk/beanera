��    %      D  5   l      @     A  	   N     X     _     v  	   �  	   �     �  	   �  
   �     �  	   �  
   �  #   �     	          +     ;     G     P     ^  V   d  
   �     �  ,   �  (         )     /     2  	   A     K     e  ,   z     �     �     �  m  �  $   \     �     �  0   �  >   �  #        :     L     l     �     �     �     �  <   �     	  /   	  /   J	     z	     �	  "   �	     �	  �   �	     �
     �
  S   �
  L        _     r  *   w     �     �     �  �   �     c          �                !   #      $                 %                                       
                    "                                          	                                    Add New Menu All Menus Amount Average price category Default Special Menu Type Edit Menu Food Menu Food Menu Options Food Type Food Types From Menu Type Menu Types Menu is available only these dates: New Menu No menus found in Trash. No menus found. Offer dates Our Menu Parent Menus: Price Refresh the page to see newly added menu types. This might be for example a Daily Menu Restaurant Search Menus Some description of the plugin functionality There are no daily menus for current day Title To View Full Menu View Menu add new on admin barMenu admin menuFood Menu leave empty date "To" for never ending offer menuAdd New post type general nameMenus post type singular nameMenu Project-Id-Version: ait-food-menu
POT-Creation-Date: 2016-10-18 12:26:40+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2017-08-28 06:35-0400
Language-Team: Russian
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
Language: ru_RU
 Добавить новое меню Все меню Количество Средняя ценовая категория Специальный тип меню по умолчанию Редактировать меню Меню блюд Возможности меню Тип продуктов Типы продуктов От Тип меню Типы меню Меню доступно только на эти даты: Новое меню Меню не найдено в корзине. Не найдено ни одного меню. Предложение даты Наше меню Родительские меню: Цена Обновите страницу, чтобы увидеть недавно добавленные типы меню. Например, это может быть ежедневное меню Ресторан Поиск меню Некоторое описание функциональности плагина Там нет ежедневного меню для текущего дня Заголовок До Меню полного просмотра Показать меню Меню Меню блюд для того чтобы никогда не заканчивать предложение, оставьте пустую дату "до" Добавить новое Меню Меню 