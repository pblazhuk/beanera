��          \      �       �   2   �      �             5   ,     b     s  &  �  C   �     �             F   #     j     y                                       Adds ability to search content around any location Advanced Search Default Location Default Radius Default radius is used unless user choose differently General Settings Use Default Values Project-Id-Version: ait-advanced-search
POT-Creation-Date: 2016-11-23 13:39:13+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2017-08-28 06:34-0400
Language-Team: Finnish
Plural-Forms: nplurals=2; plural=(n != 1);
Language: fi
 Lisää mahdollisuuden hakea sisältöä mistä tahansa sijainnista Edistynyt haku Oletussijainti Oletustoimintasäde Oletustoimintasädettä käytetään, ellei käyttäjä valitse toisin Yleisasetukset Käytä oletusarvoja 