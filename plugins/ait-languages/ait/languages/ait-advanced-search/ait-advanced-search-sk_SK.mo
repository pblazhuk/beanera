��          \      �       �   2   �      �             5   ,     b     s  C  �  D   �          +     @  K   T     �     �                                       Adds ability to search content around any location Advanced Search Default Location Default Radius Default radius is used unless user choose differently General Settings Use Default Values Project-Id-Version: ait-advanced-search
POT-Creation-Date: 2016-11-23 13:39:13+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2017-08-28 06:36-0400
Language-Team: Slovak
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
Language: sk_SK
 Pridáva možnosť vyhľadávať obsah v blízkosti každej lokality Rozšírené vyhľadávanie Predvolená lokalita Predvolený rádius Predvolený rádius sa používa dovtedy, kým si užívateľ nezvolí inak Všeobecné nastavenia Použiť predvolené hodnoty 