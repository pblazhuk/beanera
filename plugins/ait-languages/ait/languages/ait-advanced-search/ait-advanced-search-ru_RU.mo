��          \      �       �   2   �      �             5   ,     b     s  s  �  �   �  !     4   �  $   �  �   �     �  ?   �                                       Adds ability to search content around any location Advanced Search Default Location Default Radius Default radius is used unless user choose differently General Settings Use Default Values Project-Id-Version: ait-advanced-search
POT-Creation-Date: 2016-11-23 13:39:13+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2017-08-28 06:35-0400
Language-Team: Russian
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
Language: ru_RU
 Добавляет возможность поиска содержимого вокруг любого местоположения Расширенный поиск Местоположение по умолчанию Радиус по умолчанию Радиус по умолчанию используется, если пользователь не выбрал другой вариант Общие настройки Используйте значения по умолчанию 