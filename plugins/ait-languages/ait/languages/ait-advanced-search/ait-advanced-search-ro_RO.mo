��          \      �       �   2   �      �             5   ,     b     s  V  �  H   �     &     9     M  R   ^     �     �                                       Adds ability to search content around any location Advanced Search Default Location Default Radius Default radius is used unless user choose differently General Settings Use Default Values Project-Id-Version: ait-advanced-search
POT-Creation-Date: 2016-11-23 13:39:13+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2017-09-18 08:27-0400
Language-Team: Romanian
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : (n==0 || (n%100>0 && n%100<20)) ? 1 : 2);
Language: ro_RO
 Adaugă posibilitatea de a căuta conținut în jurul oricărei locații Căutare avansată Locație implicită Rază implicită Raza implicită este folosită afară de cazul în care utilizatorul alege diferit Setari Generale Folosește valorile implicite 