��          \      �       �   2   �      �             5   ,     b     s  $  �  �   �  !   E  #   g  '   �  �   �  !   K  2   m                                       Adds ability to search content around any location Advanced Search Default Location Default Radius Default radius is used unless user choose differently General Settings Use Default Values Project-Id-Version: ait-advanced-search
POT-Creation-Date: 2016-11-23 13:39:13+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2017-08-22 08:48-0400
Language-Team: Greek
Plural-Forms: nplurals=2; plural=(n != 1);
Language: el
 Προσθέτει τη δυνατότητα να αναζητήσετε περιεχόμενο γύρω από οποιαδήποτε τοποθεσία Σύνθετη Αναζήτηση Προεπιλεγμένη θέση Προεπιλεγμένη ακτίνα Προεπιλεγμένη ακτίνα χρησιμοποιείται εκτός και αν ο χρήστης επιλέξει διαφορετικά Γενικές Ρυθμίσεις Χρήση προεπιλεγμένων τιμών 