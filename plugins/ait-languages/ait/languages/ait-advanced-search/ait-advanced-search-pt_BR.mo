��          \      �       �   2   �      �             5   ,     b     s  7  �  ;   �     �     
     $  N   5     �     �                                       Adds ability to search content around any location Advanced Search Default Location Default Radius Default radius is used unless user choose differently General Settings Use Default Values Project-Id-Version: ait-advanced-search
POT-Creation-Date: 2016-11-23 13:39:13+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2017-08-28 06:35-0400
Language-Team: Portuguese, Brazilian
Plural-Forms: nplurals=2; plural=(n != 1);
Language: pt_BR
 Adiciona a capacidade de buscar conteúdo em qualquer local Busca Avançada Localização Predefinida Raio Predefinido Caso o usuário não escolha um raio diferente, o raio predefinido será usado Configurações gerais Usar Valores Predefinidos 