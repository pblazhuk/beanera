��    &      L  5   |      P     Q     `  /   f  !   �  '   �     �     �  &   �          *     0  	   A     K     Q     `  	   f  	   p     z     �  #   �     �     �     �     �     �                    -     ;     D  	   I     S  a   X     �     �  N   �  a       t     �  G   �  7   �  H        `     e  G   y     �     �     �     �     �     	     	     "	     7	     K	  "   g	  9   �	     �	     �	  "   �	  
   
     
     
     "
     ;
     G
  
   U
     `
     e
     m
  ]   s
     �
     �
  @   �
        "                       #              
      !                                       	                       &       %                                                $          + Add New Item Admin Applicable for "Number" and "Range" input types Applicable for "Range" input type Custom fields for Item Custom Post Type Date Default Value Display or Hide section on item detail Display section Email General Settings Help Text Input Item Extension Label Max Value Min Value No Items Defined No extension fields available No package defined for current user Number On-Off Open/Collapse All Items Package Range Remove Remove All Items Section Description Section Title Settings Step Telephone Text There are no extension fields defined for the current package... <a href='%s'>Change Settings</a> Type Url ait-item-extensionCurrent theme is not compatible with Item Extension plugin! Project-Id-Version: ait-item-extension
POT-Creation-Date: 2017-08-01 12:09:30+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2017-08-28 06:35-0400
Language-Team: Polish
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
Language: pl_PL
 + Dodaj nowy element Administrator Posiada zastosowanie dla „Liczby” i „Zakresu” rodzajów wejść Posiada zastosowanie dla „Zakresu” rodzaju wejścia Pola niestandardowe dla niestandardowego typu posta dotyczącego obiektu Data Wartość domyślna Pokaż lub ukryj sekcję zawierającą szczegółowe dane o przedmiocie Pokaż sekcję E-mail Ogólne ustawienia Tekst pomocy Wprowadzenie Rozszerzenie obiektu Etykieta Wartość maksymalna Wartość minimalna Brak zdefiniowanych pozycji Brak dostępnych pól rozszerzenia Brak zdefiniowanej przesyłki dla aktualnego użytkownika Numer włączony-wyłączony Otwórz/rozwiń wszystkie elementy Przesyłka Odległość Usuń Usuń wszystkie elementy Opis sekcji Tytuł sekcji Ustawienia Krok Nr tel. Tekst Nie zdefiniowano pól rozszerzeń do aktualnego pakietu... <a href='%s'>Zmień ustawienia</a> Typ URL Aktualny motyw nie jest kompatybilny z dodatkiem Item Extension! 