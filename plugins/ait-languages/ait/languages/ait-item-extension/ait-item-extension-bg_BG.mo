��    %      D  5   l      @     A     P  /   V  !   �  '   �     �     �  &   �     
             	   1     ;     A     P  	   V  	   `     j     {  #   �     �     �     �     �     �     �     �     	          +     4  	   9     C     H     M  N   Q  *  �  -   �     �  T     T   i  |   �     ;  .   D  d   s  ,   �  
   	     	     ,	     F	  ,   Y	     �	  %   �	  #   �	  .   �	  >   
  P   K
  
   �
     �
  B   �
  
   �
     �
       7   "  $   Z  &        �     �     �  
   �     �     �  ]   �        "                                      
      !                                       	                       %       $                                                #          + Add New Item Admin Applicable for "Number" and "Range" input types Applicable for "Range" input type Custom fields for Item Custom Post Type Date Default Value Display or Hide section on item detail Display section Email General Settings Help Text Input Item Extension Label Max Value Min Value No Items Defined No extension fields available No package defined for current user Number On-Off Open/Collapse All Items Package Range Remove Remove All Items Section Description Section Title Settings Step Telephone Text Type Url ait-item-extensionCurrent theme is not compatible with Item Extension plugin! Project-Id-Version: ait-item-extension
POT-Creation-Date: 2017-08-01 12:09:30+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2017-08-09 07:45-0400
Language-Team: Bulgarian
Plural-Forms: nplurals=2; plural=(n != 1);
Language: bg_BG
 + Добавяне на нов елемент Администрация Приложимо за "Номер" и "Диапазон" входни видове Приложимо за "Номер" и "Диапазон" входни видове Потребителски полета за елементи с тип на потребителски публикации Дата Стойност по подразбиране Показване или скриване на раздел за елемент от детайла Показване на описанията Имейл Общи настройки Помощен текст Въвеждане Елемент на разширението Етикет Максимална стойност Минимална стойност Няма дефинирани елементи Няма налични полета за разширение Няма пакет, дефиниран за текущия потребител Номер On-Off Отваряне/свиване на всички елементи Пакет Диапазон Премахване Премахване на всички елементи Описание на раздела Заглавие на секцията Настройки Стъпка Телефон Текст Тип url Текущата тема не е съвместима с иск листинг плъгин! 