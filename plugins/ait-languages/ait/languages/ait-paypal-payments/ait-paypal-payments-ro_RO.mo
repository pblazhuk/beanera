��          �      ,      �     �     �     �  '   �     �  #   �          $     3  2   ;     n     {     �     �  >   �  ?   �  V  +  
   �     �     �  2   �     �  #   �           ,  	   B  F   L  
   �     �     �     �  Y   �  Y   +           	                                                         
          API Password API Signature API Username Adds PayPal gateway to City Guide Theme After approving of payment After cancelling of payment process Credentials Enable logging Logging Logs are stored in wp-content/paypal-info.log file NOTIFICATION PAYMENT COMPLETED PAYMENT CONFIRMED Redirections Visitor is redirected to selected page after cancelled payment Visitor is redirected to selected page after successful payment Project-Id-Version: ait-paypal-payments
POT-Creation-Date: 2016-10-04 13:36:15+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2017-09-18 08:27-0400
Language-Team: Romanian
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : (n==0 || (n%100>0 && n%100<20)) ? 1 : 2);
Language: ro_RO
 Parola API Semnatura API Nume de Utilizator API Adaugă metoda de plată PayPal la tema City Guide După aprobarea platii După anularea procesului de plată Acreditări Activare Inregistrare Conectare Jurnalele sunt stocate în fişierul wp-conţinut/debug-paypal-log.log NOTIFICARE PLATĂ EFECTUATA PLATĂ CONFIRMATĂ Redirectionari Vizitatorul este redirecționat către pagina selectată după plata efectuată cu succes Vizitatorul este redirecționat către pagina selectată după plata efectuată cu succes 