��          �      ,      �     �     �     �  '   �     �  #   �          $     3  2   ;     n     {     �     �  >   �  ?   �  {  +     �  
   �     �  )   �     �  #     	   <     F     b  8   q     �     �     �     �  B   �  B   *           	                                                         
          API Password API Signature API Username Adds PayPal gateway to City Guide Theme After approving of payment After cancelling of payment process Credentials Enable logging Logging Logs are stored in wp-content/paypal-info.log file NOTIFICATION PAYMENT COMPLETED PAYMENT CONFIRMED Redirections Visitor is redirected to selected page after cancelled payment Visitor is redirected to selected page after successful payment Project-Id-Version: ait-paypal-payments
POT-Creation-Date: 2016-10-04 13:36:15+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2017-09-18 08:27-0400
Language-Team: Serbian (Latin)
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
Language: sr_RS
 API lozinka API potpis API Korisničko Ime Dodaj PayPal plaćanje na City Guide Temu Nakon odobravanja plaćanja Nakon otkazivanja procesa plaćanja Akreditiv Onemogućiti prijavljivanje Prijavljivanje Evidencije se čuvaju na wp-content/paypal-info.log file OBAVEŠTENJE UPLATA PRIMLJENA UPLATA POTVRĐENA Preusmerenja Posetilac je preusmeren na odabranu stranicu nakon otkazane uplate Posetilac je preusmeren na odabranu stranicu nakon uspešne uplate 