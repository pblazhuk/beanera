��          �      ,      �     �     �     �  '   �     �  #   �          $     3  2   ;     n     {     �     �  >   �  ?   �  (  +     T     d     q  ,   �     �  '   �     �     �  
     2        Q     ^     s     �  H   �  J   �           	                                                         
          API Password API Signature API Username Adds PayPal gateway to City Guide Theme After approving of payment After cancelling of payment process Credentials Enable logging Logging Logs are stored in wp-content/paypal-info.log file NOTIFICATION PAYMENT COMPLETED PAYMENT CONFIRMED Redirections Visitor is redirected to selected page after cancelled payment Visitor is redirected to selected page after successful payment Project-Id-Version: ait-paypal-payments
POT-Creation-Date: 2016-10-04 13:36:15+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2017-04-12 08:47-0400
Language-Team: Danish
Plural-Forms: nplurals=2; plural=(n != 1);
Language: da_DK
 API Adgangskode API Signatur API Brugernavn Tilføjer PayPal gateway til City Guide tema Efter godkendelse af betaling Efter annullering af betalingsprocessen Oplysninger Aktivér logføring Logføring Logfiler gemmes i wp-content/paypal-info.log filen NOTIFIKATION BETALING GENNEMFØRT BETALING BEKRÆFTET Viderestilling Besøgende bliver viderestillet til valgte side efter annuleret betaling Besøgende bliver viderestillet til valgte side efter gennemført betaling 