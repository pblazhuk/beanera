��    G      T  a   �                "  D   1     v     z     �     �     �  	   �     �     �     �     �  
   �     �     �     �               &     C     V     e     {     �  
   �  
   �  E   �  	   �     �          %     +     4     D     U     e     v     �  
   �  
   �  
   �  
   �  
   �     �     �     �  W   �     F	  
   V	     a	     {	     �	  $   �	  &   �	     �	     �	     �	     
     
  	   *
  &   4
     [
     w
     �
     �
     �
     �
  '   �
     !  x  A     �     �  E   �     /     3     B     P  
   Y     d     l  
   s     ~     �     �     �     �     �     �     �     �          "     5     S     Y  
   o     z  F   �  
   �     �     �               (     >     U     k      �     �  	   �  	   �  	   �  	   �  	   �     �     �  	   �  S        Y  
   l     w     �     �  %   �  /   �               $     :     Q  	   d     n  	   �     �     �  	   �  	   �  
   �     �  
   �         E   	   D                 C      F   )   &   +   ,   -      .                        ?      5   !   @   8   =       $   0      6       <   /                                  >                 %      1   2          3       :       (   
       G                   B   ;   #      7   A   "   '   *       9                                 4                 Add New Category Add New Review Adds rating and reviews functionality for City Guide Wordpress Theme All All Categories All Reviews Approve Approved Ascending Average Category Creation Date Date Descending Description Edit Category Edit Review Email notifications Item Reviews Item author approve Reviews  Item doesn't exist Leave a Review Maximum shown reviews Name New Category Name New Rating New Review New rating for <a href="%s">%s</a> has been approved by administrator No rating No reviews found in Trash. No reviews found. Order Order By Parent Category Parent Category: Parent Reviews: Pending Approval Please fill out all fields Publishing ... Question 1 Question 2 Question 3 Question 4 Question 5 Rate now Rating Review Review <a href="%s">#%d</a> for item <a href="%s">%s</a> is waiting for your moderation Review Category Review for Review pending moderation Search Categories Search Reviews Select order of items listed on page Select order of reviews listed on page Send Rating Status Submit your rating Update Category View Review Your Name Your rating has been successfully sent add new on admin barReview admin menuItem Reviews name of elementItem Reviews post type general nameReviews post type singular nameReview reviewAdd New taxonomy general nameReview Categories taxonomy singular nameCategory Project-Id-Version: ait-item-reviews
POT-Creation-Date: 2016-08-24 14:41:36+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2017-09-18 08:27-0400
Language-Team: Serbian (Latin)
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
Language: sr_RS
 Dodavanje nove kategorije Dodaj novu recenziju Dodaje ocene i recenzije funkcionalnosti za City Guide Wordpress Temu Sve Sve kategorije Sve recenzije Odobriti Potvrđeno Uzlazno Prosek Kategorija Datum kreiranja Datum Silazno Opis Uređivanje kategorija Uređivanje recenzije Email obaveštenje Recenzija stavke Autor stavke odobri komentar  Stavka ne postoji Ostavite recenziju Maksimalno prikazani pregledi Naziv Naziv nove kategorije Nova Ocena Nova Recenzija Nova Ocena za <a href="%s"> %s</a> je odobren od strane administratora Nema ocene Nema recenzija u Trash. Nema pronađenih recenzija. Redosled Naručiti putem Nadređena kategorija Nadređena kategorija: Nadređena recenzija: Očekuje se odobravanje Molimo vas da popunite sva polja Objavljivanje ... Pitanje 1 Pitanje 2 Pitanje 3 Pitanje 4 Pitanje 5 Ocenite sada Ocena Recenzija Pregled <a href="%s"> #%d</a> za stavku <a href="%s"> %s</a> čeka svoju moderaciju Pregled kategorija Pregled za Pregled čeka moderiranje Pretraga kategorije Pretraga recenzije Odaberite redosled stavki na stranici Izaberi redosled recenzija izlistan na stranici Pošalji Ocenu Status Dostavite Vašu Ocenu Ažuriranje kategorije Pogledaj recenziju Vaše Ime Vaša ocena je uspešno poslata Recenzija Recenzija stavke Recenzija stavke Recenzije Recenzija Dodaj novo Pregled Kategorija Kategorija 