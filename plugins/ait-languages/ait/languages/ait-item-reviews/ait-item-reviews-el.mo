��    G      T  a   �                "  D   1     v     z     �     �     �  	   �     �     �     �     �  
   �     �     �     �               &     C     V     e     {     �  
   �  
   �  E   �  	   �     �          %     +     4     D     U     e     v     �  
   �  
   �  
   �  
   �  
   �     �     �     �  W   �     F	  
   V	     a	     {	     �	  $   �	  &   �	     �	     �	     �	     
     
  	   *
  &   4
     [
     w
     �
     �
     �
     �
  '   �
     !  !  A  .   c  *   �  e   �     #  "   *     M     l     {     �     �     �  +   �     �            +   )  '   U     }     �  A   �  +   �  "   *  K   M     �  (   �      �     �  t   
       C   �  )   �  
             2      R     s  !   �  D   �     �               /     @     Q     b     �     �  �   �  #   6     Z  3   p  '   �  #   �  e   �  e   V  '   �     �  #   �  '        C     c  E   z     �     �     �                /  %   I     o         E   	   D                 C      F   )   &   +   ,   -      .                        ?      5   !   @   8   =       $   0      6       <   /                                  >                 %      1   2          3       :       (   
       G                   B   ;   #      7   A   "   '   *       9                                 4                 Add New Category Add New Review Adds rating and reviews functionality for City Guide Wordpress Theme All All Categories All Reviews Approve Approved Ascending Average Category Creation Date Date Descending Description Edit Category Edit Review Email notifications Item Reviews Item author approve Reviews  Item doesn't exist Leave a Review Maximum shown reviews Name New Category Name New Rating New Review New rating for <a href="%s">%s</a> has been approved by administrator No rating No reviews found in Trash. No reviews found. Order Order By Parent Category Parent Category: Parent Reviews: Pending Approval Please fill out all fields Publishing ... Question 1 Question 2 Question 3 Question 4 Question 5 Rate now Rating Review Review <a href="%s">#%d</a> for item <a href="%s">%s</a> is waiting for your moderation Review Category Review for Review pending moderation Search Categories Search Reviews Select order of items listed on page Select order of reviews listed on page Send Rating Status Submit your rating Update Category View Review Your Name Your rating has been successfully sent add new on admin barReview admin menuItem Reviews name of elementItem Reviews post type general nameReviews post type singular nameReview reviewAdd New taxonomy general nameReview Categories taxonomy singular nameCategory Project-Id-Version: ait-item-reviews
POT-Creation-Date: 2016-08-24 14:41:36+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2017-08-22 08:48-0400
Language-Team: Greek
Plural-Forms: nplurals=2; plural=(n != 1);
Language: el
 Προσθήκη Νέας Κατηγορίας Προσθήκη νέας κριτικής Προσθέτει βαθμολογία και σχόλια για το City Guide Wordpress θέμα Όλα Όλες οι Κατηγορίες Όλες οι κριτικές Έγκριση Εγκρίθηκε Αύξουσα Μέσος όρος Κατηγορία Ημερομηνία δημιουργίας Ημερομηνία Φθίνουσα Περιγραφή Επεξεργασία Κατηγορίας Επεξεργασία κριτικής Ειδοποιήσεις Email Κριτικές σχολίου Ο συγγραφέας εγκρίνει τις Κριτικές  Το στοιχείο δεν υπάρχει Αφήστε μια κριτική Μέγιστος αριθμός προβαλλόμενων κριτικών Ονομασία Όνομα Νέας Κατηγορίας Η νέα βαθμολόγηση Νέα κριτική Η νέα βαθμολόγηση για <a href="%s"> %s</a> έχει εγκριθεί από διαχειριστή Χωρίς Βαθμολογία Δεν βρέθηκαν κριτικές στα σκουπίδια. Δεν βρέθηκαν κριτικές. Σειρά Ταξινόμιση ως Γονική κατηγορία Γονική κατηγορία: Γονικά σχόλια: Αναμένουν έγκριση Παρακαλούμε συμπληρώστε όλα τα πεδία Δημοσίευση... Ερώτηση 1 Ερώτηση 2 Ερώτηση 3 Ερώτηση 4 Ερώτηση 5 Αξιολογήστε τώρα Βαθμολογία Κριτική Αναθεώρηση <a href="%s"> #%d</a> για το στοιχείο <a href="%s"> %s</a> είναι σε αναμονή για έλεγχο Κατηγορία κριτικής Κριτική για Έλεγχοι οι οποίοι εκκρεμούν Αναζήτηση Κατηγοριών Αναζήτηση κριτικών Επιλογή διάταξης των ειδών που παρατίθονται στη σελίδα Επιλέξτε σειρά των σχολίων που παρατίθενται στη σελίδα Αποστολή βαθμολογίας Κατάσταση Υποβολή βαθμολογία Ενημέρωση Κατηγορίας Προβολή κριτικής Το όνομά σου Η Βαθμολογία σας έχει σταλεί επιτυχώς Κριτική Κριτικές σχολίου Κριτικές σχολίου Κριτικές Κριτική Προσθήκη Νέου Κριτικές Κατηγοριών Κατηγορία 