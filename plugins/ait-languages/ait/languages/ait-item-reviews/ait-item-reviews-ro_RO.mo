��    G      T  a   �                "  D   1     v     z     �     �     �  	   �     �     �     �     �  
   �     �     �     �               &     C     V     e     {     �  
   �  
   �  E   �  	   �     �          %     +     4     D     U     e     v     �  
   �  
   �  
   �  
   �  
   �     �     �     �  W   �     F	  
   V	     a	     {	     �	  $   �	  &   �	     �	     �	     �	     
     
  	   *
  &   4
     [
     w
     �
     �
     �
     �
  '   �
     !  S  A     �     �  J   �               #     4     ;  
   C     N  	   T     ^     k     p  	   ~     �     �     �     �  (   �                )     I     N     a     p  K   ~     �  3   �          &  
   /     :     L     _     q  +   �     �     �     �     �     �     	          %     .  o   7     �     �     �     �     �  0     1   >     p     �     �     �     �     �  %   �     �                "     +     4     =  	   P         E   	   D                 C      F   )   &   +   ,   -      .                        ?      5   !   @   8   =       $   0      6       <   /                                  >                 %      1   2          3       :       (   
       G                   B   ;   #      7   A   "   '   *       9                                 4                 Add New Category Add New Review Adds rating and reviews functionality for City Guide Wordpress Theme All All Categories All Reviews Approve Approved Ascending Average Category Creation Date Date Descending Description Edit Category Edit Review Email notifications Item Reviews Item author approve Reviews  Item doesn't exist Leave a Review Maximum shown reviews Name New Category Name New Rating New Review New rating for <a href="%s">%s</a> has been approved by administrator No rating No reviews found in Trash. No reviews found. Order Order By Parent Category Parent Category: Parent Reviews: Pending Approval Please fill out all fields Publishing ... Question 1 Question 2 Question 3 Question 4 Question 5 Rate now Rating Review Review <a href="%s">#%d</a> for item <a href="%s">%s</a> is waiting for your moderation Review Category Review for Review pending moderation Search Categories Search Reviews Select order of items listed on page Select order of reviews listed on page Send Rating Status Submit your rating Update Category View Review Your Name Your rating has been successfully sent add new on admin barReview admin menuItem Reviews name of elementItem Reviews post type general nameReviews post type singular nameReview reviewAdd New taxonomy general nameReview Categories taxonomy singular nameCategory Project-Id-Version: ait-item-reviews
POT-Creation-Date: 2016-08-24 14:41:36+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2017-09-18 08:27-0400
Language-Team: Romanian
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : (n==0 || (n%100>0 && n%100<20)) ? 1 : 2);
Language: ro_RO
 Adaugă Categorie Nouă Adauga Recenzie Noua Adaugă opțiuni de evaluare şi recenzie pentru Tema Wordpress City Guide Tot Toate categoriile Toate Recenziile Aproba Aprobat Crescător Media Categorie Data Crearii Data Descrescător Descriere Editează Categoria Editeaza Recenzia Notificări prin e-mail Recenzii Articol Autorul articolului aprobă Evaluările  Articolul nu există Lasă un comentariu Număr maxim evaluări afișate Nume Nume Nou Categorie Evaluare nouă Recenzie Noua Noua evaluare pentru <a href="%s"> %s</a> a fost aprobată de administrator Fără evaluare Nu a fost gasita nici o recenzie in Cosul de Gunoi. Nici o recenzie gasita. Comandă Comanda de Categoria de Baza Categoria de Baza: Recenzii de Baza: În aşteptarea aprobării Vă rugăm să completaţi toate câmpurile În curs de publicare... Întrebarea 1 Întrebarea 1 Întrebarea 3 Întrebarea 4 Întrebarea 5 Evaluati acum Evaluare Recenzie Evaluarea <a href="%s"> #%d</a> pentru articolul <a href="%s"> %s</a> este în aşteptare pentru a fi moderată Recenzii Categorie Recenzii pentru Evaluare în curs de moderare Cautare Categorii Căutaţi Recenzii Selectaţi comanda de elemente listate pe pagina Selectaţi ordinea evaluărilor listate pe pagina Trimite Recenzia Stare Prezinta evaluarea ta Actualizeaza Categoria Citeste Recenzia Numele tău Evaluarea ta a fost trimisa cu succes Recenzie Recenzii Articol Recenzii Articol Recenzii Recenzie Adăugă Recenzie Categorii Categorie 