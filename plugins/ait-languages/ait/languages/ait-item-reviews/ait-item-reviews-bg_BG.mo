��    G      T  a   �                "  D   1     v     z     �     �     �  	   �     �     �     �     �  
   �     �     �     �               &     C     V     e     {     �  
   �  
   �  E   �  	   �     �          %     +     4     D     U     e     v     �  
   �  
   �  
   �  
   �  
   �     �     �     �  W   �     F	  
   V	     a	     {	     �	  $   �	  &   �	     �	     �	     �	     
     
  	   *
  &   4
     [
     w
     �
     �
     �
     �
  '   �
     !  (  A  1   j  '   �  v   �     ;     H     h     �     �     �     �     �      �     �     �       .     $   N      s  "   �  4   �     �       2   %     X  '   _     �     �  h   �       9   .  '   h     �     �  '   �  (   �  "        '  5   C     y     �     �     �     �     �     �     �       p     &        �  ,   �  &   �        j   -  j   �          #  (   0  0   Y      �     �  D   �       "     "   0     S     `     i  $   �     �         E   	   D                 C      F   )   &   +   ,   -      .                        ?      5   !   @   8   =       $   0      6       <   /                                  >                 %      1   2          3       :       (   
       G                   B   ;   #      7   A   "   '   *       9                                 4                 Add New Category Add New Review Adds rating and reviews functionality for City Guide Wordpress Theme All All Categories All Reviews Approve Approved Ascending Average Category Creation Date Date Descending Description Edit Category Edit Review Email notifications Item Reviews Item author approve Reviews  Item doesn't exist Leave a Review Maximum shown reviews Name New Category Name New Rating New Review New rating for <a href="%s">%s</a> has been approved by administrator No rating No reviews found in Trash. No reviews found. Order Order By Parent Category Parent Category: Parent Reviews: Pending Approval Please fill out all fields Publishing ... Question 1 Question 2 Question 3 Question 4 Question 5 Rate now Rating Review Review <a href="%s">#%d</a> for item <a href="%s">%s</a> is waiting for your moderation Review Category Review for Review pending moderation Search Categories Search Reviews Select order of items listed on page Select order of reviews listed on page Send Rating Status Submit your rating Update Category View Review Your Name Your rating has been successfully sent add new on admin barReview admin menuItem Reviews name of elementItem Reviews post type general nameReviews post type singular nameReview reviewAdd New taxonomy general nameReview Categories taxonomy singular nameCategory Project-Id-Version: ait-item-reviews
POT-Creation-Date: 2016-08-24 14:41:36+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2016-12-05 07:13-0500
Language-Team: Bulgarian
Plural-Forms: nplurals=2; plural=(n != 1);
Language: bg_BG
 Добавяне на нова категория Добавяне на ново ревю Добавя функционалност за оценка и ревюта за City Guide WordPress шаблонът Всички Всички категории Всички ревюта Одобри Одобрен Възходящо Средно Категория Дата на създаване Дата Низходящо Описание Редактиране на категория Редактиране на ревю Известия по имейл Ревю на елементите Артикул автор одобри отзива  Тя не съществува Оставете ревю Максимално показани отзива Име Име на нова категория Нова оценка Ново ревю Нова оценка за <a href="%s">%s</a> беше одобрена от администратор Няма оценка Няма намерени ревюта в кошчето. Няма намерени ревюта. Поръчка Подреди по Родителска категория Родителска категория: Родителски ревюта: Чака одобрение Моля попълнете всички полета Публикуване... Въпрос 1 Въпрос 2 Въпрос 3 Въпрос 4 Въпрос 5 Оценете сега Рейтинг Ревю Ревю <a href="%s">#%d</a> за елемент <a href="%s">%s</a> чака вашето модериране Преглед на категория Ревюта за Ревюто очаква модерация Търсене на категории Търсене на ревюта Изберете подредбата на елементите показани на страницата Изберете подредбата на елементите показани на страницата Изпратете оценка Статус Подайте Вашата оценка Актуализация на категория Преглед на ревюто Вашето име Вашата оценка беше успешно изпратена Ревю Ревю на елементите Ревю на елементите Ревюта Ревю Добавяне на ново Категории на ревюта Категория 