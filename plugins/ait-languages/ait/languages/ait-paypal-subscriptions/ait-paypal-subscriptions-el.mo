��          �            x     y     �     �  L   �     �  #   	     -     9     H  2   P     �  �   �  >   �  ?   �       )  #  %   M     s     �  �   �  6   '  M   ^     �  '   �     �  r   �     q  �  �  �   /	  �   �	  0   G
           	                                                         
           API Password API Signature API Username Adds PayPal subscriptions (automatic recurring payments) to City Guide Theme After approving of payment After cancelling of payment process Credentials Enable logging Logging Logs are stored in wp-content/paypal-info.log file Redirections The correct URL setting for Instant Payment Notifications (IPN) on PayPal website is also required for handling automatic recurring payments. See details <a href="https://www.ait-themes.club/doc/paypal-subscriptions-plugin-theme-options/">here</a> Visitor is redirected to selected page after cancelled payment Visitor is redirected to selected page after successful payment Your URL for notifications: Project-Id-Version: ait-paypal-subscriptions
POT-Creation-Date: 2016-10-04 13:36:31+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2017-08-22 08:48-0400
Language-Team: Greek
Plural-Forms: nplurals=2; plural=(n != 1);
Language: el
 API κωδικός πρόσβασης API Υπογραφή API Όνομα χρήστη Προσθέτει συνδρομές PayPal (αυτόματες περιοδικές πληρωμές) στο Θέμα City Guide Μετά την έγκριση της πληρωμής Μετά την ακύρωση της διαδικασίας πληρωμής Πιστοποιήσεις Ενεργοποίηση Εισόδου Είσοδος Τα αρχεία καταγραφής αποθηκεύονται στο αρχείο wp-content / paypal-info.log Ανακατευθύνσεις Απαιτείται επίσης τη σωστή διεύθυνση URL ρύθμιση για Instant ειδοποιήσεις πληρωμής (IPN) στην ιστοσελίδα του PayPal για το χειρισμό αυτόματες περιοδικές πληρωμές. Δείτε λεπτομέρειες <a href="https://www.ait-themes.club/doc/paypal-subscriptions-plugin-theme-options/"> εδώ</a> Επισκέπτης ανακατευθύνεται σε επιλεγμένη σελίδα μετά την ακυρωθείσα πληρωμή Επισκέπτης ανακατευθύνεται σε επιλεγμένη σελίδα μετά την επιτυχή πληρωμή Το URL σας για ειδοποιήσεις: 