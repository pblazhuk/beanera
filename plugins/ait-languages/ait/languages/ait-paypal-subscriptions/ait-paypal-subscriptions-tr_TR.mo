��          �            x     y     �     �  L   �     �  #   	     -     9     H  2   P     �  �   �  >   �  ?   �       .  #  
   R     ]     f  L   {     �     �               4  D   =     �  -  �  G   �  F        O           	                                                         
           API Password API Signature API Username Adds PayPal subscriptions (automatic recurring payments) to City Guide Theme After approving of payment After cancelling of payment process Credentials Enable logging Logging Logs are stored in wp-content/paypal-info.log file Redirections The correct URL setting for Instant Payment Notifications (IPN) on PayPal website is also required for handling automatic recurring payments. See details <a href="https://www.ait-themes.club/doc/paypal-subscriptions-plugin-theme-options/">here</a> Visitor is redirected to selected page after cancelled payment Visitor is redirected to selected page after successful payment Your URL for notifications: Project-Id-Version: ait-paypal-subscriptions
POT-Creation-Date: 2016-10-04 13:36:31+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2017-09-18 08:27-0400
Language-Team: Turkish
Plural-Forms: nplurals=2; plural=(n != 1);
Language: tr_TR
 API şifre API imza API kullanıcı adı City Guide temasına PayPal abonelikleri ekler (otomatik düzenli ödemeler) Ödeme onaylanmasından sonra Ödemi süreci iptalinden sonra Kimlik bilgileri Kayıt tutmayı etkinleştir Günlük Günlük dosyaları wp-content/paypal-info.log dosyasında saklanır Yönlendirmeler PayPal web sitesindeki Anında Ödeme Bildirimi (Instand Payment Notifications - IPN) için doğru URL ayarı, aynı zamanda otomatik düzenleme ödemeleri ayarlamak için de gereklidir. Detaylar için <a href="https://www.ait-themes.club/doc/paypal-subscriptions-plugin-theme-options/">tıklayın</a> Ziyaretçi, iptal edilen ödeme sonrası seçili sayfaya yönlendirilir Ziyaretçi, başarılı ödeme sonrası seçili sayfaya yönlendirilir Bildirimler için URL'niz: 