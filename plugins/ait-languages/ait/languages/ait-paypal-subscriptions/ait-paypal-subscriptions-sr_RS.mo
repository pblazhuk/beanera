��          �            x     y     �     �  L   �     �  #   	     -     9     H  2   P     �  �   �  >   �  ?   �       �  #     �  
   �     �  L   �       #   8  	   \     f     �  8   �     �     �  B   �  B        ^           	                                                         
           API Password API Signature API Username Adds PayPal subscriptions (automatic recurring payments) to City Guide Theme After approving of payment After cancelling of payment process Credentials Enable logging Logging Logs are stored in wp-content/paypal-info.log file Redirections The correct URL setting for Instant Payment Notifications (IPN) on PayPal website is also required for handling automatic recurring payments. See details <a href="https://www.ait-themes.club/doc/paypal-subscriptions-plugin-theme-options/">here</a> Visitor is redirected to selected page after cancelled payment Visitor is redirected to selected page after successful payment Your URL for notifications: Project-Id-Version: ait-paypal-subscriptions
POT-Creation-Date: 2016-10-04 13:36:31+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2017-09-18 08:27-0400
Language-Team: Serbian (Latin)
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
Language: sr_RS
 API lozinka API potpis API Korisničko Ime Dodaj PayPal pretplate (automatsko ponavljaju plaćanja) za City Guide Themu Nakon odobravanja plaćanja Nakon otkazivanja procesa plaćanja Akreditiv Onemogućiti prijavljivanje Prijavljivanje Evidencije se čuvaju na wp-content/paypal-info.log file Preusmerenja Ispravna URL podešavanja za Instant Payment Notifications (IPN) na PayPal sajtu je takođe zahtevano za rukovanje automatskim ponavljanju plaćanja. Vidi detalje <a href="https://www.ait-themes.club/doc/paypal-subscriptions-plugin-theme-options/">here</a> Posetilac je preusmeren na odabranu stranicu nakon otkazane uplate Posetilac je preusmeren na odabranu stranicu nakon uspešne uplate Vaš URL za obaveštenja: 