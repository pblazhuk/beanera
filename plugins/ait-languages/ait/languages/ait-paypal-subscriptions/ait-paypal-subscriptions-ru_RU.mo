��          �            x     y     �     �  L   �     �  #   	     -     9     H  2   P     �  �   �  >   �  ?   �       x  #     �     �  #   �  �   �  2   j  5   �  8   �  '        4  U   K     �  C  �  �   	  �   �	  4   
           	                                                         
           API Password API Signature API Username Adds PayPal subscriptions (automatic recurring payments) to City Guide Theme After approving of payment After cancelling of payment process Credentials Enable logging Logging Logs are stored in wp-content/paypal-info.log file Redirections The correct URL setting for Instant Payment Notifications (IPN) on PayPal website is also required for handling automatic recurring payments. See details <a href="https://www.ait-themes.club/doc/paypal-subscriptions-plugin-theme-options/">here</a> Visitor is redirected to selected page after cancelled payment Visitor is redirected to selected page after successful payment Your URL for notifications: Project-Id-Version: ait-paypal-subscriptions
POT-Creation-Date: 2016-10-04 13:36:31+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2017-08-28 06:35-0400
Language-Team: Russian
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
Language: ru_RU
 Пароль API Подпись API Имя пользователя API Добавляет подписки PayPal (автоматически повторяющиеся платежи) City Guide темы После подтверждения оплаты После отмены процесса оплаты Специальные настройки доступа Включить логирование Логирование Журналы хранятся в файле wp содержания/paypal-info.log Перенаправления Также необходимо установить правильную настройку URL для мгновенных уведомлений об оплате (IPN) на сайте PayPal. Более подробно <a href="https://www.ait-themes.club/doc/paypal-subscriptions-plugin-theme-options/">здесь</a> Посетитель, после отмены оплаты, перенаправляется на выбранную страницу Посетитель, после успешной оплаты, будет перенаправлен на выбранную страницу Ваш URL-адрес для уведомлений: 