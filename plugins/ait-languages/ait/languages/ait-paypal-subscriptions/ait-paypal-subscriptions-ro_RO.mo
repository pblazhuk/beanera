��          �            x     y     �     �  L   �     �  #   	     -     9     H  2   P     �  �   �  >   �  ?   �       [  #  
        �     �  J   �     �  #        5     A  	   W  F   a     �    �  Y   �  Y   &     �           	                                                         
           API Password API Signature API Username Adds PayPal subscriptions (automatic recurring payments) to City Guide Theme After approving of payment After cancelling of payment process Credentials Enable logging Logging Logs are stored in wp-content/paypal-info.log file Redirections The correct URL setting for Instant Payment Notifications (IPN) on PayPal website is also required for handling automatic recurring payments. See details <a href="https://www.ait-themes.club/doc/paypal-subscriptions-plugin-theme-options/">here</a> Visitor is redirected to selected page after cancelled payment Visitor is redirected to selected page after successful payment Your URL for notifications: Project-Id-Version: ait-paypal-subscriptions
POT-Creation-Date: 2016-10-04 13:36:31+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2017-09-18 08:27-0400
Language-Team: Romanian
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : (n==0 || (n%100>0 && n%100<20)) ? 1 : 2);
Language: ro_RO
 Parola API Semnatura API Nume de Utilizator API Adaugă abonamente PayPal (Plăţi automate recurente) în Tema City Guide După aprobarea platii După anularea procesului de plată Acreditări Activare Inregistrare Conectare Jurnalele sunt stocate în fişierul wp-conţinut/debug-paypal-log.log Redirectionari Setarea URL-ului corect pentru Notificări de Plată Instantanee (IPN) pe site-ul PayPal este, de asemenea, necesar pentru manipularea plăţilor automate recurente. Vedeți detalii <a href="https://www.ait-themes.club/doc/paypal-subscriptions-plugin-theme-options/"> aici</a> Vizitatorul este redirecționat către pagina selectată după plata efectuată cu succes Vizitatorul este redirecționat către pagina selectată după plata efectuată cu succes URL-ul pentru notificări: 