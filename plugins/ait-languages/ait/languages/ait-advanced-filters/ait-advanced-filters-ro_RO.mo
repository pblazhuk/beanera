��          �   %   �      `     a  3   p     �     �     �     �     �     �     �     �            ?        X     e     r  '   �     �     �     �     �     �     �     �  &   	  &   0  W  W     �  =   �                     ;     Q     `     h     o     �  
   �  \   �     �            -   +     Y     i     z     �     �     �     �     �     �                           
      	                                                                                           Add New Filter Adds advanced content filtering for CityGuide theme Advanced Filters All Filters Assign filters to Item Checkbox Edit Filter Enabled Filter Filter Columns Filters Icon Icon image displayed with filter as feature on Item detail page Insert Image Item Filters New Filter Name Number of columns displayed on frontend Parent Filter Parent Filter: Search Filters Select Image Type Unsupported filter Update Filter taxonomy general nameAdvanced Filters taxonomy singular nameAdvanced Filter Project-Id-Version: ait-advanced-filters
POT-Creation-Date: 2016-10-18 12:27:09+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2017-09-18 08:27-0400
Language-Team: Romanian
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : (n==0 || (n%100>0 && n%100<20)) ? 1 : 2);
Language: ro_RO
 Adaugă filtru nou Adaugă filtrare avansată de conținut pentru tema CityGuide Filtre avansate Toate filtrele Atribuie filtre elementului Căsuţă de validare Editare filtru Activat Filtru Coloane de filtrare Filtre Pictograma Imaginea tip icoană afișată cu filtru ca și caracteristică pe pagina detaliată articol Inserare Imagine Filtrele elementului Nume nou de filtru Număr de coloane afișate pe pagina de start Filtru Părinte Filtru Părinte: Filtre căutare Selectati Imaginea Tip Filtru neacceptat Actualizează filtru Filtre avansate Filtru avansat 