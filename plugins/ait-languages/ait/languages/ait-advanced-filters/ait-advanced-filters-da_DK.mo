��          �   %   �      `     a  3   p     �     �     �     �     �     �     �     �            ?        X     e     r  '   �     �     �     �     �     �     �     �  &   	  &   0  )  W     �  9   �     �     �     �            	   $     .     5     E     L  @   Q     �     �     �  *   �     �     �               &     ,     G     V     h                           
      	                                                                                           Add New Filter Adds advanced content filtering for CityGuide theme Advanced Filters All Filters Assign filters to Item Checkbox Edit Filter Enabled Filter Filter Columns Filters Icon Icon image displayed with filter as feature on Item detail page Insert Image Item Filters New Filter Name Number of columns displayed on frontend Parent Filter Parent Filter: Search Filters Select Image Type Unsupported filter Update Filter taxonomy general nameAdvanced Filters taxonomy singular nameAdvanced Filter Project-Id-Version: ait-advanced-filters
POT-Creation-Date: 2016-10-18 12:27:09+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2017-04-12 08:47-0400
Language-Team: Danish
Plural-Forms: nplurals=2; plural=(n != 1);
Language: da_DK
 Tilføj Ny Filter Tilføjer avanceret indholdsfiltrering for CityGuide tema Avancerede filtre Alle Filtre Tildel filtre til Stedet Afkrydsningsfelt Rediger Filter Aktiveret Filter Filter Kolonner Filtre Ikon Ikonbillede vises med filter som feature på stedets detaljeside Indsæt Billede Sted Filtre Nyt Filter Navn Antallet af kolonner, der vises i frontend Forældre Filter Forældre Filter: Søgefiltre Vælg Billede Skriv Filtret understøttes ikke Opdater Filter Avancerede filtre Avanceret Filter 