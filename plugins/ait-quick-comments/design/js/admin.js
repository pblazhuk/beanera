/* AIT Plugin Product Questions */
/* Admin Script */
jQuery(document).ready(function(){

	jQuery('#ait-quick-comments-page #quick-comments-save-options').on('click', function(e){
		e.preventDefault();
		$('#action-indicator-save').addClass('action-working').show();
		jQuery('#ait-quick-comments-page form').trigger('submit');
	});

	jQuery('#quick-comments-reset-options').on('click', function(e){
		e.preventDefault();
		if(confirm(jQuery(this).data('confirm'))){
			window.location.replace(jQuery(this).data('action'));
		}
	});

	jQuery('#quick-comments-export-subscribers').on('click', function(e){
		e.preventDefault();
		$.fileDownload(ajaxurl, {
			httpMethod: "POST",
			data: {
				'action': 'adminExportSubscribers'
			},
		});
	});

	jQuery('.settings_page_ait_quick_comments_options #display-subscribers').on('click', function(e){
		e.preventDefault();
		var $context = jQuery('.settings_page_ait_quick_comments_options');
		var $ajaxContainer = $context.find('.ajax-subscribers-container');
		jQuery.get(
			ajaxurl,
			{
				'action': 'adminRenderSubscribers',
			}
		).done(function(xhr){
			var ajaxData = JSON.parse(xhr);
			$ajaxContainer.html(ajaxData.html);

			jQuery('#quick-comments-export-subscribers').on('click', function(e){
				e.preventDefault();
				$.fileDownload(ajaxurl, {
					httpMethod: "POST",
					data: {
						'action': 'adminExportSubscribers',
						'data': 'simple'
					},
				});
			});
			jQuery('#quick-comments-export-subscribers-all').on('click', function(e){
				e.preventDefault();
				$.fileDownload(ajaxurl, {
					httpMethod: "POST",
					data: {
						'action': 'adminExportSubscribers',
						'data': 'all'
					},
				});
			});
		}).fail(function(xhr){
			console.log(xhr);
		});
	});

	jQuery('.edit-comments-php #comments-form').find('a.private-message').each(function(){
		jQuery(this).on('click', function(e){
			e.preventDefault();
			var $ajaxData = {};
			var $hrefLink = jQuery(this).attr('href');

			$ajaxData['comment_author'] = jQuery(this).attr('data-comment-author');
			$ajaxData['comment_author_email'] = jQuery(this).attr('data-comment-author-email');
			$ajaxData['comment_author_IP'] = jQuery(this).attr('data-comment-author-ip');
			$ajaxData['comment_content'] = jQuery(this).attr('data-comment-content');

			jQuery.post(ajaxurl,{
				'action': 'sendPrivateQuestion',
				'data': $ajaxData
			}).done(function(xhr){
				window.location.href = $hrefLink;
			});
		});
	});
});