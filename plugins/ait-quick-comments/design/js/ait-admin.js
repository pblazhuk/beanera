/* AIT Plugin Product Questions */
/* Admin Script */
jQuery(document).ready(function(){

	var $context = jQuery('.ait_quick_comments_options');

	var $select = $context.find('.ait-opt-page-select .chosen-wrapper select');
	$select.addClass('chosen').chosen();
	$context.find('.ait-opt-color .ait-colorpicker').colorpicker();
	ait.admin.options.Ui.onoff($context);

	var currentPage = "quick-comments";
	var id = '#ait-' + currentPage;
	new ait.admin.Tabs(jQuery(id + '-tabs'), jQuery(id + '-panels'), 'ait-admin-' + currentPage + '-page');
	//console.log(ait.admin);
});

jQuery(document).bind('ait-tabs-inited', function(e, panel){
	var panelId = panel.replace('#', "");
	panelId = panelId.replace('ait-quick-comments-', "");
	panelId = panelId.replace('-panel', "");
	if(panelId === "subscription"){
		var $context = jQuery('.ait_quick_comments_options');
		var $ajaxContainer = $context.find('#ait-quick-comments-subscription-panel .ait-ajax-section-subscribers');
		var $ajaxDataContainer = $ajaxContainer.find('.ait-ajax-section-data');
		jQuery.get(
			ajaxurl,
			{
				'action': 'adminRenderSubscribers',
			}
		).done(function(xhr){
			var ajaxData = JSON.parse(xhr);
			$ajaxDataContainer.html(ajaxData.html);

			jQuery('#quick-comments-export-subscribers').on('click', function(e){
				e.preventDefault();
				$.fileDownload(ajaxurl, {
					httpMethod: "POST",
					data: {
						'action': 'adminExportSubscribers',
						'data': 'simple'
					},
				});
			});
			jQuery('#quick-comments-export-subscribers-all').on('click', function(e){
				e.preventDefault();
				$.fileDownload(ajaxurl, {
					httpMethod: "POST",
					data: {
						'action': 'adminExportSubscribers',
						'data': 'all'
					},
				});
			});
		}).fail(function(xhr){
			console.log(xhr);
		});
	}
});

jQuery(document).bind('ait-tabs-switched', function(e, panel){
	var panelId = panel.replace('#', "");
	panelId = panelId.replace('ait-quick-comments-', "");
	panelId = panelId.replace('-panel', "");
	if(panelId === "subscription"){
		var $context = jQuery('.ait_quick_comments_options');
		var $ajaxContainer = $context.find('#ait-quick-comments-subscription-panel .ait-ajax-section-subscribers');
		var $ajaxDataContainer = $ajaxContainer.find('.ait-ajax-section-data');
		jQuery.get(
			ajaxurl,
			{
				'action': 'adminRenderSubscribers',
			}
		).done(function(xhr){
			var ajaxData = JSON.parse(xhr);
			$ajaxDataContainer.html(ajaxData.html);

			jQuery('#quick-comments-export-subscribers').on('click', function(e){
				e.preventDefault();
				$.fileDownload(ajaxurl, {
					httpMethod: "POST",
					data: {
						'action': 'adminExportSubscribers',
						'data': 'simple'
					},
				});
			});
			jQuery('#quick-comments-export-subscribers-all').on('click', function(e){
				e.preventDefault();
				$.fileDownload(ajaxurl, {
					httpMethod: "POST",
					data: {
						'action': 'adminExportSubscribers',
						'data': 'all'
					},
				});
			});
		}).fail(function(xhr){
			console.log(xhr);
		});
	}
});