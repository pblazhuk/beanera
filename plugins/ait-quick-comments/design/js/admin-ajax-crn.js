/* Admin Ajax Comment Reply Notification Script */
jQuery(document).ready(function(){
	jQuery('body a.vim-r').each(function(){
		jQuery(this).on('click', function(){
			// bind the reply click functions
			var fromName = jQuery(this).attr('data-reply-from');
			jQuery('a.save #replybtn').on('click', function(e){
				e.preventDefault();
				e.stopPropagation();
				var $comment = jQuery('#comment-'+commentReply.cid);
				var reply = {};
				reply['to'] = $comment.find('.author a[href^="mailto:"]').html();
				reply['msg'] = jQuery('#replycontent').val();
				reply['link'] = $comment.find('.response-links').children('a.comments-view-item-link').attr('href')+'#comment-'+commentReply.cid;
				reply['fromName'] = fromName;
				jQuery.post(
					ajaxurl,
					{
						'action': 'sendReplyNotification',
						'data': reply
					}
				).done(function(xhr){
					commentReply.send();
				}).fail(function(xhr){
					console.log("Ajax reply failed to send the mail");
				});
			});
		});
	});
});