/* AIT Plugin Quick Comments */
/* Main Script */
jQuery(document).ready(function(){

	jQuery('.quick-comments-container .quick-comments-toggle').on('click', function(){
		var $container = jQuery('.quick-comments-container');
		jQuery(this).hide();
		$container.find('.quick-comments-form').show();
		$container.addClass('form-shown');

		if($container.find('.form-captcha img').length == 0){
			jQuery.get(
				jQuery('.quick-comments-container .quick-comments-form form').attr('action'),
				{
					'action': 'loadCaptcha',
					'data': null
				}
			).done(function(xhr){
				var data = JSON.parse(xhr);
				var $container = jQuery('.quick-comments-container .quick-comments-form form');
				
				$container.find('input[name="rand"]').val(data.rand);
				// replace this with proper image
				//$container.find('.form-captcha img').attr('src', data.url);
				jQuery(data.html).insertBefore($container.find('input[name=captcha_check]'));
				
				jQuery('.quick-comments-container').find('.form-value-overlay').hide();
			});
		}
	});

	jQuery('.quick-comments-container .quick-comments-form .form-header i').on('click', function(){
		var $container = jQuery('.quick-comments-container');
		$container.find('.quick-comments-form').hide();
		$container.find('.quick-comments-toggle').show();
		$container.removeClass('form-shown');
	});

	jQuery('.quick-comments-container .quick-comments-form .form-overflow').on('click', function(){
		var $animationContainer = jQuery(this);
		$animationContainer.fadeOut('fast');
		$animationContainer.find('.form-notification').hide();
	});

	if(typeof jQuery.cookie("qc-comment_author") != "undefined"){
		jQuery('.quick-comments-container .quick-comments-form form').find('input[name=comment_author]').val(jQuery.cookie("qc-comment_author"))
	}
	if(typeof jQuery.cookie("qc-comment_author_email") != "undefined"){
		jQuery('.quick-comments-container .quick-comments-form form').find('input[name=comment_author_email]').val(jQuery.cookie("qc-comment_author_email"))
	}

	// load new captcha by ajax .. fix for caching plugins
	
	/*jQuery.get(
		jQuery('.quick-comments-container .quick-comments-form form').attr('action'),
		{
			'action': 'loadCaptcha',
			'data': null
		}
	).done(function(xhr){
		var data = JSON.parse(xhr);
		var $container = jQuery('.quick-comments-container .quick-comments-form form');
		var oldRand = $container.find('input[name="rand"]').val();
		$container.find('input[name="rand"]').val(data.rand);
		var oldHref = $container.find('.form-captcha img').attr('src');
		if(oldHref){
			$container.find('.form-captcha img').attr('src', oldHref.replace(oldRand, data.rand));
		}
	});*/

	jQuery('.quick-comments-container .quick-comments-form form').submit(function(e){
		var $animationContainer = jQuery('.quick-comments-container .quick-comments-form .form-overflow');
		var $inputs = jQuery(this).find('input:not(:submit):not(:checkbox), textarea');
		var $pmInput = jQuery(this).find('input[name=private_message]');
		var $sbInput = jQuery(this).find('input[name=newsletter_subscribe]');
		var $invalid = [];
		var $ajaxData = {};

		// display form overflow
		$inputs.removeClass('invalid');
		$animationContainer.fadeIn('fast');

		$inputs.each(function(){
			if(jQuery(this).val() != ""){
				if(jQuery(this).attr('name') == "comment_author_email"){
					var emailregex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
					if(emailregex.test(jQuery(this).val())){
						$ajaxData[jQuery(this).attr('name')] = jQuery(this).val();
					} else {
						$invalid.push(jQuery(this));
					}
				} else {
					$ajaxData[jQuery(this).attr('name')] = jQuery(this).val();
				}
			} else {
				$invalid.push(jQuery(this));
			}
		});
		$ajaxData['newsletter_subscribe'] = $sbInput.is(':checked') == true ? 1 : 0;

		if($invalid.length > 0){
			var $displayedNotification = $animationContainer.find('.form-notification.form-validation-error');
			$displayedNotification.show();

			// display all invalids
			jQuery.each($invalid, function(index, element){
				element.addClass('invalid');
			});

			/*setTimeout(function(){
				$animationContainer.fadeOut('fast');
				$animationContainer.find('.form-notification').hide();
			}, 3000);*/
		} else {
			var $displayedNotification = $animationContainer.find('.form-notification.form-ajax-sending');
			$displayedNotification.show();
			// send it

			if($pmInput.is(':checked')){
				jQuery.post(
					jQuery(this).attr('action'),
					{
						'action': 'sendPrivateQuestion',
						'data': $ajaxData
					}
				).done(function(xhr){
					$displayedNotification = $animationContainer.find('.form-notification.form-ajax-success');
					$displayedNotification.show();

					// reset everything
					$inputs.each(function(){
						if(jQuery(this).attr('type') != "hidden"){
							if(jQuery(this).attr('name') != 'comment_author' && jQuery(this).attr('name') != 'comment_author_email'){
								jQuery(this).val("");
								jQuery(this).removeClass('invalid');
							} else {
								//store to seesion cookies
								jQuery.cookie("qc-"+jQuery(this).attr('name'), jQuery(this).val());
							}
						}
					});
					$pmInput.removeAttr('checked');
					$sbInput.attr('checked', true);

					if(jQuery('.quick-comments-container .form-captcha').length != 0){
						jQuery.get(
							jQuery('.quick-comments-container .quick-comments-form form').attr('action'),
							{
								'action': 'loadCaptcha',
								'data': null
							}
						).done(function(xhr){
							var data = JSON.parse(xhr);
							var $container = jQuery('.quick-comments-container .quick-comments-form form');
							var oldRand = $container.find('input[name="rand"]').val();
							$container.find('input[name="rand"]').val(data.rand);
							var oldHref = $container.find('.form-captcha img').attr('src');
							if(oldHref){
								$container.find('.form-captcha img').attr('src', oldHref.replace(oldRand, data.rand));
							}
						});
					}
				}).fail(function(xhr){
					var data = JSON.parse(xhr.responseText);
					$displayedNotification = $animationContainer.find('.form-notification.'+data.msg);
					$displayedNotification.show();

					// dont reset everything // maybe new captcha
				});
			} else {
				jQuery.post(
					jQuery(this).attr('action'),
					{
						'action': 'storeQuestion',
						'data': $ajaxData
					}
				).done(function(xhr){
					$displayedNotification = $animationContainer.find('.form-notification.form-ajax-success');
					$displayedNotification.show();

					// reset everything
					$inputs.each(function(){
						if(jQuery(this).attr('type') != "hidden"){
							if(jQuery(this).attr('name') != 'comment_author' && jQuery(this).attr('name') != 'comment_author_email'){
								jQuery(this).val("");
								jQuery(this).removeClass('invalid');
							} else {
								//store to seesion cookies
								jQuery.cookie("qc-"+jQuery(this).attr('name'), jQuery(this).val());
							}
						}
					});
					$pmInput.removeAttr('checked');
					$sbInput.attr('checked', true);

					if(jQuery('.quick-comments-container .form-captcha').length != 0){
						jQuery.get(
							jQuery('.quick-comments-container .quick-comments-form form').attr('action'),
							{
								'action': 'loadCaptcha',
								'data': null
							}
						).done(function(xhr){
							var data = JSON.parse(xhr);
							var $container = jQuery('.quick-comments-container .quick-comments-form form');
							var oldRand = $container.find('input[name="rand"]').val();
							$container.find('input[name="rand"]').val(data.rand);
							var oldHref = $container.find('.form-captcha img').attr('src');
							if(oldHref){
								$container.find('.form-captcha img').attr('src', oldHref.replace(oldRand, data.rand));
							}
						});
					}
				}).fail(function(xhr){
					var data = JSON.parse(xhr.responseText);
					$displayedNotification = $animationContainer.find('.form-notification.'+data.msg);
					$displayedNotification.show();

					// dont reset everything // maybe new captcha
				});
			}
		}

		return false;		// always return false
	});

});