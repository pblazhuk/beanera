<?php
$options = get_option('ait_quick_comments_options', array());
function quickCommentsGetText($text){
	$result = '';
	if(class_exists('AitLangs')){
		// ait themes
		$result = AitLangs::getCurrentLocaleText($text);
	} else {
		// 3rd party
		if(is_array($text)){
			// transition from ait theme with ait-langs plugin
			$result = $text['en_US'];
		} else {
			// clean install on 3rd party theme
			$result = $text;
		}
	}
	return $result;
}
?>

<div class="quick-comments-container">
	<div class="quick-comments-toggle">
		<i class="<?php echo $options['intro'] ?>"></i>
		<span class="toggle-text"><?php echo quickCommentsGetText($options['title']); ?></span>
	</div>
	<div class="quick-comments-form" style="display: none">
		<div class="form-header">
			<span><?php echo quickCommentsGetText($options['title']); ?></span>
			<i class="<?php echo $options['close'] ?>"></i>
		</div>
		<div class="form-body">
			<div class="form-overflow">
				<div class="form-notifications">
					<div class="form-notification form-validation-error">
						<span><?php echo quickCommentsGetText($options['form-validation-error']); ?></span>
					</div>
					<div class="form-notification form-ajax-sending">
						<span><?php echo quickCommentsGetText($options['form-ajax-sending']); ?></span>
					</div>
					<div class="form-notification form-ajax-success">
						<span><?php echo quickCommentsGetText($options['form-ajax-success']); ?></span>
					</div>
					<div class="form-notification form-captcha-error">
						<span><?php echo quickCommentsGetText($options['form-captcha-error']); ?></span>
					</div>
					<div class="form-notification form-ajax-error">
						<span><?php echo quickCommentsGetText($options['form-ajax-error']); ?></span>
					</div>
				</div>
			</div>
			<div class="form-message">
				<span><?php echo quickCommentsGetText($options['message']); ?></span>
			</div>
			<form action="<?php echo admin_url('admin-ajax.php'); ?>" method="POST" name="product-question">
				<?php $rand = rand(); ?>
				<input type="hidden" name="rand" value="<?php echo $rand; ?>">
				<?php
					$postId = intval($options['defaultPage']);
					$current = get_queried_object();
					if(!empty($current)){
						if(isset($current->ID)){
							$test = $current->ID;
						} else {
							$test = $current->term_id;
						}

						if(comments_open( $test )){
							$postId = $test;
						}
					}
				?>
				<input type="hidden" name="comment_post_ID" value="<?php echo $postId; ?>">
				<input type="hidden" name="comment_author_IP" value="<?php echo $_SERVER['REMOTE_ADDR']; ?>">

				<?php
					$protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
				?>

				<input type="hidden" name="current_page" value="<?php echo $protocol.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'] ?>">

				<div class="form-control form-label form-control-required form-label-info">
					<div class="form-label">
						<span><?php echo quickCommentsGetText($options['form-info-label']); ?></span>
					</div>
				</div>
				<div class="form-control form-input-author">
					<div class="form-value">
						<input type="text" name="comment_author" placeholder="<?php echo quickCommentsGetText($options['form-name-placeholder']); ?>" tabindex="1001" <?php if(isset($_COOKIE['qc-comment_author'])){echo 'value="'.$_COOKIE['qc-comment_author'].'"';} ?>>
					</div>
				</div>
				<div class="form-control form-input-email">
					<div class="form-value">
						<input type="text" name="comment_author_email" placeholder="<?php echo quickCommentsGetText($options['form-email-placeholder']); ?>" tabindex="1002" <?php if(isset($_COOKIE['qc-comment_author_email'])){echo 'value="'.$_COOKIE['qc-comment_author_email'].'"';} ?>>
					</div>
				</div>
				<div class="form-control form-label form-control-required form-label-data">
					<div class="form-label">
						<span><?php echo quickCommentsGetText($options['form-data-label']); ?></span>
					</div>
				</div>
				<div class="form-control form-input-content">
					<div class="form-value">
						<textarea name="comment_content" rows="5" tabindex="1003"></textarea>
					</div>
				</div>
				<?php if($options['privateEnabled']) { ?>
				<div class="form-control form-input-private">
					<div class="form-value">
						<input type="checkbox" name="private_message" tabindex="1004" <?php if($options['privateCheckedByDefault']) {echo "checked";} ?> /> <?php echo quickCommentsGetText($options['form-private-text']); ?>
					</div>
				</div>
				<?php } ?>
				<?php if($options['subscriptionEnabled']) { ?>
				<div class="form-control form-input-subscribe">
					<div class="form-value">
						<input type="checkbox" name="newsletter_subscribe" tabindex="1005" checked /> <?php echo quickCommentsGetText($options['form-subscribe-text']); ?>
					</div>
				</div>
				<?php } ?>
				<?php if($options['captchaEnabled']) { ?>
				<div class="form-control form-label form-control-required form-label-captha">
					<div class="form-label">
						<span><?php echo quickCommentsGetText($options['form-captcha-label']); ?></span>
					</div>
				</div>

				<?php
				/*if(!class_exists("AitReallySimpleCaptcha")){
					@include plugin_dir_path( __FILE__ ).'../captcha/ait-really-simple-captcha.php';
				}
				$captcha = new AitReallySimpleCaptcha();

				$imgUrl = "";
				if(class_exists("AitTheme")){
					$captcha->tmp_dir = aitPaths()->dir->cache . '/captcha';
					$cacheUrl = aitPaths()->url->cache . '/captcha';
				} else {
					$captcha->tmp_dir = plugin_dir_path( __FILE__ ) . '../captcha/cache';
					$captcha->fonts = array(
						plugin_dir_path( __FILE__ ) . '../design/font/gentium/GenBkBasR.ttf',
						plugin_dir_path( __FILE__ ) . '../design/font/gentium/GenBkBasI.ttf',
						plugin_dir_path( __FILE__ ) . '../design/font/gentium/GenBkBasBI.ttf',
						plugin_dir_path( __FILE__ ) . '../design/font/gentium/GenBkBasB.ttf'
					);
					$cacheUrl = plugin_dir_url( __FILE__ ) . '../captcha/cache';
				}
				$img = $captcha->generate_image('ait-quick-comments-captcha-'.$rand, $captcha->generate_random_word());
				$imgUrl = $cacheUrl."/".$img;*/
				?>

				<div class="form-control form-captcha form-input-captha">
					<div class="form-value-overlay">
						<i class="fa fa-refresh fa-spin"></i>
					</div>
					<div class="form-value">
						<input type="text" name="captcha_check" tabindex="1006">
					</div>
				</div>
				<?php } ?>
				<div class="form-control form-input-submit">
					<div class="form-value">
						<input type="submit" name="submit" value="<?php echo quickCommentsGetText($options['form-btn-label']); ?>" tabindex="1006">
					</div>
				</div>
			</form>
		</div>
		<div class="form-footer">
			<span><?php if($options['brandingEnabled'] == 1 && $options['brandingLink'] != ""){ ?><a href="<?php echo $options['brandingLink'] ?>" <?php if($options['brandingBlank'] == 1) { echo 'target="_blank"'; } ?> ><?php echo quickCommentsGetText($options['brandingLabel']); ?></a><?php } else { ?> &nbsp; <?php } ?></span>
		</div>
	</div>
</div>
