<?php

/*
Plugin Name: AIT Quick Comments
Plugin URI: http://ait-themes.club
Description: Quick and easy way to post comments across wordpress
Version: 2.25
Author: AitThemes.Club
Author URI: http://ait-themes.club
Text Domain: ait-quick-comments
Domain Path: /languages
License: GPLv2 or later
*/

/* trunk@r367 */

define('AIT_QUICK_COMMENTS_ENABLED', true);

$quickComments = AitQuickComments::getInstance();
$quickComments->init();

class AitQuickComments {
	public static $instance;
	private $paths;
	private $config;

	public function __construct(){
		// disabled in singleton
	}

	public function __clone(){
		// disabled in singleton
	}

	public static function getInstance(){
		if(!isset(self::$instance)){
			self::$instance = new self();
		}
		return self::$instance;
	}

	public function init(){

		// main plugin hooks
		register_activation_hook(__FILE__, array($this, 'onActivation') );
		register_deactivation_hook(__FILE__, array($this, 'onDeactivation') );

		add_action('plugins_loaded', array($this, 'onLoaded') );
		add_action('init', array($this, 'loadConfig'));

		// load template
		add_action( 'wp_footer', array($this, 'enqueueTemplate') );

		// load design
		add_action( 'wp_enqueue_scripts', array($this, 'enqueueDesign') );
		add_action( 'admin_enqueue_scripts', array($this, 'enqueueAdminDesign') );

		// register ajax
		add_action( 'wp_ajax_storeQuestion', array($this, 'ajaxStoreQuestion') );
		add_action( 'wp_ajax_nopriv_storeQuestion', array($this, 'ajaxStoreQuestion') );
		add_action( 'wp_ajax_sendPrivateQuestion', array($this, 'ajaxSendPrivateQuestion') );
		add_action( 'wp_ajax_nopriv_sendPrivateQuestion', array($this, 'ajaxSendPrivateQuestion') );
		add_action( 'wp_ajax_loadCaptcha', array($this, 'ajaxLoadCaptcha') );
		add_action( 'wp_ajax_nopriv_loadCaptcha', array($this, 'ajaxLoadCaptcha') );

		// subscribers render ajaxAdminRenderSubscribers
		add_action( 'wp_ajax_adminRenderSubscribers', array($this, 'ajaxAdminRenderSubscribers') );
		add_action( 'wp_ajax_adminExportSubscribers', array($this, 'ajaxAdminExportSubscribers') );

		// reply notification
		add_action( 'wp_ajax_sendReplyNotification', array($this, 'ajaxSendReplyNotification') );

		add_filter( 'plugin_action_links_' . plugin_basename(__FILE__), array($this, 'pluginActionLinks'));

		add_filter( 'comment_row_actions', array($this, 'commentActionLinks'), 10, 2);

		add_filter( 'admin_body_class', array($this, 'adminBodyClass'), 10, 1);
	}

	public function pluginActionLinks($links)
	{
		$link = sprintf('<a href="%s">%s</a>', admin_url('admin.php?page=ait_quick_comments_options'), __('Settings', 'ait-quick-comments'));
		array_unshift($links, $link);
		return $links;
	}

	public function commentActionLinks($links, $comment){
		$current_user = wp_get_current_user();
		if(isset($links['reply'])){
			$links['reply'] = str_replace('class=', "data-reply-from='".$current_user->display_name."' class=", $links['reply']);
		}

		$link = sprintf('<a class="private-message" href="%s" data-comment-author="%s" data-comment-author-email="%s" data-comment-author-ip="%s" data-comment-content="%s">%s</a>',
			wp_nonce_url(admin_url('comment.php?c='.$comment->comment_ID.'&action=trashcomment'), 'delete-comment_'.$comment->comment_ID),
			$comment->comment_author,
			$comment->comment_author_email,
			$comment->comment_author_IP,
			esc_attr($comment->comment_content),
			__('Private', 'ait-quick-comments')
		);
		array_push($links, $link);
		return $links;
	}

	public function onActivation(){
		if(get_option( 'ait_quick_comments_options' ) == false){
			update_option('ait_quick_comments_options', AitQuickComments::getInstance()->getDefaultOptions());
		}
		// create database table for storing information
		$this->createDataTables();
	}

	public function onDeactivation(){
		// nothing special here yet
	}

	public function onLoaded(){
		$this->loadPaths();

		add_action('admin_menu', array($this, 'adminMenu'));
		add_action('admin_init', array($this, 'registerSettings'));
		load_plugin_textdomain('ait-quick-comments', false, plugin_basename(dirname(__FILE__)) . '/languages');
	}

	public function loadPlugin(){
		$this->loadConfig();
		$this->loadPaths();
	}

	public function loadConfig(){
		$this->config = include plugin_dir_path( __FILE__ ).'config/config.php';
	}

	public function loadPaths(){
		$this->paths = array(
			'url' => array(
				'js' => plugin_dir_url( __FILE__ ).'design/js/',
				'css' => plugin_dir_url( __FILE__ ).'design/css/',
				'img' => plugin_dir_url( __FILE__ ).'design/img/',
				'tpl' => plugin_dir_url( __FILE__ ).'templates/',
			),
			'uri' => array(
				'js' => plugin_dir_path( __FILE__ ).'design/js/',
				'css' => plugin_dir_path( __FILE__ ).'design/css/',
				'img' => plugin_dir_path( __FILE__ ).'design/img/',
				'tpl' => plugin_dir_path( __FILE__ ).'templates/',
			),
		);
	}

	public function enqueueTemplate(){
		include $this->paths['uri']['tpl'].'product-questions.php';
	}

	public function enqueueDesign(){
		foreach($this->config['assets']['frontend']['css'] as $asset){
			if($asset['handle'] == "ait-product-questions-font-fontawesome"){
				if(!class_exists("AitTheme")){
					wp_enqueue_style( $asset['handle'], $this->paths['url']['css'].$asset['filename'], $asset['dependencies'], $asset['version'], $asset['media'] );
				}
			} else {
				wp_enqueue_style( $asset['handle'], $this->paths['url']['css'].$asset['filename'], $asset['dependencies'], $asset['version'], $asset['media'] );
			}
		}
		foreach($this->config['assets']['frontend']['js'] as $asset){
			wp_enqueue_script( $asset['handle'], $this->paths['url']['js'].$asset['filename'], $asset['dependencies'], $asset['version'], $asset['inFooter'] );
		}
	}

	public function enqueueAdminDesign($hook){
		foreach($this->config['assets']['admin']['css'] as $asset){
			wp_enqueue_style( $asset['handle'], $this->paths['url']['css'].$asset['filename'], $asset['dependencies'], $asset['version'], $asset['media'] );
		}
		foreach($this->config['assets']['admin']['js'] as $asset){
			wp_enqueue_script( $asset['handle'], $this->paths['url']['js'].$asset['filename'], $asset['dependencies'], $asset['version'], $asset['inFooter'] );
		}

		$page = "";
		if(!empty($_REQUEST['page'])){
			$page = $_REQUEST['page'];
		}

		if(class_exists("AitTheme") && $page == "ait_quick_comments_options"){
			$assetsUrl = aitPaths()->url->admin . '/assets';
			$min = ((defined('SCRIPT_DEBUG') and SCRIPT_DEBUG) or AIT_DEV) ? '' : '.min';

			wp_enqueue_style('ait-colorpicker', "{$assetsUrl}/libs/colorpicker/colorpicker.css", array(), '2.2.1');
			wp_enqueue_style('ait-jquery-chosen', "{$assetsUrl}/libs/chosen/chosen.css", array(), '0.9.10');
			wp_enqueue_style('jquery-ui', "{$assetsUrl}/libs/jquery-ui/jquery-ui.css", array('media-views'), AIT_THEME_VERSION);
			wp_enqueue_style('jquery-switch', "{$assetsUrl}/libs/jquery-switch/jquery.switch.css", array(), '0.4.1');
			wp_enqueue_style('ait-admin-style', "{$assetsUrl}/css/style.css", array('media-views'), AIT_THEME_VERSION);
			wp_enqueue_style('ait-admin-options-controls', "{$assetsUrl}/css/options-controls" . "" . ".css", array('ait-admin-style', 'ait-jquery-chosen'), AIT_THEME_VERSION);
			$fontCssFile = aitUrl('css', '/libs/font-awesome.min.css');
			if($fontCssFile){
				wp_enqueue_style('ait-font-awesome-select', $fontCssFile, array(), '4.2.0');
			}

			wp_enqueue_script('ait.admin', "{$assetsUrl}/js/ait.admin.js", array('media-editor'), AIT_THEME_VERSION, TRUE);

			AitAdmin::adminGlobalJsSettings();

			wp_enqueue_script('ait-jquery-filedownload', "{$assetsUrl}/libs/file-download/jquery.fileDownload{$min}.js", array('jquery', 'ait.admin'), '1.3.3', TRUE);

			wp_enqueue_script('ait-colorpicker', "{$assetsUrl}/libs/colorpicker/colorpicker{$min}.js", array('jquery'), '2.2.1', TRUE);
			wp_enqueue_script('ait-jquery-chosen', "{$assetsUrl}/libs/chosen/chosen.jquery{$min}.js", array('jquery'), '1.0.0', TRUE);
			wp_enqueue_script('ait-jquery-sheepit', "{$assetsUrl}/libs/sheepit/jquery.sheepItPlugin{$min}.js", array('jquery', 'ait.admin'), '1.1.1-ait-1', TRUE);
			wp_enqueue_script('ait-jquery-deparam', "{$assetsUrl}/libs/jquery-deparam/jquery-deparam{$min}.js", array('jquery', 'ait.admin'), FALSE, TRUE);
			wp_enqueue_script('ait-jquery-rangeinput', "{$assetsUrl}/libs/rangeinput/rangeinput.min.js", array('jquery', 'ait.admin'), '1.2.7', TRUE);
			wp_enqueue_script('ait-jquery-numberinput', "{$assetsUrl}/libs/numberinput/numberinput{$min}.js", array('jquery', 'ait.admin'), FALSE, TRUE);
			if(class_exists('AitLangs') and AitLangs::getCurrentLanguageCode() !== 'en'){
				wp_enqueue_script('ait-jquery-datepicker-translation', "{$assetsUrl}/libs/datepicker/jquery-ui-i18n{$min}.js", array('jquery', 'ait.admin', 'jquery-ui-datepicker'), FALSE, TRUE);
			}
			wp_enqueue_script('ait-jquery-switch', "{$assetsUrl}/libs/jquery-switch/jquery.switch{$min}.js", array('jquery', 'ait.admin'), FALSE, TRUE);
			wp_enqueue_script('ait-bootstrap-dropdowns', "{$assetsUrl}/libs/bootstrap-dropdowns/bootstrap-dropdowns{$min}.js", array('jquery', 'ait.admin'), FALSE, TRUE);

			//wp_enqueue_media();

			wp_enqueue_script('ait.admin.Tabs', "{$assetsUrl}/js/ait.admin.tabs.js", array('ait.admin', 'jquery'), AIT_THEME_VERSION, TRUE);
			wp_enqueue_script('ait.admin.options', "{$assetsUrl}/js/ait.admin.options.js", array('ait.admin', 'jquery', 'jquery-ui-tabs', 'ait-jquery-chosen', 'jquery-ui-datepicker'), AIT_THEME_VERSION, TRUE);
			//wp_enqueue_script('ait.admin.backup', "{$assetsUrl}/js/ait.admin.backup.js", array('ait.admin', 'jquery', 'ait-jquery-filedownload'), AIT_THEME_VERSION, TRUE);
			wp_enqueue_script('ait.admin.options.elements', "{$assetsUrl}/js/ait.admin.options.elements.js", array('ait.admin', 'ait.admin.options', 'jquery-ui-draggable', 'jquery-ui-droppable', 'jquery-ui-sortable'), AIT_THEME_VERSION, TRUE);
			//wp_enqueue_script('ait.admin.nav-menus', "{$assetsUrl}/js/ait.admin.nav-menus.js", array('ait.admin', 'ait.admin.options', 'jquery-ui-draggable', 'jquery-ui-droppable', 'jquery-ui-sortable'), AIT_THEME_VERSION, TRUE);

			// custom ui style
			wp_enqueue_style( 'ait-product-questions-aitadmin', $this->paths['url']['css']."ait-admin.css", array(), '1.0', 'all');
			// custom ui init
			wp_enqueue_script( 'ait-product-questions-aitadmin', $this->paths['url']['js']."ait-admin.js", array("ait.admin.options"), '1.0', true );
		} else {
			wp_enqueue_script('jquery-filedownload', $this->paths['url']['js']."libs/jquery.fileDownload.min.js", array('jquery'), '1.3.3', TRUE);
		}

		if($hook == 'edit-comments.php'){
			wp_enqueue_script( 'ait-commentreply-notification', $this->paths['url']['js']."admin-ajax-crn.js", array('jquery'), '1.0', true );
		}
	}

	public function createDataTables(){
		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		global $wpdb;

		$table_name = $wpdb->prefix . 'quickcomments_emails';
		$charset_collate = $wpdb->get_charset_collate();

		$sql = "CREATE TABLE IF NOT EXISTS $table_name (
			id mediumint(11) NOT NULL AUTO_INCREMENT,
			email varchar(255) NOT NULL,
			name varchar(255) NOT NULL,
			date timestamp DEFAULT CURRENT_TIMESTAMP,
			subscribed boolean DEFAULT 0 NOT NULL,
			data varchar(2048) DEFAULT '',
			PRIMARY KEY(`ID`)
		) $charset_collate;";

		dbDelta( $sql );
	}

	public function storeUser($email, $name, $subscribed = 0, $jsonData = null){
		global $wpdb;
		$query = array(
			"check" => "SELECT * FROM `".$wpdb->prefix."quickcomments_emails` WHERE `email` = '%s';",
			"store" => "INSERT INTO `".$wpdb->prefix."quickcomments_emails` (`email`, `name`, `subscribed`) VALUES ('%s', '%s', '%d');",
			"update" => "UPDATE `".$wpdb->prefix."quickcomments_emails` SET `subscribed`='%d' WHERE `email`='%s';",
		);
		if($jsonData != null){
			$query['store'] = "INSERT INTO `".$wpdb->prefix."quickcomments_emails` (`email`, `name`, `subscribed`, `data`) VALUES ('%s', '%s', '%d', '%s');";
		}

		$dbUser = $wpdb->get_results( $wpdb->prepare( $query['check'], $email ));
		if(count($dbUser) == 0){
			// dont have user ... store
			if($jsonData != null){
				$result = $wpdb->query( $wpdb->prepare( $query['store'] , $email, $name, $subscribed, $jsonData ));
			} else {
				$result = $wpdb->query( $wpdb->prepare( $query['store'] , $email, $name, $subscribed ));
			}
		} else {
			// already have the user .. check the subscription status
			$userData = reset($dbUser);
			if(intval($userData->subscribed) == 0){
				// not subscribed -> check current subscription status
				if(intval($subscribed) == 1){
					// this is new subscription status -> update user
					$result = $wpdb->query( $wpdb->prepare( $query['update'] , $subscribed, $email ));
				}
			}
		}
	}

	public function loadUsers($subscribed = true, $today = false){
		global $wpdb;
		$query = array(
			"check" => "SELECT * FROM `".$wpdb->prefix."quickcomments_emails` WHERE `subscribed` = '%d';",
		);
		if($today){
			$query['check'] = "SELECT * FROM `".$wpdb->prefix."quickcomments_emails` WHERE `subscribed` = '%d' AND `date` LIKE '%s';";
		}

		if($today){
			$result = $wpdb->get_results( $wpdb->prepare( $query['check'], $subscribed, "%".date("Y-m-d")."%" ));
		}else{
			$result = $wpdb->get_results( $wpdb->prepare( $query['check'], $subscribed ));
		}
		return $result;
	}

	public function ajaxLoadCaptcha(){
		$rand = rand();
		if(!class_exists("AitReallySimpleCaptcha")){
			@include plugin_dir_path( __FILE__ ).'captcha/ait-really-simple-captcha.php';
		}
		$captcha = new AitReallySimpleCaptcha();

		$imgUrl = "";
		if(class_exists("AitTheme")){
			$captcha->tmp_dir = aitPaths()->dir->cache . '/captcha';
			$cacheUrl = aitPaths()->url->cache . '/captcha';
		} else {
			$captcha->tmp_dir = plugin_dir_path( __FILE__ ) . 'captcha/cache';
			$captcha->fonts = array(
				plugin_dir_path( __FILE__ ) . 'design/font/gentium/GenBkBasR.ttf',
				plugin_dir_path( __FILE__ ) . 'design/font/gentium/GenBkBasI.ttf',
				plugin_dir_path( __FILE__ ) . 'design/font/gentium/GenBkBasBI.ttf',
				plugin_dir_path( __FILE__ ) . 'design/font/gentium/GenBkBasB.ttf'
			);
			$cacheUrl = plugin_dir_url( __FILE__ ) . 'captcha/cache';
		}
		$img = $captcha->generate_image('ait-quick-comments-captcha-'.$rand, $captcha->generate_random_word());
		$imgUrl = $cacheUrl."/".$img;

		echo json_encode(array('rand' => $rand, 'url' => $imgUrl, 'html' => '<img src="'.$imgUrl.'" alt="captcha">'));
		exit();
	}

	public function ajaxStoreQuestion(){
		if(!class_exists("AitReallySimpleCaptcha")){
			@include plugin_dir_path( __FILE__ ).'captcha/ait-really-simple-captcha.php';
		}
		$captcha = new AitReallySimpleCaptcha();

		if(class_exists("AitTheme")){
			$captcha->tmp_dir = aitPaths()->dir->cache . '/captcha';
		} else {
			$captcha->tmp_dir = plugin_dir_path( __FILE__ ) . 'captcha/cache';
			$captcha->fonts = array(
				plugin_dir_path( __FILE__ ) . 'design/font/gentium/GenBkBasR.ttf',
				plugin_dir_path( __FILE__ ) . 'design/font/gentium/GenBkBasI.ttf',
				plugin_dir_path( __FILE__ ) . 'design/font/gentium/GenBkBasBI.ttf',
				plugin_dir_path( __FILE__ ) . 'design/font/gentium/GenBkBasB.ttf'
			);
		}

		$quickComments = AitQuickComments::getInstance();

		$geoIpDataJson = file_get_contents('http://freegeoip.net/json/'.$_POST['data']['comment_author_IP']);
		$geoIpData = json_decode($geoIpDataJson);

		$data = array(
			'comment_post_ID'		=> $_POST['data']['comment_post_ID'],
			'comment_author_IP'		=> $_POST['data']['comment_author_IP'],
			'comment_type'			=> 'comment',
			'comment_date'			=> current_time('mysql'),
			'comment_approved'		=> 0,
			// data provided by user
			'comment_author'		=> $_POST['data']['comment_author'],
			'comment_author_email'	=> $_POST['data']['comment_author_email'],
			'comment_author_url'	=> "",
			'comment_content'		=> $_POST['data']['comment_content'],
		);

		if(isset($_POST['data']['captcha_check'])){
			if($captcha->check('ait-quick-comments-captcha-'.$_POST['data']['rand'], $_POST['data']['captcha_check'])){
				$comment_id = wp_new_comment($data);
				$quickComments->storeUser($_POST['data']['comment_author_email'], $_POST['data']['comment_author'], $_POST['data']['newsletter_subscribe'], $geoIpData);
				if($comment_id != null){
					// return success
					header("HTTP/1.0 200 OK");
				} else {
					// throw an error
					header("HTTP/1.0 400 Bad Request");
					echo json_encode(array('msg' => 'form-ajax-error'));
					exit();
				}
			} else {
				header("HTTP/1.0 400 Bad Request");
				echo json_encode(array('msg' => 'form-captcha-error'));
				exit();
			}
		} else {
			$comment_id = wp_new_comment($data);
			$quickComments->storeUser($_POST['data']['comment_author_email'], $_POST['data']['comment_author'], $_POST['data']['newsletter_subscribe'], $geoIpData);
			if($comment_id != null){
				// return success
				header("HTTP/1.0 200 OK");
			} else {
				// throw an error
				header("HTTP/1.0 400 Bad Request");
				echo json_encode(array('msg' => 'form-ajax-error'));
				exit();
			}
		}
	}

	public function ajaxSendPrivateQuestion(){
		if(!class_exists("AitReallySimpleCaptcha")){
			@include plugin_dir_path( __FILE__ ).'captcha/ait-really-simple-captcha.php';
		}
		$captcha = new AitReallySimpleCaptcha();

		if(class_exists("AitTheme")){
			$captcha->tmp_dir = aitPaths()->dir->cache . '/captcha';
		} else {
			$captcha->tmp_dir = plugin_dir_path( __FILE__ ) . 'captcha/cache';
			$captcha->fonts = array(
				plugin_dir_path( __FILE__ ) . 'design/font/gentium/GenBkBasR.ttf',
				plugin_dir_path( __FILE__ ) . 'design/font/gentium/GenBkBasI.ttf',
				plugin_dir_path( __FILE__ ) . 'design/font/gentium/GenBkBasBI.ttf',
				plugin_dir_path( __FILE__ ) . 'design/font/gentium/GenBkBasB.ttf'
			);
		}

		$quickComments = AitQuickComments::getInstance();

		$geoIpDataJson = file_get_contents('http://freegeoip.net/json/'.$_POST['data']['comment_author_IP']);
		$geoIpData = json_decode($geoIpDataJson);
		$location = "-";
		if(!empty($geoIpData)){
			if(isset($geoIpData->country_name) && !empty($geoIpData->country_name)){
				$location = $geoIpData->country_name;
			}
			if(isset($geoIpData->region_name) && !empty($geoIpData->region_name)){
				$location .= ' / '.$geoIpData->region_name;
			}
			if(isset($geoIpData->city) && !empty($geoIpData->city)){
				$location .= ' / '.$geoIpData->city;
			}
		}

		$content = $quickComments->getOptions('privateContent');

		$content = str_replace('[content]', stripslashes(nl2br($_POST['data']['comment_content'], false)), $content);
		$content = str_replace('[name]', $_POST['data']['comment_author'], $content);
		$content = str_replace('[email]', '<a href="mailto: '.$_POST['data']['comment_author_email'].'">'.$_POST['data']['comment_author_email'].'</a>', $content);
		$content = str_replace('[time]', current_time('mysql'), $content);
		$content = str_replace('[link]', '<a href="'.$_POST['data']['current_page'].'">'.$_POST['data']['current_page'].'</a>', $content);
		$content = str_replace('[location]', $location, $content);
		$content = str_replace('[branding]', '<a href="'.$quickComments->getOptions('brandingLink').'" style="color: #0074a2; text-decoration: none; font-weight: bold;">'.$quickComments->getOptions('brandingLabel').'</a>', $content);

		$subject = $quickComments->getOptions('privateSubject');
		$subject = str_replace('[name]', $_POST['data']['comment_author'], $subject);
		$subject = str_replace('[excerpt]', substr(stripslashes($_POST['data']['comment_content']), 0, 50), $subject);

		$fromName = $quickComments->getOptions('privateEmailFromName') != "" ? $quickComments->getOptions('privateEmailFromName') : "admin";
		$fromEmail = $quickComments->getOptions('privateEmailFromEmail') != "" ? $quickComments->getOptions('privateEmailFromEmail') : get_option('admin_email');
		$mailData = array(
			'to' => $quickComments->getOptions('privateEmail') != "" ? $quickComments->getOptions('privateEmail') : get_option('admin_email'),
			'subject' => $subject,
			'content' => $content,
			'headers' => array(
				'Content-Type: text/html; charset=UTF-8',
				'From: '.$fromName.' <'.$fromEmail.'>',
				'Reply-To: '.$_POST['data']['comment_author'].' <'.$_POST['data']['comment_author_email'].'>',
			),
		);

		if(isset($_POST['data']['captcha_check'])){
			if($captcha->check('ait-quick-comments-captcha-'.$_POST['data']['rand'], $_POST['data']['captcha_check'])){
				$status = wp_mail($mailData['to'], $mailData['subject'], $mailData['content'], $mailData['headers']);
				$quickComments->storeUser($_POST['data']['comment_author_email'], $_POST['data']['comment_author'], $_POST['data']['newsletter_subscribe'], $geoIpDataJson);
				if($status != null){
					// return success
					header("HTTP/1.0 200 OK");
				} else {
					// throw an error
					header("HTTP/1.0 400 Bad Request");
					echo json_encode(array('msg' => 'form-ajax-error'));
					exit();
				}
			} else {
				header("HTTP/1.0 400 Bad Request");
				echo json_encode(array('msg' => 'form-captcha-error'));
				exit();
			}
		} else {
			$status = wp_mail($mailData['to'], $mailData['subject'], $mailData['content'], $mailData['headers']);
			$quickComments->storeUser($_POST['data']['comment_author_email'], $_POST['data']['comment_author'], $_POST['data']['newsletter_subscribe'], $geoIpDataJson);
			if($status != null){
				// return success
				header("HTTP/1.0 200 OK");
			} else {
				// throw an error
				header("HTTP/1.0 400 Bad Request");
				echo json_encode(array('msg' => 'form-ajax-error'));
				exit();
			}
		}
	}

	public function ajaxSendReplyNotification(){
		$quickComments = AitQuickComments::getInstance();
		$fromName = $quickComments->getOptions('notificationHeaderFromName') != "" ? $quickComments->getOptions('notificationHeaderFromName') : "admin";
		$fromEmail = $quickComments->getOptions('notificationHeaderFromEmail') != "" ? $quickComments->getOptions('notificationHeaderFromEmail') : get_option('admin_email');

		$mailData = array(
			'to' => $_POST['data']['to'],
			'subject' => $quickComments->getOptions('notificationSubject'),
			'content' => $quickComments->getOptions('notificationContent'),
			'headers' => array(
				'Content-Type: text/html; charset=UTF-8',
				'From: '.$fromName.' <'.$fromEmail.'>',
			)
		);

		$mailData['content'] = str_replace('[content]', $_POST['data']['msg'], $mailData['content']);
		$mailData['content'] = str_replace('[name]', $_POST['data']['fromName'], $mailData['content']);
		$mailData['content'] = str_replace('[link]', '<a href="'.$_POST['data']['link'].'">'.$_POST['data']['link'].'</a>', $mailData['content']);
		$mailData['content'] = str_replace('[time]', current_time('mysql'), $mailData['content']);
		$mailData['content'] = str_replace('[branding]', '<a href="'.$quickComments->getOptions('brandingLink').'" style="color: #0074a2; text-decoration: none; font-weight: bold;">'.$quickComments->getOptions('brandingLabel').'</a>', $mailData['content']);

		$status = wp_mail($mailData['to'], $mailData['subject'], $mailData['content'], $mailData['headers']);
		if($status != false){
			header("HTTP/1.0 200 OK");
		} else {
			header("HTTP/1.0 400 Bad Request");
			echo json_encode(array('msg' => 'form-ajax-error'));
			exit();
		}
	}

	public function ajaxAdminRenderSubscribers(){
		$quickComments = AitQuickComments::getInstance();
		$subscribers = $quickComments->loadUsers(true, true);
		$result = "";
		if(is_array($subscribers) && count($subscribers) > 0){
			$result .= '<table class="ajax-subscribers">';
			$result .= '<tr><th>'.__('Email','ait-quick-comments').'</th><th>'.__('Name','ait-quick-comments').'</th><th>'.__('Date','ait-quick-comments').'</th></tr>';
			foreach ($subscribers as $key => $user) {
				$result .= '<tr><td>'.$user->email.'</td><td>'.$user->name.'</td><td>'.$user->date.'</td></tr>';
			}
			//if(class_exists("AitTheme")){
			$result .= '<tr><td colspan="3" class="table-actions">
				<a id="quick-comments-export-subscribers" href="#"  data-confirm="'.__('Are you sure?', 'ait-quick-comments').'" data-action="'.admin_url( "admin.php?page=ait_quick_comments_options&export-subscribers=true" ).'">'.__('Export All (email, name)', 'ait-quick-comments').'</a>
				 |
				<a id="quick-comments-export-subscribers-all" href="#"  data-confirm="'.__('Are you sure?', 'ait-quick-comments').'" data-action="'.admin_url( "admin.php?page=ait_quick_comments_options&export-subscribers=true" ).'">'.__('Export All (all)', 'ait-quick-comments').'</a>
				</td></tr>';
			//}
			$result .= '</table>';
		} else {
			$result .= '<table class="ajax-subscribers">';
			$result .= '<tr><th>'.__('Email','ait-quick-comments').'</th><th>'.__('Name','ait-quick-comments').'</th><th>'.__('Date','ait-quick-comments').'</th></tr>';
			$result .= '<tr><td colspan="3">'.__('There are no subscribers yet', 'ait-quick-comments').'</td></tr>';
			$result .= '<tr><td colspan="3" class="table-actions">
				<a id="quick-comments-export-subscribers" href="#"  data-confirm="'.__('Are you sure?', 'ait-quick-comments').'" data-action="'.admin_url( "admin.php?page=ait_quick_comments_options&export-subscribers=true" ).'">'.__('Export All (email, name)', 'ait-quick-comments').'</a>
				 |
				<a id="quick-comments-export-subscribers-all" href="#"  data-confirm="'.__('Are you sure?', 'ait-quick-comments').'" data-action="'.admin_url( "admin.php?page=ait_quick_comments_options&export-subscribers=true" ).'">'.__('Export All (all)', 'ait-quick-comments').'</a>
				</td></tr>';
			$result .= '</table>';
		}
		header("HTTP/1.0 200 OK");
		echo json_encode(array('data' => $subscribers, 'html' => $result));
		exit();
	}

	public function ajaxAdminExportSubscribers(){
		$quickComments = AitQuickComments::getInstance();
		$subscribers = $quickComments->loadUsers();
		// save to file and send headers attachment
		$export = "";
		if($_POST['data'] == 'simple'){
			foreach ($subscribers as $key => $user) {
				$export .= $user->email.",".$user->name."\r\n";
			}
		} else {
			foreach ($subscribers as $key => $user) {
				$data = "";
				$dataJson = json_decode($user->data);
				if($dataJson != null){
					$data = $dataJson->ip.','.$dataJson->country_code.','.$dataJson->country_name.','.$dataJson->region_code.','.$dataJson->region_name.','.$dataJson->city.','.$dataJson->zip_code.','.$dataJson->time_zone.','.$dataJson->latitude.','.$dataJson->longitude.','.$dataJson->metro_code;
				}
				$export .= $user->email.",".$user->name.",".$user->date.",".$user->subscribed.",".$data."\r\n";
			}
		}

		header('Set-Cookie: fileDownload=true');
		header('Expires: ' . gmdate('D, d M Y H:i:s') . ' GMT');
		header('Cache-Control: no-cache');
		header('Pragma: hack');
		header('Content-Type: text/csv');
		header('Content-Disposition: attachment; filename="qc-subscribers-'.date("Y-m-d-h-i-s").'.csv"');
		header('Content-Length: ' . strlen($export));
		header('Connection: close');

		echo $export;
		exit();
	}

	public function adminBodyClass($classes) {
		if(!empty($_REQUEST['page'])){
			if($_REQUEST['page'] == 'ait_quick_comments_options'){
				$classes = explode(" ", $classes);
				array_push($classes, 'ait_quick_comments_options');
				$classes = implode(" ", $classes);
			}
		}
		return $classes;
	}

	public function registerSettings(){
		register_setting(
			'ait_quick_comments_options',
			'ait_quick_comments_options',
			array($this, 'validateOptions')
		);

		foreach($this->config['admin'] as $section => $settings){
			add_settings_section(
				$section,
				$settings['title'],
				'__return_false',
				'ait_quick_comments_options'
			);

			foreach($settings['options'] as $slug => $value){
				$label = class_exists("AitTheme") ? "" : $value['label'];

				add_settings_field(
					$slug,
					$label,
					array($this, 'renderField'),
					'ait_quick_comments_options',
					$section,
					array('slug' => $slug, 'config' => $value)
				);
			}
		}
	}

	public function validateOptions($input){
		return wp_parse_args($input, $this->getOptions());
	}

	public function renderField($option){
		if(class_exists("AitTheme")){
			switch($option['config']['type']){
				case "text":
					echo "<div class='ait-opt-container ait-opt-string-main'>";
						echo "<div class='ait-opt-wrap'>";
							echo "<div class='ait-opt-label'>";
								echo "<div class='ait-label-wrapper'>";
									echo "<span class='ait-label'>".$option['config']['label']."</span>";
								echo "</div>";
								if(!empty($option['config']['help'])) {
									echo "<div class='ait-opt-help'>";
										echo "<div class='ait-help'>";
											echo $option['config']['help'];
										echo "</div>";
									echo "</div>";
								}
							echo "</div>";
							echo "<div class='ait-opt ait-opt-string'>";
								/*echo "<div class='ait-opt-wrapper'>";
									echo '<input type="text" name="ait_quick_comments_options['.$option['slug'].']" value="'.esc_attr($this->getOptions($option['slug'])).'">';
								echo "</div>";*/
								$stored = $this->getOptions($option['slug']);
								foreach(AitLangs::getLanguagesList() as $lang){

									if(!AitLangs::isFilteredOut($lang)){
										echo '<div class="ait-opt-wrapper '.AitLangs::htmlClass($lang->locale).'">';
											if(AitLangs::isEnabled()){
												echo '<div class="flag">'.$lang->flag.'</div>';
											}
											$value = is_array($stored) ? $stored[$lang->locale] : $stored;
											echo '<input type="text" name="ait_quick_comments_options['.$option['slug'].']['.$lang->locale.']" value="'.$value.'">';
										echo '</div>';
									} else {
										$value = is_array($input['label']) ? $input['label'][$lang->locale] : "";
										echo '<input type="hidden" name="ait_quick_comments_options['.$option['slug'].']['.$lang->locale.']" value="'.$value.'">';
									}

								}
							echo "</div>";
						echo "</div>";
					echo "</div>";
				break;
				case "textarea":
					echo "<div class='ait-opt-container ait-opt-multiline-code-main'>";
						echo "<div class='ait-opt-wrap'>";
							echo "<div class='ait-opt-label'>";
								echo "<div class='ait-label-wrapper'>";
									echo "<span class='ait-label'>".$option['config']['label']."</span>";
								echo "</div>";
								if(!empty($option['config']['help'])) {
									echo "<div class='ait-opt-help'>";
										echo "<div class='ait-help'>";
											echo $option['config']['help'];
										echo "</div>";
									echo "</div>";
								}
							echo "</div>";
							echo "<div class='ait-opt ait-opt-multiline-code'>";
								echo "<div class='ait-opt-wrapper'>";
									echo '<textarea name="ait_quick_comments_options['.$option['slug'].']" rows="5">'.esc_attr($this->getOptions($option['slug'])).'</textarea>';
								echo "</div>";
							echo "</div>";
						echo "</div>";
					echo "</div>";
				break;
				case "code":
					echo "<div class='ait-opt-container ait-opt-code-main'>";
						echo "<div class='ait-opt-wrap'>";
							echo "<div class='ait-opt-label'>";
								echo "<div class='ait-label-wrapper'>";
									echo "<span class='ait-label'>".$option['config']['label']."</span>";
								echo "</div>";
								if(!empty($option['config']['help'])) {
									echo "<div class='ait-opt-help'>";
										echo "<div class='ait-help'>";
											echo $option['config']['help'];
										echo "</div>";
									echo "</div>";
								}
							echo "</div>";
							echo "<div class='ait-opt ait-opt-code'>";
								echo "<div class='ait-opt-wrapper'>";
									echo '<input type="text" name="ait_quick_comments_options['.$option['slug'].']" value="'.esc_attr($this->getOptions($option['slug'])).'">';
								echo "</div>";
							echo "</div>";
						echo "</div>";
					echo "</div>";
				break;
				case "on-off":
					echo "<div class='ait-opt-container ait-opt-on-off-main'>";
						echo "<div class='ait-opt-wrap'>";
							echo "<div class='ait-opt-label'>";
								echo "<div class='ait-label-wrapper'>";
									echo "<span class='ait-label'>".$option['config']['label']."</span>";
								echo "</div>";
							echo "</div>";
							echo "<div class='ait-opt ait-opt-on-off'>";
								echo "<div class='ait-opt-wrapper'>";
									echo "<div class='ait-opt-switch'>";
										echo '<select name="ait_quick_comments_options['.$option['slug'].']">';
											if(esc_attr($this->getOptions($option['slug'])) == "1"){
												echo '<option value="1" selected>'.__('On', 'ait-quick-comments').'</option>';
												echo '<option value="0">'.__('Off', 'ait-quick-comments').'</option>';
											} else {
												echo '<option value="1">'.__('On', 'ait-quick-comments').'</option>';
												echo '<option value="0" selected>'.__('Off', 'ait-quick-comments').'</option>';
											}
										echo '</select>';
									echo "</div>";
								echo "</div>";
							echo "</div>";
							if(!empty($option['config']['help'])) {
								echo "<div class='ait-opt-help'>";
									echo "<div class='ait-help'>";
										echo $option['config']['help'];
									echo "</div>";
								echo "</div>";
							}
						echo "</div>";
					echo "</div>";
				break;
				case "color":
					echo "<div class='ait-opt-container ait-opt-color-main'>";
						echo "<div class='ait-opt-wrap'>";
							echo "<div class='ait-opt-label'>";
								echo "<div class='ait-label-wrapper'>";
									echo "<span class='ait-label'>".$option['config']['label']."</span>";
								echo "</div>";
							echo "</div>";
							echo "<div class='ait-opt ait-opt-color'>";
								echo "<div class='ait-opt-wrapper'>";
									echo "<div class='ait-colorpicker ait-control-wrapper'>";
										echo '<span class="ait-colorpicker-preview"><i style="background-color: '.esc_attr($this->getOptions($option['slug'])).'"></i></span>';
										echo '<input type="text" name="ait_quick_comments_options['.$option['slug'].']" value="'.esc_attr($this->getOptions($option['slug'])).'" class="ait-colorpicker-color" data-color-format="hex">';
										echo '<span class="ait-unit ait-value">100</span>';
										echo '<span class="ait-unit"> %</span>';
									echo "</div>";
								echo "</div>";
							echo "</div>";
							if(!empty($option['config']['help'])) {
								echo "<div class='ait-opt-help'>";
									echo "<div class='ait-help'>";
										echo $option['config']['help'];
									echo "</div>";
								echo "</div>";
							}
						echo "</div>";
					echo "</div>";
				break;
				case "url":
					echo "<div class='ait-opt-container ait-opt-url-main'>";
						echo "<div class='ait-opt-wrap'>";
							echo "<div class='ait-opt-label'>";
								echo "<div class='ait-label-wrapper'>";
									echo "<span class='ait-label'>".$option['config']['label']."</span>";
								echo "</div>";
								if(!empty($option['config']['help'])) {
									echo "<div class='ait-opt-help'>";
										echo "<div class='ait-help'>";
											echo $option['config']['help'];
										echo "</div>";
									echo "</div>";
								}
							echo "</div>";
							echo "<div class='ait-opt ait-opt-url'>";
								echo "<div class='ait-opt-wrapper'>";
									echo '<input type="url" name="ait_quick_comments_options['.$option['slug'].']" value="'.esc_attr($this->getOptions($option['slug'])).'">';
								echo "</div>";
							echo "</div>";
						echo "</div>";
					echo "</div>";
				break;
				case "page-select":
					echo "<div class='ait-opt-container ait-opt-page-select-main'>";
						echo "<div class='ait-opt-wrap'>";
							echo "<div class='ait-opt-label'>";
								echo "<div class='ait-label-wrapper'>";
									echo "<span class='ait-label'>".$option['config']['label']."</span>";
								echo "</div>";
								if(!empty($option['config']['help'])) {
									echo "<div class='ait-opt-help'>";
										echo "<div class='ait-help'>";
											echo $option['config']['help'];
										echo "</div>";
									echo "</div>";
								}
							echo "</div>";
							echo "<div class='ait-opt ait-opt-page-select'>";
								echo "<div class='ait-opt-wrapper chosen-wrapper'>";
									wp_dropdown_pages( array(
										'name'		=> 'ait_quick_comments_options['.$option['slug'].']',
										'selected' 	=> esc_attr($this->getOptions($option['slug'])),
									) );
								echo "</div>";
							echo "</div>";
						echo "</div>";
					echo "</div>";
				break;
				default:
				break;
			}
		} else {
			switch($option['config']['type']){
				case "text":
					$stored = $this->getOptions($option['slug']);
					if(is_array($stored)){
						foreach($stored as $lang => $text){
							if($lang == 'en_US'){
								echo '<input type="text" name="ait_quick_comments_options['.$option['slug'].'][en_US]" value="'.esc_attr($stored['en_US']).'" class="regular-text">';
							} else {
								echo '<input type="hidden" name="ait_quick_comments_options['.$option['slug'].']['.$lang.']" value="'.esc_attr($stored[$lang]).'" class="regular-text">';
							}
						}
					} else {
						echo '<input type="text" name="ait_quick_comments_options['.$option['slug'].']" value="'.esc_attr($stored).'" class="regular-text">';
					}
					if(!empty($option['config']['help'])) {echo '<p class="description">'.$option['config']['help'].'</p>';}
				break;
				case "textarea":
					echo '<textarea name="ait_quick_comments_options['.$option['slug'].']" class="large-text code">'.esc_attr($this->getOptions($option['slug'])).'</textarea>';
					if(!empty($option['config']['help'])) {echo '<p class="description">'.$option['config']['help'].'</p>';}
				break;
				case "code":
					echo '<input type="text" name="ait_quick_comments_options['.$option['slug'].']" value="'.esc_attr($this->getOptions($option['slug'])).'" class="regular-text">';
					if(!empty($option['config']['help'])) {echo '<p class="description">'.$option['config']['help'].'</p>';}
				break;
				case "on-off":
					echo '<select name="ait_quick_comments_options['.$option['slug'].']">';
						if(esc_attr($this->getOptions($option['slug'])) == "1"){
							echo '<option value="1" selected>'.__('On', 'ait-quick-comments').'</option>';
							echo '<option value="0">'.__('Off', 'ait-quick-comments').'</option>';
						} else {
							echo '<option value="1">'.__('On', 'ait-quick-comments').'</option>';
							echo '<option value="0" selected>'.__('Off', 'ait-quick-comments').'</option>';
						}
					echo '</select>';
					if(!empty($option['config']['help'])) {echo '<p class="description">'.$option['config']['help'].'</p>';}
				break;
				case "color":
					echo '<input type="text" name="ait_quick_comments_options['.$option['slug'].']" value="'.esc_attr($this->getOptions($option['slug'])).'" class="regular-text">';
					if(!empty($option['config']['help'])) {echo '<p class="description">'.$option['config']['help'].'</p>';}
				break;
				case "url":
					echo '<input type="text" name="ait_quick_comments_options['.$option['slug'].']" value="'.esc_attr($this->getOptions($option['slug'])).'" class="regular-text">';
					if(!empty($option['config']['help'])) {echo '<p class="description">'.$option['config']['help'].'</p>';}
				break;
				case "page-select":
					wp_dropdown_pages( array(
						'name'		=> 'ait_quick_comments_options['.$option['slug'].']',
						'selected' 	=> esc_attr($this->getOptions($option['slug'])),
					) );
					if(!empty($option['config']['help'])) {echo '<p class="description">'.$option['config']['help'].'</p>';}
				break;
				/*case "cpts-select":
					$post_types = get_post_types( array('public' => true), 'names' );		// show only types which have a detail page
					$ignored = array('attachment', 'revision', 'nav_menu_item');			// ignored cpts

					echo '<select name="ait_quick_comments_options['.$option['slug'].'][]" multiple="multiple" size="'.intval(count($post_types)-6).'">';
					foreach ( $post_types as $post_type ) {
						if(!in_array($post_type, $ignored)){
							$selected = "";
							if(in_array($post_type, $this->getOptions($option['slug']))){
								$selected = "selected";
							}
							echo '<option value="'.$post_type.'" '.$selected.'>'.$post_type.'</option>';
						}
					}
					echo '</select>';
					if(!empty($option['config']['help'])) {echo '<p class="description">'.$option['config']['help'].'</p>';}
				break;*/
				default:
				break;
			}
		}
	}

	public function getDefaultOptions(){
		$result = array();
		$this->loadPlugin();

		foreach($this->config['admin'] as $section => $settings){
			foreach ($settings['options'] as $slug => $value) {
				switch($value['type']){
					case "on-off":
						$result[$slug] = intval((bool)$value['default']);
					break;
					case "page-select":
						$result[$slug] = intval($value['default']);
					break;
					default:
						$result[$slug] = $value['default'];
					break;
				}
			}
		}
		return $result;
	}

	public function getOptions($key = ''){
		$options = wp_parse_args(
			get_option('ait_quick_comments_options', array()),
			$this->getDefaultOptions()
		);
		if($key and isset($options[$key])){
			return $options[$key];
		}

		return $options;
	}

	public function adminOptionsOnSave(){
		if(isset($_GET['settings-updated']) && $_GET['settings-updated']){
			if(!class_exists('lessc')){
				require(plugin_dir_path( __FILE__ ).'libs/lessc.inc.php');
			}
			$parser = new lessc;

			$colors = array();
			$dbOptions = $this->getOptions();
			foreach($this->config['admin']['colors']['options'] as $slug => $value){
				$colors[$slug] = $dbOptions[$slug];
			}

			$parser->setVariables($colors);
			@$parser->compileFile($this->paths['uri']['css'].'style.less', $this->paths['uri']['css'].'style.css');
		}

		if(isset($_GET['settings-reset']) && $_GET['settings-reset']){
			update_option('ait_quick_comments_options', $this->getDefaultOptions());
			wp_safe_redirect( admin_url( 'admin.php?page=ait_quick_comments_options&settings-updated=true' ) );
		}
	}

	public function adminMenu(){
		$parent = class_exists('AitTheme') ? "ait-theme-options" : "options-general.php";
		$caps = class_exists('AitTheme') ? "edit_theme_options" : "manage_options";
		$hook = add_submenu_page(
			$parent,
			__("Quick Comments", "ait-quick-comments"),
			__("Quick Comments", "ait-quick-comments"),
			apply_filters('ait-quick-comments-menu-permission', $caps),
			'ait_quick_comments_options',
			array($this, 'adminPage')
		);
		add_action('load-'.$hook, array($this, 'adminOptionsOnSave') );
	}

	public function adminPage(){
		if(class_exists("AitTheme")){
			echo '<div class="wrap">';
				echo '<div id="ait-quick-comments-page" class="ait-admin-page ait-options-layout">';
					echo '<div class="ait-admin-page-wrap">';
						/* Hack for WP notifications, all will be placed right after this h2 */
						echo '<h2 style="display: none;"></h2>';

						echo '<div class="ait-options-page-header">';
							echo '<h3 class="ait-options-header-title">'. __('Quick Comments', 'ait-quick-comments') .'</h3>';
							echo '<div class="ait-options-header-tools">';
								echo '<a class="ait-scroll-to-top"><i class="fa fa-chevron-up"></i></a>';
								echo '<div class="ait-header-save">';
									echo '<button type="submit" name="submit" id="quick-comments-save-options" class="ait-save-quick-comments">';
										esc_html_e('Save Options', 'ait-quick-comments');
									echo '</button>';

									echo '<div id="action-indicator-save" class="action-indicator action-save"></div>';
								echo '</div>';
							echo '</div>';

							echo '<div class="ait-sticky-header">';
								echo '<h4 class="ait-sticky-header-title">'. __('Quick Comments', 'ait-quick-comments') .'<i class="fa fa-circle"></i><span class="subtitle"></span></h4>';
							echo '</div>';
						echo '</div>';

						echo '<div class="ait-options-page">';

							/*echo '<div class="ait-options-page-top">';
								echo '<div class="ait-top-left">&nbsp;</div>';
								echo '<div class="ait-top-links">';
									echo '<ul class="ait-datalinks">';
										echo '<li>';
											echo '<a id="quick-comments-reset-options" href="#"  data-confirm="'.__('Are you sure?', 'ait-quick-comments').'" data-action="'.admin_url( "admin.php?page=ait_quick_comments_options&settings-reset=true" ).'">'.__('Reset To Defaults', 'ait-quick-comments').'</a>';
										echo '</li>';
									echo '</ul>';
									echo '<div class="ait-clear"></div>';
								echo '</div>';
								echo '<div class="ait-top-right">&nbsp;</div>';
							echo '</div>';*/

							echo '<div class="ait-options-page-content">';
								echo '<div class="ait-options-sidebar">';
									echo '<div class="ait-options-sidebar-content">';
										echo '<ul id="ait-quick-comments-tabs" class="ait-options-tabs">';
											foreach($this->config['admin'] as $section => $settings){
												echo '<li id="ait-quick-comments-'.$section.'-panel-tab" class=""><a href="#ait-quick-comments-'.$section.'-panel">'.$settings['title'].'</a></li>';
											}
										echo '</ul>';
									echo '</div>';
								echo '</div>';

								echo '<div class="ait-options-content">';
									echo '<div class="ait-options-controls-container">';
										echo '<div id="ait-quick-comments-panels" class="ait-options-controls ait-options-panels">';
											echo '<form action="options.php" method="post" name="">';
											settings_fields('ait_quick_comments_options');
											foreach($this->config['admin'] as $section => $settings){
												switch($section){
													case 'subscription':
														echo '<div id="ait-quick-comments-'.$section.'-panel" class="ait-options-group ait-options-panel ait-quick-comments-tabs-panel">';
															echo '<div id="ait-options-basic-'.$section.'" class="ait-controls-tabs-panel ait-options-basic">';
																echo '<div class="ait-options-section ">';
																	do_settings_fields( 'ait_quick_comments_options', $section );
																echo '</div>';
																if($this->getOptions('subscriptionEnabled')){
																	echo '<div class="ait-options-section ait-sec-title ait-ajax-section-subscribers">';
																		echo '<h2 class="ait-options-section-title">'.__('Subscribed today', 'ait-quick-comments').'</h2>';
																		echo '<div class="ait-ajax-section-data"></div>';
																	echo '</div>';
																}
															echo '</div>';
														echo '</div>';
													break;
													default:
														echo '<div id="ait-quick-comments-'.$section.'-panel" class="ait-options-group ait-options-panel ait-quick-comments-tabs-panel">';
															echo '<div id="ait-options-basic-'.$section.'" class="ait-controls-tabs-panel ait-options-basic">';
																echo '<div class="ait-options-section ">';
																	do_settings_fields( 'ait_quick_comments_options', $section );
																echo '</div>';
															echo '</div>';
														echo '</div>';
													break;
												}
											}
											echo '</form>';
										echo '</div>';
									echo '</div>';
								echo '</div>';
							echo '</div>';
						echo '</div>';
					echo '</div>';
				echo '</div>';
			echo '</div>';
		} else {
			echo '<div class="wrap">';
				screen_icon();
				echo '<h2>'.__('Quick Comments', 'ait-quick-comments').'</h2>';
				echo '<form action="options.php" method="post">';
					settings_fields('ait_quick_comments_options');
					do_settings_sections('ait_quick_comments_options');

					if($this->getOptions('subscriptionEnabled')){
						echo '<table class="form-table"><tr><th scope="row">'.__('Subscribed today', 'ait-quick-comments').'</th><td class="ajax-subscribers-container"><a id="display-subscribers" href="#">'.__('Display subscribers','ait-quick-comments').'</a></td></table>';
					}

					echo '<div class="controls">';
					submit_button(__('Submit', 'ait-quick-comments'), 'primary', 'submit', false, array( 'id' => 'quick-comments-save-options' ));
					echo '&nbsp;';
					echo '<input id="quick-comments-reset-options" type="submit" name="reset" value="'.__('Reset To Defaults', 'ait-quick-comments').'" class="button reset" data-confirm="'.__('Are you sure ?', 'ait-quick-comments').'" data-action="'.admin_url( "admin.php?page=ait_quick_comments_options&settings-reset=true" ).'">';
					/*if($this->getOptions('subscriptionEnabled')){
						echo '&nbsp;';
						echo '<input id="quick-comments-export-subscribers" type="submit" name="export" value="'.__('Export Subscribers', 'ait-quick-comments').'" class="button reset" data-confirm="'.__('Are you sure ?', 'ait-quick-comments').'" data-action="'.admin_url( "admin.php?page=ait_quick_comments_options&export-subscribers=true" ).'">';
					}*/
					echo '</div>';
				echo '</form>';
			echo '</div>';
		}
	}
}