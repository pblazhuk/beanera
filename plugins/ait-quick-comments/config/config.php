<?php

return array(

	'assets' => array(

		'frontend' => array(
			'js' => array(
				array(
					'handle'		=> 'ait-quick-comments-main',
					'filename'		=> 'script.js',
					'dependencies'	=> array('jquery', 'ait-quick-comments-cookies'),
					'version'		=> '1.0',
					'inFooter'		=> true
				),
				array(
					'handle'		=> 'ait-quick-comments-cookies',
					'filename'		=> 'libs/jquery.cookie.js',
					'dependencies'	=> array('jquery'),
					'version'		=> '1.0',
					'inFooter'		=> true
				),
			),
			'css' => array(
				array(
					'handle' 		=> 'ait-quick-comments-main',
					'filename'		=> 'style.css',
					'dependencies'	=> "",
					'version'		=> '1.0',
					'media'			=> 'all'
				),
				array(
					'handle' 		=> 'ait-quick-comments-responsive',
					'filename'		=> 'responsive.css',
					'dependencies'	=> "",
					'version'		=> '1.0',
					'media'			=> 'all'
				),
				array(
					'handle' 		=> 'ait-quick-comments-font-fontawesome',
					'filename'		=> 'font/fontawesome.css',
					'dependencies'	=> "",
					'version'		=> '4.2.0',
					'media'			=> 'all',
				),
			),
		),

		'admin' => array(
			'js' => array(
				array(
					'handle'		=> 'ait-quick-comments-admin',
					'filename'		=> 'admin.js',
					'dependencies'	=> array('jquery'),
					'version'		=> '1.0',
					'inFooter'		=> true
				),
			),
			'css' => array(
				array(
					'handle' 		=> 'ait-quick-comments-admin',
					'filename'		=> 'admin.css',
					'dependencies'	=> "",
					'version'		=> '1.0',
					'media'			=> 'all'
				),
			),
		),

	),

	'admin' => array(
		'general' => array(
			'title' => __('General', 'ait-quick-comments'),
			'options' => array(
				'defaultPage' => array(
					'label' 	=> __('Default Page', "ait-quick-comments"),
					'type'		=> 'page-select',
					'default'	=> '',
					'help'		=> __("Page where will be posted comments from the quick comments form if comments are not allowed on current page", 'ait-quick-comments'),
				),
				/*'products' => array(
					'label'		=> __('Products', "ait-quick-comments"),
					'type'		=> 'cpts-select',
					'default'	=> array('page'),
					'help'		=> __("Post types which are defined as products and have a detail page", 'ait-quick-comments'),
				),*/
				/*'adminNotifications' => array(
					'label' 	=> __('Admin notifications', "ait-quick-comments"),
					'type'		=> 'on-off',
					'default'	=> false,
					'help'		=> __("Notify admin about every public message posted", 'ait-quick-comments'),
				),*/
			),
		),

		'texts' => array(
			'title' => __('Texts', 'ait-quick-comments'),
			'options' => array(
				'title' => array(
					'label'		=> __('Title', 'ait-quick-comments'),
					'type'		=> 'text',
					'default'	=> __('Pre-sale Questions', 'ait-quick-comments'),
					'help'		=> __('Title displayed in widget header', 'ait-quick-comments'),
				),
				'message' => array(
					'label'		=> __('Message', 'ait-quick-comments'),
					'type'		=> 'text',
					'default'	=> __("Ask anything about our product and we will reply to you soon as possible", 'ait-quick-comments'),
					'help'		=> __('Text displayed in widget body', 'ait-quick-comments'),
				),
				'form-info-label' => array(
					'label'		=> __('User Information Label', 'ait-quick-comments'),
					'type'		=> 'text',
					'default'	=> __("Introduce Yourself", 'ait-quick-comments'),
					'help'		=> __('Text displayed above user information', 'ait-quick-comments'),
				),
				'form-name-placeholder' => array(
					'label'		=> __('User Name Placeholder', 'ait-quick-comments'),
					'type'		=> 'text',
					'default'	=> __("Name", 'ait-quick-comments'),
					'help'		=> __('Placeholder text displayed inside input', 'ait-quick-comments'),
				),
				'form-email-placeholder' => array(
					'label'		=> __('User Email Placeholder', 'ait-quick-comments'),
					'type'		=> 'text',
					'default'	=> __("Email", 'ait-quick-comments'),
					'help'		=> __('Placeholder text displayed inside input', 'ait-quick-comments'),
				),
				'form-data-label' => array(
					'label'		=> __('Message Information Label', 'ait-quick-comments'),
					'type'		=> 'text',
					'default'	=> __("Message", 'ait-quick-comments'),
					'help'		=> __('Text displayed above message information', 'ait-quick-comments'),
				),

				'form-private-text' => array(
					'label'		=> __('Private Message Label', 'ait-quick-comments'),
					'type'		=> 'text',
					'default'	=> __("Private Message", 'ait-quick-comments'),
					'help'		=> __('Text displayed beside private message checkbox', 'ait-quick-comments'),
				),
				'form-subscribe-text' => array(
					'label'		=> __('Subscription Label', 'ait-quick-comments'),
					'type'		=> 'text',
					'default'	=> __("Subscribe to newsletter", 'ait-quick-comments'),
					'help'		=> __('Text displayed beside subscription checkbox', 'ait-quick-comments'),
				),

				'form-captcha-label' => array(
					'label'		=> __('Captcha Label', 'ait-quick-comments'),
					'type'		=> 'text',
					'default'	=> __("Prove you're a human", 'ait-quick-comments'),
					'help'		=> __('Text displayed above captcha', 'ait-quick-comments'),
				),
				'form-btn-label' => array(
					'label'		=> __('Button Label', 'ait-quick-comments'),
					'type'		=> 'text',
					'default'	=> __("Send Message", 'ait-quick-comments'),
					'help'		=> __('Text displayed on send button', 'ait-quick-comments'),
				),
				'form-validation-error' => array(
					'label'		=> __('Validation Error Message', 'ait-quick-comments'),
					'type'		=> 'text',
					'default'	=> __("You have not filled all fields", 'ait-quick-comments'),
					'help'		=> __('Text displayed when form validation is invalid', 'ait-quick-comments'),
				),
				'form-captcha-error' => array(
					'label'		=> __('Captcha Error Message', 'ait-quick-comments'),
					'type'		=> 'text',
					'default'	=> __("Captcha failed to verify", 'ait-quick-comments'),
					'help'		=> __('Text displayed when captcha validation fail', 'ait-quick-comments'),
				),
				'form-ajax-sending' => array(
					'label'		=> __('Sending form message', 'ait-quick-comments'),
					'type'		=> 'text',
					'default'	=> __("Your message is being processed, please wait", 'ait-quick-comments'),
					'help'		=> __('Text displayed when the form is to be sent', 'ait-quick-comments'),
				),
				'form-ajax-success' => array(
					'label'		=> __('Successful form message', 'ait-quick-comments'),
					'type'		=> 'text',
					'default'	=> __("Comment successfully sent", 'ait-quick-comments'),
					'help'		=> __('Message displayed when form is sent successfully', 'ait-quick-comments'),
				),
				'form-ajax-error' => array(
					'label'		=> __('Error form message', 'ait-quick-comments'),
					'type'		=> 'text',
					'default'	=> __("Sending failed, try again later", 'ait-quick-comments'),
					'help'		=> __('Message displayed when form sending failed', 'ait-quick-comments'),
				),
			),
		),
		'icons' => array(
			'title' => __('Icons', 'ait-quick-comments'),
			'options' => array(
				'intro' => array(
					'label'		=> __('Main Icon Image', 'ait-quick-comments'),
					'type'		=> 'code',
					'default'	=> 'fa fa-envelope',
					'help'		=> __('Icon displayed in closed state, use Font Awesome classes', 'ait-quick-comments'),
				),
				'close' => array(
					'label'		=> __('Close Icon Image', 'ait-quick-comments'),
					'type'		=> 'code',
					'default'	=> 'fa fa-times',
					'help'		=> __('Icon for closing form, use Font Awesome classes', 'ait-quick-comments'),
				),
			),
		),
		'colors' => array(
			'title' => __('Colors', 'ait-quick-comments'),
			'options' => array(
				'colorMajor' => array(
					'label'		=> __('Main Color', 'ait-quick-comments'),
					'type'		=> 'color',
					'default'	=> '#6AC9ED',
					'help'		=> __('Main color for the widget', 'ait-quick-comments'),
				),
				'colorMinor' => array(
					'label'		=> __('Minor Color', 'ait-quick-comments'),
					'type'		=> 'color',
					'default'	=> '#666666',
					'help'		=> __('Minority color for the widget', 'ait-quick-comments'),
				),
				'colorBackground' => array(
					'label'		=> __('Background Color', 'ait-quick-comments'),
					'type'		=> 'color',
					'default'	=> '#FFFFFF',
					'help'		=> __('Background color for the widget', 'ait-quick-comments'),
				),

				'colorTitles' => array(
					'label'		=> __('Titles Color', 'ait-quick-comments'),
					'type'		=> 'color',
					'default'	=> '#FFFFFF',
					'help'		=> __('Color of titles inside widget', 'ait-quick-comments'),
				),
				'colorTexts' => array(
					'label'		=> __('Texts Color', 'ait-quick-comments'),
					'type'		=> 'color',
					'default'	=> '#666666',
					'help'		=> __('Color of texts inside widget', 'ait-quick-comments'),
				),
				'colorLinks' => array(
					'label'		=> __('Links Color', 'ait-quick-comments'),
					'type'		=> 'color',
					'default'	=> '#6AC9ED',
					'help'		=> __('Color of Links inside widget', 'ait-quick-comments'),
				),
			),
		),
		'branding' => array(
			'title' => __('Branding', 'ait-quick-comments'),
			'options' => array(
				'brandingEnabled' => array(
					'label'		=> __('Enabled', 'ait-quick-comments'),
					'type'		=> 'on-off',
					'default'	=> 'true',
					'help'		=> __('Show or hide bottom branding', 'ait-quick-comments'),
				),
				'brandingLink' => array(
					'label'		=> __('Link', 'ait-quick-comments'),
					'type'		=> 'url',
					'default'	=> 'https://www.ait-themes.club/wordpress-plugins/quick-comments/',
					'help'		=> __('Link for branding', 'ait-quick-comments'),
				),
				'brandingLabel' => array(
					'label'		=> __('Label', 'ait-quick-comments'),
					'type'		=> 'text',
					'default'	=> 'Powered by Quick Comments Plugin',
					'help'		=> __('Label for branding', 'ait-quick-comments'),
				),
				'brandingBlank' => array(
					'label'		=> __('Open In New Window', 'ait-quick-comments'),
					'type'		=> 'on-off',
					'default'	=> 'true',
					'help'		=> __('Open links in new window or tab', 'ait-quick-comments'),
				),
			),
		),

		'captha' => array(
			'title' => __('Captha', 'ait-quick-comments'),
			'options' => array(
				'captchaEnabled' => array(
					'label'		=> __('Enabled', 'ait-quick-comments'),
					'type'		=> 'on-off',
					'default'	=> 'true',
					'help'		=> __('Show or hide captcha validation', 'ait-quick-comments'),
				),
			),
		),

		'notification' => array(
			'title' => __('Email Notifications', 'ait-quick-comments'),
			'options' => array(

				'notificationHeaderFromEmail' => array(
					'label'		=> __('From Email', 'ait-quick-comments'),
					'type'		=> 'code',
					'default'	=> '',
					'help'		=> __('Email address displayed as sender in received email, leave empty to use default admin email', 'ait-quick-comments'),
				),
				'notificationHeaderFromName' => array(
					'label'		=> __('From Name', 'ait-quick-comments'),
					'type'		=> 'code',
					'default'	=> 'Quick comments',
					'help'		=> __('Name displayed as sender in received email', 'ait-quick-comments'),
				),
				'notificationSubject' => array(
					'label'		=> __('Subject', 'ait-quick-comments'),
					'type'		=> 'code',
					'default'	=> 'Admin replied to your comment',
					'help'		=> __('Email subject text', 'ait-quick-comments'),
				),
				'notificationContent' => array(
					'label'		=> __('Content', 'ait-quick-comments'),
					'type'		=> 'textarea',
					'default'	=> '<html><head><meta http-equiv="Content-Type" content="text/html charset="utf-8"><title>Message</title></head>
<body style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;">
<div leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0" style="width: 100%; -webkit-text-size-adjust: none; margin: 0; padding: 0; background-color: #f5f5f5; font-family: Helvetica, Arial, sans-serif">
<center><table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%"><tbody><tr><td align="center" valign="top" style="border-collapse: collapse; color: #666666"><table border="0" cellpadding="0" cellspacing="0" width="85%">
<tbody><tr><td align="center" valign="top" style="border-collapse: collapse; color: #333333; padding: 60px 20px; font-size: 30px; font-weight: bold; line-height: 120%; text-align: center;">Reply from <b style="color: #0074a2">[name]</b></td></tr><tr>
<td width="100%" style="border-collapse: collapse; color: #666666; padding: 40px; background-color: #ffffff; border: 1px solid #dddddd; -moz-border-radius: 5px; -webkit-border-radius: 5px; border-radius: 5px; font-size: 13px;" align="left" valign="top">
<div style="font-size: 17px; color: #333333; text-align: left; font-weight: bold; padding-bottom: 5px;">Message left on <b style="color: #0074a2">[time]</b></div>
<div style="line-height: 20px;">[content]</div></td></tr><tr><td align="center" valign="top" style="border-collapse: collapse; height: 40px;">
</td></tr><tr><td width="100%" style="border-collapse: collapse; color: #666666; padding: 35px; background-color: #ffffff; border: 1px solid #dddddd; -moz-border-radius: 5px; -webkit-border-radius: 5px; border-radius: 5px; font-size: 13px;" align="left" valign="top">
<table border="0" cellpadding="5px" cellspacing="0"><tbody><tr><td style="border-collapse: collapse; color: #666666; padding-right:20px"><b style="color: #333333; font-size: 11px; text-transform: uppercase">URL</b></td>
<td style="border-collapse: collapse; color: #666666">[link]</td></tr></tbody></table></td></tr></tbody></table></td></tr><tr><td align="center" valign="top" style="border-collapse: collapse; color: #666666"><table border="0" cellpadding="0" cellspacing="0" width="85%">
<tbody><tr><td align="center" valign="middle" style="border-collapse: collapse; color: #999999; font-size: 13px; padding: 40px; ">[branding]</td>
</tr></tbody></table></td></tr></tbody></table></center></div></body></html>',
					'help'		=> __('Content for email notification, use [content] tag to include text of the reply or [link] to include href', 'ait-quick-comments'),
				),
			),
		),

		'private' => array(
			'title' => __('Private Messages', 'ait-quick-comments'),
			'options' => array(
				'privateEnabled' => array(
					'label'		=> __('Enable Private Messages', 'ait-quick-comments'),
					'type'		=> 'on-off',
					'default'	=> true,
					'help'		=> __('Enable private messages funcionality through quick comments', 'ait-quick-comments'),
				),
				'privateCheckedByDefault' => array(
					'label'		=> __('Checked By Default', 'ait-quick-comments'),
					'type'		=> 'on-off',
					'default'	=> true,
					'help'		=> __('Private messages checkbox is selected by default', 'ait-quick-comments'),
				),
				'privateEmailFromEmail' => array(
					'label'		=> __('From Email', 'ait-quick-comments'),
					'type'		=> 'code',
					'default'	=> '',
					'help'		=> __('Email address displayed as sender in received email, leave empty to use default admin email', 'ait-quick-comments'),
				),
				'privateEmailFromName' => array(
					'label'		=> __('From Name', 'ait-quick-comments'),
					'type'		=> 'code',
					'default'	=> 'Quick comments',
					'help'		=> __('Name displayed as sender in received email', 'ait-quick-comments'),
				),
				'privateEmail' => array(
					'label'		=> __('Recipient Email', 'ait-quick-comments'),
					'type'		=> 'code',
					'default'	=> '',
					'help'		=> __('Email address of private message recipient, leave empty to use default admin email', 'ait-quick-comments'),
				),
				'privateSubject' => array(
					'label'		=> __('Subject', 'ait-quick-comments'),
					'type'		=> 'code',
					'default'	=> 'New private message from [name]: [excerpt]',
					'help'		=> __('Email subject text, use [name] to include sender name, [excerpt] to include part of message', 'ait-quick-comments'),
				),
				'privateContent' => array(
					'label'		=> __('Content', 'ait-quick-comments'),
					'type'		=> 'textarea',
					'default'	=> '<html><head><meta http-equiv="Content-Type" content="text/html charset="utf-8"><title>Message</title></head>
<body style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;">
<div leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0" style="width: 100%; -webkit-text-size-adjust: none; margin: 0; padding: 0; background-color: #f5f5f5; font-family: Helvetica, Arial, sans-serif">
<center><table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%"><tbody><tr><td align="center" valign="top" style="border-collapse: collapse; color: #666666"><table border="0" cellpadding="0" cellspacing="0" width="85%">
<tbody><tr><td align="center" valign="top" style="border-collapse: collapse; color: #333333; padding: 60px 20px; font-size: 30px; font-weight: bold; line-height: 120%; text-align: center;">Quick comment from <b style="color: #0074a2">[name]</b></td></tr><tr>
<td width="100%" style="border-collapse: collapse; color: #666666; padding: 40px; background-color: #ffffff; border: 1px solid #dddddd; -moz-border-radius: 5px; -webkit-border-radius: 5px; border-radius: 5px; font-size: 13px;" align="left" valign="top">
<div style="font-size: 17px; color: #333333; text-align: left; font-weight: bold; padding-bottom: 5px;">Message left on <b style="color: #0074a2">[time]</b></div>
<div style="line-height: 20px;">[content]</div></td></tr><tr><td align="center" valign="top" style="border-collapse: collapse; height: 40px;">
</td></tr><tr><td width="100%" style="border-collapse: collapse; color: #666666; padding: 35px; background-color: #ffffff; border: 1px solid #dddddd; -moz-border-radius: 5px; -webkit-border-radius: 5px; border-radius: 5px; font-size: 13px;" align="left" valign="top">
<table border="0" cellpadding="5px" cellspacing="0"><tbody><tr><td style="border-collapse: collapse; color: #666666; padding-right:20px"><b style="color: #333333; font-size: 11px; text-transform: uppercase">NAME</b></td>
<td style="border-collapse: collapse; color: #666666">[name]</td></tr><tr><td style="border-collapse: collapse; color: #666666; padding-right:20px"><b style="color: #333333; font-size: 11px; text-transform: uppercase">EMAIL</b></td>
<td style="border-collapse: collapse; color: #666666">[email]</td></tr><tr><td style="border-collapse: collapse; color: #666666; padding-right:20px"><b style="color: #333333; font-size: 11px; text-transform: uppercase">LOCATION</b></td>
<td style="border-collapse: collapse; color: #666666">[location]</td></tr><tr><td style="border-collapse: collapse; color: #666666; padding-right:20px"><b style="color: #333333; font-size: 11px; text-transform: uppercase">URL</b></td>
<td style="border-collapse: collapse; color: #666666">[link]</td></tr></tbody></table></td></tr></tbody></table></td></tr><tr><td align="center" valign="top" style="border-collapse: collapse; color: #666666"><table border="0" cellpadding="0" cellspacing="0" width="85%">
<tbody><tr><td align="center" valign="middle" style="border-collapse: collapse; color: #999999; font-size: 13px; padding: 40px; ">[branding]</td>
</tr></tbody></table></td></tr></tbody></table></center></div></body></html>',
					'help'		=> __('Content for email notification, use [content] tag to include text of the message, [name] to include writers name, [email] to include writers email, [link] to include page url, [location] to include user location, [time] to get message time and [branding] to include branding', 'ait-quick-comments'),
				),
			),
		),

		'subscription' => array(
			'title' => __('Subscription', 'ait-quick-comments'),
			'options' => array(
				'subscriptionEnabled' => array(
					'label'		=> __('Enable Subscription', 'ait-quick-comments'),
					'type'		=> 'on-off',
					'default'	=> true,
					'help'		=> __('Enable subscription functionality through quick comments', 'ait-quick-comments'),
				),
			),
		),

	),
);