<?php
return array(
	'title' => _x('Get Directions', 'name of element', 'ait-get-directions'),
	'package' => array(
		'business' => true,
		'developer' => true,
	),
	'configuration' => array(
		'columnable' => false,
		'sortable' => true,
		'cloneable' => true,
		'class' => 'AitGetDirectionsElement',
	),
	'icon' => 'fa-location-arrow',
	'color' => '#DC5959',
);