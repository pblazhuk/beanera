<?php

/*
 * AIT WordPress Theme Framework
 *
 * Copyright (c) 2013, Affinity Information Technology, s.r.o. (http://ait-themes.com)
 */


class AitGetItemsAjax extends AitFrontendAjax
{

	/**
	 * @WpAjax
	 */
	public function retrieve()
	{
		switch ($_POST['type']) {
			case 'headerMap':
				$data['markers'] = $this->getHeaderMapMarkers($_POST['pageType']);
				$html_data = '';
				$data['request_data'] = $_POST;
				break;
			default:
				break;
		}
		$this->sendJson(array(
			'message' => __("Have posts", 'ait'),
			'raw_data' => $data,
			'html_data' => $html_data,
		));

	}


	/**
	 * @WpAjax
	 */
	public function getHeaderMapMarkers()
	{
		$start = microtime(true);

		$data = array();
		$html_data = "";

		$GLOBALS['__ait_query_data'] = $_POST['query-data'];
		// cast nopaging parameter from string 'false' and 'true' into boolean
		$queryVars = $_POST['globalQueryVars'];
		if(isset($queryVars['nopaging'])){
			$queryVars['nopaging'] = filter_var( $queryVars['nopaging'], FILTER_VALIDATE_BOOLEAN);
		}

		$queryVars['post_status'] = "publish";
		/*clear meta_query and prevent duplicities
		build meta_query in next filters */
		$queryVars['meta_query'] = array();

		$options = array();
		if(isset($_POST['enableTel'])){
			$options['enableTel'] = $_POST['enableTel'];
		}

		$markers = array();
		$ignorePagination = filter_var( $_POST['ignorePagination'], FILTER_VALIDATE_BOOLEAN);

		/******** IS SEARCH PAGE *********/
		if ($_POST['pageType'] == "search"){
			$args = aitBuildSearchQuery($queryVars, $ignorePagination);
			$itemsQuery = new WpLatteWpQuery($args);
			$markers = aitGetItemsMarkers($itemsQuery, $options);

		/******** IS AIT TAX PAGE *******/
		} elseif($_POST['pageType'] == "ait-items") {
			$args = aitBuildItemTaxQuery($queryVars, $ignorePagination);
			$itemsQuery = new WpLatteWpQuery($args);
			$markers = aitGetItemsMarkers($itemsQuery, $options);
		/******** IS SINGLE ITEM PAGE *******/
		/******** IS SINGLE EVENT PRO PAGE *******/
		} elseif($_POST['pageType'] == "ait-item" || $_POST['pageType'] == "ait-event-pro") {
			// $args = aitBuildItemQuery($queryVars, $ignorePagination);
			$itemsQuery = new WpLatteWpQuery($queryVars);
			$markers = aitGetItemsMarkers($itemsQuery, $options);
		/****** IS NORMAL PAGE ******/
		} else {

			$args = array(
				'post_type'      => 'ait-item',
				'post_status'	 => 'publish',
				'posts_per_page' => (int)$_POST['query-data']['ajax']['limit'],
				'offset'         => (int)$_POST['query-data']['ajax']['offset'],
				// 'lang'           => AitLangs::getCurrentLanguageCode(),
				'nopaging'       => false,
				'no_found_rows'  => false
			);
			$itemsQuery = new WpLatteWpQuery($args);
			$markers = aitGetItemsMarkers($itemsQuery, $options);
		}

		$data['found_posts'] = $itemsQuery->found_posts;
		$data['post_count'] = $itemsQuery->post_count;

		$data['markers'] = $markers;
		$time_elapsed_secs = microtime(true) - $start;
		$this->sendJson(array(
			'message' => __("Have posts", 'ait'),
			'raw_data' => $data,
			'html_data' => $html_data,
		));
	}

}
